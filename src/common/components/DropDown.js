import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
    },
    dense: {
        marginTop: 19,
    },
    menu: {
        width: 200,
    },
}));



const DropDown =(props) => {
    const {value, setValue, data, label, helperText, error}= props
    const classes = useStyles();

    const handleChange = name => event => {

        const status = event.target.value
        setValue(status)
    };

    return (
        <form className={classes.container} noValidate autoComplete="off">

             <TextField
                id="standard-select-currency-native"
                select
                error={error}
                label={label}
                className={classes.textField}
                value={value}
                onChange={handleChange('currency')}
                SelectProps={{
                    native: true,
                    MenuProps: {
                        className: classes.menu,
                    },
                }}
                helperText={error}
                margin="normal"
            >
                {data.map(option => (
                    <option key={option.value} value={option.value}>
                        {option.label}
                    </option>
                ))}
            </TextField>

        </form>
    );
}

export default DropDown