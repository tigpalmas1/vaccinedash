import React, { Fragment, useState } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {connect} from 'react-redux';
import {fetch_cities_action, fetch_tags, filterUpdateAction, setFilterAction, stateChangedAction} from "../../redux/actions/filter_action";

import MenuListComposition from './MenuListComposition'
import Switches from "./Switches";
import Tag from "./Tag";
import TagItem from "./TagItem";
import {grey, mainOrange, titleGrey} from "../colors/index";
import {tag} from "../styles/geralStyles";
import _  from 'lodash';
import MyDatePicker from './MyDatePicker'
class AlertDialog extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            open: false,
        };

        this.props.fetch_tags();
        this.props.fetch_cities_action();

    }


    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    checkHasDate(date) {
        if (date === "" || date === "today" || date === "week" || date === "weekend") {
            return false
        } else {
            return true
        }
    }

    checkColor =  (tag) =>{

        const {selectedTags,} = this.props
        const found = _.find(selectedTags, tag);
        if (found) {

            return mainOrange
        } else {

            return titleGrey
        }
    }


    searchFilter = ()=>{
        this.props.setFilterAction(this.props.preference)
        this.props.stateChangedAction(true)
        this.props.onDialogDismiss()
    }


    render() {
        const {favorit, cities, distance, date, selectedCity, tags, selectedTags, type, value} = this.props.preference
        const {hasDate, hasFavorit, hasCity, hasStyle, hasDistance, hasType, hasValue} = this.props



        return (
            <div>
                <Dialog
                    className={{width: 500, height: '100%'}}
                    open={this.props.isDialogOpen}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >


                    {hasCity &&
                    <div style={{width: 300, height:200}}>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                <h3>Cidade</h3>

                                <MenuListComposition
                                    setCity={(city) => this.props.filterUpdateAction({prop: "selectedCity", value: city})}
                                    selectedCity={selectedCity}
                                    cities={cities}/>


                            </DialogContentText>
                        </DialogContent>
                    </div>

                    }


                    {hasFavorit &&
                    <DialogContent>

                        <DialogContentText id="alert-dialog-description">
                            <h3>Favoritos</h3>
                            <div style={{flexDirection: 'row', display: 'flex', alignItems: 'center'}}> Buscar somente
                                de suas casas Favoritas ?
                                <Switches/></div>

                        </DialogContentText>

                    </DialogContent>
                    }


                    {hasDistance &&
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            <h3>Distância</h3>

                            <div style={{flexDirection: 'row', display: 'flex'}}>



                            </div>


                        </DialogContentText>

                    </DialogContent>
                    }


                    {hasDate &&
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            <h3>Data</h3>


                            <div style={{flexDirection: 'row', display: 'flex', width: '100%',justifyContent: 'center'}}>
                                <div onClick={() => this.props.filterUpdateAction({prop: "date", value: ''})}>
                                    <Tag fontSize={12} backgroundColor={date === "" ? mainOrange : titleGrey}
                                         title="Todos os dias"/>
                                </div>
                            </div>


                            <div style={{flexDirection: 'row', display: 'flex', marginTop: 10}}>

                                <div onClick={() => this.props.filterUpdateAction({prop: "date", value: 'today'})}>
                                    <Tag fontSize={12} backgroundColor={date === "today" ? mainOrange : titleGrey}
                                         title="Hoje"/>
                                </div>
                                <div onClick={() => this.props.filterUpdateAction({prop: "date", value: 'week'})}>
                                    <Tag fontSize={12} backgroundColor={date === "week" ? mainOrange : titleGrey}
                                         title="Essa semana"/>
                                </div>
                                <div onClick={() => this.props.filterUpdateAction({prop: "date", value: 'weekend'})}>
                                    <Tag fontSize={12} backgroundColor={date === "weekend" ? mainOrange : titleGrey}
                                         title="Fim de Semana"/>
                                </div>

                            </div>


                            <div style={{flexDirection: 'row', display: 'flex', marginTop: 10, width: '100%', justifyContent: 'center'}}>
                                <div >
                                    <MyDatePicker/>
                                </div>
                            </div>


                        </DialogContentText>

                    </DialogContent>
                    }

                    {hasType &&
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            <h3>Tipo de Estabelecimento</h3>


                            <div style={{flexDirection: 'row', display: 'flex', width: '100%',justifyContent: 'center'}}>
                                <div onClick={() => this.props.filterUpdateAction({prop: "type", value: 'all'})}>
                                    <Tag fontSize={12} backgroundColor={type === "all" ? mainOrange : titleGrey}
                                         title="Todos os Tipos"/>
                                </div>
                            </div>


                            <div style={{flexDirection: 'row', display: 'flex', marginTop: 10}}>

                                <div onClick={() => this.props.filterUpdateAction({prop: "type", value: 'nightclub'})}>
                                    <Tag fontSize={12} backgroundColor={type === "nightclub" ? mainOrange : titleGrey}
                                         title="Baladas"/>
                                </div>
                                <div onClick={() => this.props.filterUpdateAction({prop: "type", value: 'bar'})}>
                                    <Tag fontSize={12} backgroundColor={type === "bar" ? mainOrange : titleGrey}
                                         title="Bares"/>
                                </div>
                                <div onClick={() => this.props.filterUpdateAction({prop: "type", value: 'showClubs'})}>
                                    <Tag fontSize={12} backgroundColor={type === "showClubs" ? mainOrange : titleGrey}
                                         title="Casas de Show"/>
                                </div>

                            </div>





                        </DialogContentText>

                    </DialogContent>
                    }

                    {hasValue &&
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            <h3>Valor de Entrada</h3>



                            <div style={{flexDirection: 'row', display: 'flex', marginTop: 10}}>
                                <div onClick={() => this.props.filterUpdateAction({prop: "value", value: 'all'})}>
                                    <Tag fontSize={12} backgroundColor={value === "all" ? mainOrange : titleGrey}
                                         title="Todos os Valores"/>
                                </div>

                                <div onClick={() => this.props.filterUpdateAction({prop: "value", value: 'free'})}>
                                    <Tag fontSize={12} backgroundColor={value === "free" ? mainOrange : titleGrey}
                                         title="Gratuitos"/>
                                </div>
                                <div onClick={() => this.props.filterUpdateAction({prop: "value", value: 'paid'})}>
                                    <Tag fontSize={12} backgroundColor={value === "paid" ? mainOrange : titleGrey}
                                         title="Pagos"/>
                                </div>


                            </div>





                        </DialogContentText>

                    </DialogContent>
                    }

                    {hasStyle &&
                    <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            <h3>Estilos</h3>

                            <div
                                style={{width: '100%',  marginTop: 5, alignItems: 'center'}}>


                                <div
                                    style={{  flexDirection: 'row',display: 'flex', marginBottom: 10, width: '100%', justifyContent: 'center'}}
                                    onClick={() =>
                                        this.props.filterUpdateAction({prop: 'selectedTags', value: []})}
                                >

                                    <Tag fontSize={12}
                                         backgroundColor={selectedTags.length === 0 ? mainOrange : titleGrey}
                                         title="Todos os Estilos"/>


                                </div>


                            </div>

                            <div style={{flexDirection: 'row', display: 'flex'}}>

                                {tags.map(tag =>
                                    <TagItem fontSize={12} backgroundColor={this.checkColor(tag)}  tag={tag}/>
                                )}


                            </div>


                        </DialogContentText>

                    </DialogContent>
                    }




                    <DialogActions>
                        <Button onClick={() => this.props.onDialogDismiss()} color="red">
                            Cancelar
                        </Button>
                        <Button onClick={() => this.searchFilter()} color="primary">
                            Salvar
                        </Button>

                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

const mapStateToProps = (state) => {

    return {
        selectedTags: state.filter.preference.selectedTags,
        preference: state.filter.preference,
    }


}

export default connect(mapStateToProps, {fetch_cities_action,
    fetch_tags,
    filterUpdateAction,
    setFilterAction,
    stateChangedAction})(AlertDialog);
