import React from 'react';
import { createMuiTheme, withStyles, makeStyles } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import Button from '@material-ui/core/Button';
import purple from '@material-ui/core/colors/purple';
import deepPurple from '@material-ui/core/colors/deepPurple';
import {mainOrange} from "../colors/index";

const BootstrapButton = withStyles({
    root: {
        boxShadow: 'none',
        textTransform: 'none',
        fontSize: 16,
        padding: '6px 12px',
        border: '1px solid',
        lineHeight: 1.5,
        backgroundColor: mainOrange,
        borderColor: mainOrange,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        '&:hover': {
            backgroundColor: mainOrange,
            borderColor: 'grey',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: 'grey',
            borderColor: mainOrange,
        },

    },
})(Button);



const useStyles = makeStyles(theme => ({
    margin: {
        margin: theme.spacing(1),
    },
}));



export default function CustomizedButtons({onClick, label, disabled}) {
    const classes = useStyles();

    return (
        <div>

            <BootstrapButton
                disabled={disabled}
                onClick={onClick}
                variant="contained" color="primary" disableRipple className={classes.margin}>
                {label}
            </BootstrapButton>
        </div>
    );
}
