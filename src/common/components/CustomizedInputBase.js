import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import CancelIcon from '@material-ui/icons/Clear';


class  CustomizedInputBase extends   React.Component {


    state ={
        text: '',
    }

    handleChange =(e,)=>  {
            this.setState({text: e.target.value})
           this.props.handleChange(e.target.value)

    };



    render() {
        const {text} = this.state
        const {root, input, iconButton, divider} = useStyles


        return (
            <div>
            <Paper style={{paddingLeft: 20, paddingRight: 20,  display: 'flex', flexWrap: 'wrap'}}>
                <IconButton className={iconButton} aria-label="Search">
                    <SearchIcon/>
                </IconButton>
                <InputBase
                    value={text}
                    dense
                    size={'small'}
                    onChange={(e) => {
                        this.handleChange(e,)
                    }}
                    style={{width: 300,}} placeholder="Pesquisa rápida"/>


                {this.state.text.length>0 &&
                <IconButton
                    onClick={()=>{

                        this.setState({text: ''})
                        this.props.handleChange('')

                    }}
                    className={iconButton} aria-label="Search">
                    <CancelIcon/>
                    </IconButton>
                }



            </Paper>


            </div>
        );
    }
}

const useStyles = {
    root: {
        padding: '2px 4px',
        display: 'flex',
        alignItems: 'center',
        width: 400,
    },
    input: {
        width:400,
        marginLeft: 8,
        flex: 1,
    },
    iconButton: {
        padding: 10,
    },
    divider: {
        width: 1,
        height: 28,
        margin: 4,
    },
}

export default CustomizedInputBase;
