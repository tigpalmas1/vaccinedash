import React from 'react';
import {gradientColor, lightPurple, mainOrange, purpleTitle} from "../colors/index";

const Tag = ({title, iconName, backgroundColor,fontSize, onClick, cursor}) =>{
    return (
        <div
            onClick={onClick}
            style={{ background: backgroundColor || mainOrange, flexWrap: 'wrap', padding:5,
                cursor: 'pointer',
                borderRadius: 10, marginRight: 5}}>
            <div style={{color: 'white', flexWrap: 'wrap',flex: 1, fontSize: fontSize || 10, fontWeight: '900'}}>
                {iconName &&
                <i class={`${iconName}  icon`}></i>
                }

                {title}
            </div>
        </div>

    )
}



export default Tag