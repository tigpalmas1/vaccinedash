import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(0),
        marginRight: theme.spacing(1),
        width: 200,
    },
    dense: {
        marginTop: -5,
    },
    menu: {
        width: 200,
    },
}));




const  TextField = (props) => {

    const {selectValue, setStatus, data,value, label, type } = props


    const classes = useStyles();



    const handleChange = name => event => {

        console.log('chamando')
       // selectValue(event.target.value)
    };

    return (


        <TextField
            id="standard-number"
            label={label}
            value={value}
            onChange={handleChange('age')}
            type={type}
            className={classes.textField}
            InputLabelProps={{
                shrink: true,
            }}
            margin="normal"
        />


    );
}


export default TextField