import React from 'react';
import MaterialTable from 'material-table';
import {createTagAction, deleteTagAction} from "../../redux/actions";
import {connect} from "react-redux";

const  TableStyles =(props)=> {
    const {createTagAction, deleteTagAction} = props||{}
    const {tags} = props||{}


    const [state, setState] = React.useState({
        columns: [
            { title: 'Nome', field: 'name' },

        ],
        data: tags,
    });


    const callback =(styles)=>{

        setState(
            prevState => {
            return { ...prevState,
                data: styles
            };
        });
    }
    return (
        <MaterialTable
            title="Estilos"
            columns={state.columns}
            data={state.data}
            editable={{
                onRowAdd: newData =>

                    new Promise(resolve => {

                        setTimeout(() => {
                            resolve();
                            createTagAction(newData, callback)
                        }, 600);
                    }),
                onRowUpdate: (newData, oldData) =>
                    new Promise(resolve => {
                        setTimeout(() => {
                            resolve();
                            if (oldData) {
                                setState(prevState => {
                                    const data = [...prevState.data];
                                    data[data.indexOf(oldData)] = newData;
                                    return { ...prevState, data };
                                });
                            }
                        }, 600);
                    }),
                onRowDelete: oldData =>
                    new Promise(resolve => {
                        setTimeout(() => {
                            resolve();
                            deleteTagAction(oldData, callback)
                        }, 600);
                    }),
            }}
        />
    );
}


const mapStateToProps = (state) => {


    return {

    }
}


export default connect(mapStateToProps, {createTagAction, deleteTagAction})(TableStyles)

