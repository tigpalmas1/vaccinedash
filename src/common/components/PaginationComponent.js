import React, { Component } from 'react'
import { Grid, Form, Pagination, Segment } from 'semantic-ui-react'

class PaginationComponent extends Component {
    state = {
        boundaryRange: 1,
        showFirstAndLastNav: true,
        showPreviousAndNextNav: true,
        totalPages: 50,
    }

    handleCheckboxChange = (e, { checked, name }) =>
        this.setState({ [name]: checked })

    handleInputChange = (e, { name, value }) => this.setState({ [name]: value })

    handlePaginationChange = (e, { activePage }) => {

        this.props.setActivePage(activePage)
       }

    render() {
        const {
            showFirstAndLastNav,
            showPreviousAndNextNav,
            totalPages,

        } = this.state

        const {activePage} = this.props

        return (
            <Grid columns={1}>
                <Grid.Column>
                    <Pagination
                        activePage={activePage}
                        onPageChange={this.handlePaginationChange}
                        size='medium'
                        totalPages={totalPages}
                        // Heads up! All items are powered by shorthands, if you want to hide one of them, just pass `null` as value

                        firstItem={showFirstAndLastNav ? undefined : null}
                        lastItem={showFirstAndLastNav ? undefined : null}
                        prevItem={showPreviousAndNextNav ? undefined : null}
                        nextItem={showPreviousAndNextNav ? undefined : null}
                    />
                </Grid.Column>


            </Grid>
        )
    }
}

export default  PaginationComponent