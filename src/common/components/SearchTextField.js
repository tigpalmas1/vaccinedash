import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
    },
    dense: {
        marginTop: 19,
    },
    menu: {
        width: 200,
    },
}));


const SearchTextField =({label, value, setValue})=> {
    const classes = useStyles();


    const handleChange = name => event => {

        setValue( event.target.value);
    };

    return (

            <TextField
                id="standard-search"
                label={label}
                type="search"
                value={value}
                onChange={handleChange('keyword')}
                className={classes.textField}
                margin="normal"
            />

    );
}

export default SearchTextField