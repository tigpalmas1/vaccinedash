import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        width: 200,
    },
    dense: {
        marginTop: 19,
    },
    menu: {
        width: 200,
    },
}));

const currencies = [
    {
        value: 'all',
        label: 'Todos',
    },
    {
        value: 'Aberto',
        label: 'Abertos',
    },
    {
        value: 'Cancelado',
        label: 'Cancelados',
    },
    {
        value: 'Concluido',
        label: 'Concluidos',
    },
    {
        value: 'Pedido',
        label: 'Pedidos',
    },
];

const DropDownTextField =(props) => {
    const {status, setStatus}= props



    const classes = useStyles();


    const handleChange = name => event => {

        const status = event.target.value

        setStatus(status)
    };

    return (
        <form className={classes.container} noValidate autoComplete="off">

             <TextField
                id="standard-select-currency-native"
                select
                label="Procurar por Status"
                className={classes.textField}
                value={status}
                onChange={handleChange('currency')}
                SelectProps={{
                    native: true,
                    MenuProps: {
                        className: classes.menu,
                    },
                }}
                helperText="selecione um status"
                margin="normal"
            >
                {currencies.map(option => (
                    <option key={option.value} value={option.value}>
                        {option.label}
                    </option>
                ))}
            </TextField>

        </form>
    );
}

export default DropDownTextField