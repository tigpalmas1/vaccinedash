import React from 'react';
import {mainOrange, purpleTitle} from "../colors/index";
import {filterUpdateAction, } from "../../redux/actions";
import {connect} from "react-redux";
import _ from 'lodash';

class TagItem extends React.Component{




    onPress = () => {

        const {selectedTags, tag}  = this.props
        const found = _.find(selectedTags,tag);

        if(found){
            _.remove(selectedTags, found);

        }else{
            selectedTags.push(tag)
        }


        this.props.filterUpdateAction({prop: 'selectedTags', value: selectedTags})
    }


    render(){
       const  {tag, iconName, backgroundColor,fontSize} = this.props
        const {name} = tag || {}

        return (
            <div onClick={this.onPress} style={{backgroundColor: backgroundColor || mainOrange, flexWrap: 'wrap', padding:5, borderRadius: 10, marginRight: 5}}>
                <div style={{color: 'white', flexWrap: 'wrap',flex: 1, fontSize: fontSize || 8, fontWeight: '900'}}>
                    {iconName &&
                    <i class={`${iconName}  icon`}></i>
                    }

                    {name}
                </div>
            </div>

        )
    }

}


const mapStateToProps = (state) => {
    const {selectedTags, tags } = state.filter.preference
    return {
        selectedTags,
        tags
    }
}


export default connect(mapStateToProps, {
    filterUpdateAction,
})(TagItem)
