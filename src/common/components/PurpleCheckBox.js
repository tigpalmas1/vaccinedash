import React, {useState} from 'react';

import Checkbox from '@material-ui/core/Checkbox';
import { withStyles } from '@material-ui/core/styles';
import {mainOrange} from "../colors/index";


const PurpleCheckBox = withStyles({
    root: {
        color: 'grey',
        '&$checked': {
            color:mainOrange,
        },
    },
    checked: {},
})(props => <Checkbox color="default" {...props} />);

export default PurpleCheckBox