import React from 'react';
import {purpleTitle, titleColor} from "../colors/index";






const Button = ({onClick, title, color, icon, fontSize}) => {


    return (
        <div
            onClick={onClick}
            style={{display: 'flex',
                cursor: 'pointer',
                fontSize: fontSize? fontSize: 24, lineHeight: 1, flexDirection: 'row',
            alignItems: 'center', justifyContent: 'center',
            backgroundColor: color, borderRadius: 10,
            padding: 15}}>
            {icon &&
            <i class={`${icon} grey  icon`}/>
            }

            <div style={{ fontWeight: '500', color: 'white'}}>{title} </div>
        </div>
    )
}

const styles = {}

export {Button};