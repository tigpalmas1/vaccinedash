import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(0),
        marginRight: theme.spacing(1),
        width: 200,
    },
    dense: {
        marginTop: -5,
    },
    menu: {
        width: 200,
    },
}));

const currencies = [
    {
        value: 'Aberto',
        label: 'Aberto',
    },
    {
        value: 'Cancelado',
        label: 'Cancelado',
    },
    {
        value: 'Concluido',
        label: 'Concluido',
    },
    {
        value: 'Pedido',
        label: 'Pedido',
    },
];


const  DropDownOrderItem = (props) => {

    const {status, setStatus} = props

    const classes = useStyles();
    const [values, setValues] = React.useState({
        name: 'Cat in the Hat',
        age: '',
        multiline: 'Controlled',
        currency: status,
    });


    const handleChange = name => event => {
        setValues({ ...values, [name]: event.target.value });
        setStatus(event.target.value)
    };

    return (
        <form className={classes.container} noValidate autoComplete="off">

            <TextField
                id="standard-select-currency"
                select
                label="Status do Pedido"
                className={classes.textField}
                value={values.currency}
                onChange={handleChange('currency')}
                SelectProps={{
                    MenuProps: {
                        className: classes.menu,
                    },
                }}


            >
                {currencies.map(option => (
                    <MenuItem key={option.value} value={option.value}>
                        {option.label}
                    </MenuItem>
                ))}
            </TextField>


        </form>
    );
}


export default DropDownOrderItem