import {grey, purpleTitle, titleGrey} from "../colors/index";


//PERFIL STYLES
export const txtEmail = {marginTop: 5, fontSize: 14, fontWeight: '700', color: grey}
export const imgAvatar =   { height: 90,  width: 90,  borderRadius: 45,  borderWidth: 1,   borderColor: purpleTitle}
export const imgAvatarTicket =   { height: 60,  width: 60,  borderRadius: 30,  borderWidth: 1,   borderColor: 'grey'}
export const userInfoWrapper = {flex: 1,flexDirection: 'row',  padding: 20}
export const countWrapper = {flexDirection:'row', alignItems: 'center', justifyContent: 'space-between', paddingHorizontal:20}
export const txtCount = {fontSize: 18, color: titleGrey, fontWeight: '900'}
export const txtCountWrapper = {flexDirection: 'column', alignItems: 'center', justifyContent: 'center'}
export const txtCountText = {fontSize: 14, color: titleGrey,  fontWeight: '700'}
export const buttonWrapper = { flexDirection: 'row', paddingTop: 10, justifyContent: 'space-around',  borderTopWidth: 1,  borderTopColor: '#eae5e5'}
export const txtName =  { marginTop: 10,  fontSize: 18, fontWeight: '700', color: titleGrey}
export const textTitle =  {fontSize: 24,fontWeight: '700'}


//EDITPROFILE

export const imagePickerContainer =  {  }
export const imageContainer = {height: 400, width: '100%',  marginBottom: 10}
export const nameLastNameContainer = {flexDirection: 'row', width: '100%', flex: 1}
export const genderContainer = {width: '100%', marginTop: 10, }
export const titleGenderStyle = {color: titleGrey, marginLeft: 5, fontSize: 16, fontWeight: '700'}
export const pickerStyle = {paddingRight: 5, paddingLeft: 5, color: 'black',}
export const itemPickerStyle = { borderBottomWidth: 1,  borderColor: purpleTitle,}
export const errorMessageContainer = {width: '100%', alignItems: 'center', marginTop: 10}
export const errorMessageStyle = {color: 'red', fontWeight: '900', fontSize: 12, }
export const buttonWrapperEditProfile = {marginTop: 10, marginBottom: 80}