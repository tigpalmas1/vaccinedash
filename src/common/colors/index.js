export const  facebookBlue =  '#3b5998'
export const  logoBlue =   '#114C6C'
export const  lightOrange =   '#ffdd00'
export const  mainOrange =   '#69c9d7'
export const  darkOrange =   '#d77135'
export const    grey= "#eae5e5"
export const    titleGrey= "#9EA0A4"
export const    darkGrey= "#454A58"
export const    titleColor= "#1F1E24"
export const  gradientColor =   [lightOrange, mainOrange, darkOrange];

export const  lightPurple =   '#cdc5ff'
export const  purpleTitle =   '#69c9d7'
export const  darkPurple =   '#3227ff'