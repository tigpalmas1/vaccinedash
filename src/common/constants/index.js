
export const baseUrl= 'https://mt-vaccine-card.herokuapp.com'
export const noEstablishments= "Nenhum estabelecimento encontrado, tente remover alguns filtros"
export const noNews= "Nenhuma notícia no momento"



export  const establishmentsType = [
    {
        label: '',
        value: '',
    },
    {
        label: 'Balada',
        value: 'nightclub',
    },
    {
        label: 'Bar',
        value: 'bar',
    },
    {
        label: 'Casa de Show',
        value: 'showHouse',
    },
];

export  const constPhoneTypes = [
    {
        label: '',
        value: '',
    },
    {
        label: 'Comercial',
        value: 'comercial',
    },
];


export  const constHorarioType = [
    {
        label: '',
        value: '',
    },
    {
        label: 'Todos Os dias',
        value: 'todos_os_dias',
    },
    {
        label: 'Segunda',
        value: 'segunda',
    },
    {
        label: 'Terça',
        value: 'terça',
    },
    {
        label: 'Quarta',
        value: 'quarta',
    },
    {
        label: 'Quinta',
        value: 'quinta',
    },
    {
        label: 'Sexta',
        value: 'sexta',
    },
    {
        label: 'Sábado',
        value: 'sabado',
    },
    {
        label: 'Domingo',
        value: 'domingo',
    },
];

export  const constMediaTypes = [
    {
        label: '',
        value: '',
    },
    {
        label: 'Facebook',
        value: 'facebook',
    },
    {
        label: 'Instagram',
        value: 'instagram',
    },
    {
        label: 'Youtube',
        value: 'Youtube',
    },
];


export  const fixedTypeItems = [
    {
        label: 'Telha Cerâmica',
        value: 'Telha Cerâmica',
    },
    {
        label: 'Telha Fibromadeira',
        value: 'Telha Fibromadeira',
    },
    {
        label: 'Telha Fibrometal',
        value: 'Telha Fibrometal',
    },
    {
        label: 'Sem Estrutura',
        value: 'Sem Estrutura',
    },
];


export const userStatus =  [
    {
        value: 'pending',
        label: 'Pendente',
    },
    {
        value: 'approved',
        label: 'Aprovado',
    },
    {
        value: 'rejected',
        label: 'Rejeitado',
    },
]



export const oneUserType =  [
    {
        value: 'Instalador',
        label: 'Instalador',
    },
    {
        value: 'Cliente',
        label: 'Cliente',
    },
    {
        value: 'Vendedor',
        label: 'Vendedor',
    },

]


