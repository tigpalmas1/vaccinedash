import {titleGrey} from "../colors/index";
import React from 'react';

export const setStorage = async (string, object) => {
    try {
        localStorage.setItem(string, JSON.stringify(object))
    }
    catch (e) {
        console.log(e)
        return (null)
    }
}

export const getStorage = async (string) => {
    try {
        let item = await localStorage.getItem(string);
        let parsedItem = JSON.parse(item);
        return (parsedItem)
    } catch (e) {

        console.log(e)
        return (null)
    }
}

export const formataDinheiro =(value) =>{
    if(value){
        return "R$ " + value.toFixed(2).replace('.', ',').replace(/(\d)(?=(\d{3})+\,)/g, "$1.");
    }

}

export const add_zero = (your_number, length)=> {
    var num = '' + your_number;
    while (num.length < length) {
        num = '0' + num;
    }
    return num;
}




export const getMonth = (stringDate) => {
    var date = new Date(stringDate)
    const monthNames = ["JAN", "FEV", "MAR", "ABR", "MAI", "JUN",
        "JUL", "AGO", "SET", "OUT", "NOV", "DEZ"
    ];
    const month = date.getUTCMonth() + 1
    return monthNames[month - 1]
}

export const getDay = (stringDate) => {
    var date = new Date(stringDate)
    const day = (date.getUTCDate() < 10 ? '0' : '') + date.getUTCDate()
    return day
}

export const getWeekDay = (stringDate) => {
    var days = ['DOM', 'SEG', 'TER', 'QUA', 'QUI', 'SEX', 'SAB'];
    var date = new Date(stringDate)
    const day = days[date.getUTCDay()]
    return day
}


export const getBeginTime = (stringDate) => {
    var date = new Date(stringDate)
    const hours = (date.getHours() < 10 ? '0' : '') + date.getHours()
    const minutes = (date.getMinutes() < 10 ? '0' : '') + date.getMinutes()
    return `${hours}:${minutes}`
}


export const getFormatedDate = (stringDate) => {
    if (stringDate === "today") {
        return "Hoje"
    }
    if (stringDate === "week") {
        return "Essa Semana"
    }
    if (stringDate === "weekend") {
        return "Final de Semana"
    }
    var dateObj = new Date(stringDate);
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getUTCDate();
    var year = dateObj.getUTCFullYear();

    if (month != null && day != null && year != null) {
        return day + "/" + month + "/" + year;
    } else {
        return '';
    }
}


export const getExpirantionTime = (expirationDate) => {
    var finalDate = new Date(expirationDate);
    var dif = finalDate.getTime() - new Date().getTime()
    var Seconds_from_T1_to_T2 = dif / 1000
    return (Seconds_from_T1_to_T2);

}

export const getPassedTime = (expirationDate) => {
    var finalDate = new Date(expirationDate);
    var dif = new Date().getTime() - finalDate.getTime()
    var Seconds_from_T1_to_T2 = dif / 1000

    var d = Math.floor(Seconds_from_T1_to_T2 / (3600 * 24));
    var h = Math.floor(Seconds_from_T1_to_T2 % (3600 * 24) / 3600);
    var m = Math.floor(Seconds_from_T1_to_T2 % 3600 / 60);
    var s = Math.floor(Seconds_from_T1_to_T2 % 3600 % 60);

    var dDisplay = d > 0 ? d + (d == 1 ? " dia, " : " dias, ") : "";

    if (d >= 2) {
        return getFormatedDate(expirationDate)
    }
    var hDisplay = h > 0 ? h + (h === 1 ? " hora, " : " horas, ") : "";
    var mDisplay = m > 0 ? m + (m ===1 ? " minuto, " : " minutos, ") : "";
    var sDisplay = s > 0 ? s + (s === 1 ? " segundo" : " segundos") : "";
    return dDisplay + hDisplay + mDisplay + sDisplay;


}




export const checkResponseError = (e) => {
    const {response} = e || {}
    console.log(response)
    const {data} = response || {}
    const {fullResponse, message: messageData1, resMessage} = data || {}
    const {data: MessageData, message: firstMessage} = fullResponse || {}
    const {message} = MessageData || {}

    return resMessage|| messageData1|| firstMessage || resMessage || message || 'ops, algo deu errado, tente novamente'
}


export const revertData = (date) => {
    console.log(date)
    const onlyDate = date.substring(0, 10);
    console.log(onlyDate)
    return onlyDate.split('-').reverse().join('-');
}

export const revertFilterData = (date) => {

    return date.split('-').reverse().join('-');
}

export const getDistanceFromLatLonInKm = (lat1,lon1,lat2,lon2)=> {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1);
    var a =
        Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon/2) * Math.sin(dLon/2)
    ;
    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c; // Distance in km

    return d;
}

const deg2rad= (deg) =>{
    return deg * (Math.PI/180)
}


export const getFormatedDateWY = (stringDate) => {

    if (stringDate === "today") {
        return "Hoje"
    }
    if (stringDate === "week") {
        return "Essa Semana"
    }
    if (stringDate === "weekend") {
        return "Final de Semana"
    }
    var dateObj = new Date(stringDate);
    var month = dateObj.getUTCMonth() + 1; //months from 1-12
    var day = dateObj.getDate();
    var year = dateObj.getUTCFullYear();

    if (month != null && day != null && year != null) {
        return  day + "/" + month
    } else {
        return '';
    }
}

/*export const renderFooter = (objects, endlist, loading) => {
    if (objects.length > 0) {
        return (
            <View style={{padding: 10, width: '100%', alignItems: 'center'}}>
                <Text style={{fontWeight: '700', color: titleGrey}}>
                    {endlist && !loading ? "Chegou ao fim" : "Buscando mais..."}
                </Text>
            </View>
        )
    } else return null;
}*/

