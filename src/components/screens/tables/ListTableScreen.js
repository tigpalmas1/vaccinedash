import React, {useEffect, useState} from 'react';

import PaginationComponent from "../../../common/components/PaginationComponent";
import EmptyState from "../../fragments/EmptyState";
import {useDispatch, useSelector} from "react-redux";
import { fetchCardsAction} from "../../../redux/actions/card_action";
import ItemCard from "../imagesCard/ItemCard";
import DropDown from "../../../common/components/DropDown";


const ListTableScreen = (props) => {
    const dispatch = useDispatch();
    const cards = useSelector(state => state.card.cards);
    const { message,  } = props


    const [option, setOption]  = useState('Todos');
    const options =  [
        {value: 'Todos', label:'Todos'},
        {value: 'Aguardando verificação', label:'Aguardando verificação'},
        {value: 'Verificada', label:'Verificada'},
        {value: 'Imagem Ilegivel', label:'Imagem Ilegivel'},
    ]



    const [activePage, setActivePage] = useState(1)



    useEffect(() => {
        dispatch(fetchCardsAction(activePage, option));
    }, [activePage, option])






    const renderList = () => {
        return cards.map((item)=>{
            return(
                <ItemCard item={item}/>
            )
        })
    }
    return (
        <div style={{display: 'flex',flex: 1,  flexDirection: 'column'}}>

            <DropDown
                label={'Status'}
                error={''}
                value={option}
                setValue={(value) => {
                    setOption(value)
                }}
                data={options}

            />




                <div style={{  display: 'flex',flex: 1, flexDirection: 'column', }}>
                    {renderList()}

                    {cards?.length === 0 && message &&

                    <EmptyState message={message}/>
                    }

                </div>






            <div style={{marginTop: 10}}>
                <PaginationComponent
                    activePage={activePage}
                    setActivePage={(value)=>setActivePage(value)}
                />
            </div>



        </div>



    )


}



export default ListTableScreen