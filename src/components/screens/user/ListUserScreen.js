import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom'
import {connect} from "react-redux";
import {fetchUserAction, clearUserList, setUserAction} from "../../../redux/actions/user_action";
import UserItem from './UserItem'
import EmptyState from "../../fragments/EmptyState";
import './ListUserScreen.css'
import PaginationComponent from "../../../common/components/PaginationComponent";
import PermissionDialog from '../../dialogs/PermissionDialog'

const ListUserScreen = (props) => {
    const {fetchUserAction, setUserAction, users,  stateUserChanged, clearUserList, message} = props
    const [activePage, setActivePage] = useState(1)

    const [open, setOpen] = useState(false);

    useEffect(() => {
        if (stateUserChanged) {
            clearUserList()
            fetchUserAction( activePage);
        }

    }, [stateUserChanged])

    useEffect(() => {

        clearUserList()
        fetchUserAction( activePage);


    }, [activePage])


    

    const renderList = () => {
        return users.map(item => {
            return (

                <UserItem
                    setItem={()=>{
                        setOpen(true)
                        setUserAction(item)
                    }}
                    key={item.user._id}
                    item={item}/>
            )
        })
    }

    const handlePageClick = () => {

    }


    return (


        <div style={{display: 'flex', flex: 1, flexDirection: 'column', }}>


            <div style={{  display: 'flex', width: '100%', flexDirection: 'row', flexWrap: 'wrap'  }}>
                {renderList()}

                {users.length === 0 && message &&

                <EmptyState message={message}/>
                }

            </div>


            <div style={{marginTop: 10}}>
                <PaginationComponent
                    activePage={activePage}
                    setActivePage={(value)=>setActivePage(value)}
                />
            </div>

            <div style={{display: 'flex', flexDirection: 'row', justifyContent:'flex-end', marginTop:10}}>
                <PermissionDialog
                    setOpen={()=>setOpen(false)}
                    open={open}

                />

            </div>

        </div>



    )

};

const mapStateToProps = (state) => {


    return {
        users: state.user.users,
        userFilter: state.filter.userFilter,
        stateUserChanged: state.filter.stateUserChanged,
        message: state.user.message,
        endList: state.user.endList
    }
}


export default connect(mapStateToProps, {fetchUserAction, clearUserList, setUserAction})(ListUserScreen)