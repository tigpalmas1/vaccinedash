import React from 'react';
import {NavLink, Route, BrowserRouter} from 'react-router-dom'
import ListUserScreen from "./ListUserScreen";
import CreateUserScreen from "./CreateUserScreen";
import AppBar from '../../../common/components/AppBar'


const UserContainerScreen  = ({ match })=>{

    return(


        <div style={{display: 'flex', flex: 1, flexDirection: 'column'}}>
            <AppBar title={'Usuários'}/>

            <div style={{display: 'flex', flex: 1, flexDirection: 'column', padding: 20}}>
                <Route path={'/contact'} exact  component={ListUserScreen}/>
                <Route path={'/contact/create'} exact component={CreateUserScreen}/>

            </div>




        </div>


    )
}

export default UserContainerScreen;