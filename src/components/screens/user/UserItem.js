import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {titleColor} from "../../../common/colors/index";

import {connect} from "react-redux";
import {changeUserStatusAction} from "../../../redux/actions";

import Button from '@material-ui/core/Button';


const useStyles


    = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: 10,
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
}));


const UserItem = (props) => {
    const {item,  setItem} = props || {}
    const classes = useStyles();

    const {user, permission} = item ||{}


    const {name, lastName,  email, } = user || {}

    const {isAdmin,isManager, isNurse} = permission || {}
      const {textStyle} = styles



    return (
        <div className={classes.root}>
            <ExpansionPanel>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon/>}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <div style={{display: 'flex', flexDirection: 'row', fontWeight: '700', color: titleColor}}>
                        <div style={textStyle}>{name} {lastName}</div>
                        <div style={textStyle}>{isAdmin ? 'Administrador': '        '}</div>
                        <div style={textStyle}>{isManager ? 'Diretor': '        '}</div>
                        <div style={textStyle}>{isNurse ? 'Aplicador': '        '}</div>
                    </div>

                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div style={{display: 'flex', flexDirection: "column"}}>

                        <div style={{display: 'flex', flexDirection: 'row',}}>




                        </div>
                        <div style={{display: 'flex', flexDirection: 'row'}}>
                            <div style={{display: 'flex1', flexDirection: 'column'}}>
                                <div style={{display: 'flex', flexDirection: 'row',}}>
                                    <div style={{marginRight: 5}}>{name}</div>
                                    <div style={{marginRight: 5}}>{lastName}</div>
                                </div>

                                <div style={{display: 'flex', flexDirection: 'row',}}>
                                    <div style={{marginRight: 5}}>{email}</div>


                                </div>


                            </div>


                        </div>

                        <Button variant="outlined" color="primary" onClick={setItem}>
                            Permissões
                        </Button>


                    </div>

                </ExpansionPanelDetails>
            </ExpansionPanel>

        </div>
    );
}

const styles = {
    textStyle: {
        marginRight: 10,
        width: 150,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
}


const mapStateToProps = (state) => {

    return {}
}


export default connect(mapStateToProps, {changeUserStatusAction})(UserItem)
