import React, {useState} from 'react';
import {Link} from 'react-router-dom'
import TextField from '@material-ui/core/TextField';
import { MuiPickersUtilsProvider, DateTimePicker } from "material-ui-pickers";
import DateFnsUtils from "@date-io/date-fns";
import MapFragment from "../../fragments/MapFragment";


const CreateUserScreen  = (props)=>{
    const [selectedDate, handleDateChange] = useState(new Date());


    return(
        <div >


            <div style={{flexDirection: 'row', alignItems: 'center', display: 'flex', padding: 20}}>
                <TextField
                    id="standard-name"
                    label="Name"
                    value={null}
                    onChange={()=>{}}
                    margin="normal"
                />

                <TextField
                    style={{marginLeft: 20}}
                    id="standard-name"
                    label="Descrição"
                    value={null}
                    onChange={()=>{}}
                    margin="normal"
                    multiline

                />

                <MuiPickersUtilsProvider style={{marginTop:80, marginLeft: 20}} utils={DateFnsUtils}>
                    <DateTimePicker
                        value={null}
                        disablePast
                        ampm={false}
                        onChange={(value)=>{alert(value)}}
                        label="Inicio"
                        showTodayButton
                    />
                </MuiPickersUtilsProvider>
            </div>

            <div>




            </div>



        </div>
    )
}

export default CreateUserScreen;