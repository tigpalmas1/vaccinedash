import React, {useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {fetchCovidVaccination, postScheduleCovid, scheduleUpdate} from "../../../redux/actions";

import {Button} from "../../../common/components/Button";
import {purpleTitle} from "../../../common/colors";
import TextField from "@material-ui/core/TextField";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import DropDown from "../../../common/components/DropDown";


const SaveGroupVaccination = () => {
    const dispatch = useDispatch();
    const schedule = useSelector(state => state.covid.schedule);
    const {city, group, date, description, age} = schedule ||{}
    const [saving, setSaving] = useState(false)
    const filter = useSelector(state => state.covid.filter);
    const { cities} = filter ||{}

   const [groupError, setGroupError] = useState();
   const [descriptionError, setDescriptionError] = useState();
   const [dateError, setdateError] = useState();


    const handleSave= ()=>{
        if(!checkError()){
            setSaving(true)
            dispatch(postScheduleCovid(schedule, callback))
        }

    }

    const checkError = () => {
        let errors = false;

        if (!group) {
            setGroupError('Grupo Necessário')
            errors = true
        }
        if (!description) {
            setDescriptionError( 'Grupo Necessário')
            errors = true
        }    if (!date) {
            setdateError( 'Data Necessário')
            errors = true
        }




        return errors
    }

    const callback = (sucess, message)=>{
        setSaving(false)
        if(sucess){
            dispatch(fetchCovidVaccination(city))
            dispatch(scheduleUpdate({prop: 'group', value: '',}))
            dispatch(scheduleUpdate({prop: 'date', value: null,}))
            dispatch(scheduleUpdate({prop: 'age', value: '',}))
            dispatch(scheduleUpdate({prop: 'description', value: '',}))
        }
        alert(message);
    }



    return (
        <div style={{flex: 1, padding: 20, backgroundColor: '#f8f8f8', borderRadius: 10}}>
            <div style={{fontSize: 18}}>Criar Grupo de vacinação</div>

            <div style={{display:'flex', flexDirection: 'row', }}>
                <DropDown
                    error={''}
                    value={city}
                    setValue={(value) => {

                        dispatch(scheduleUpdate({prop: 'city', value: value,}))
                    }}
                    data={cities}

                />


            <div style={{ marginRight: 20}}>
                <MuiPickersUtilsProvider style={{
                    marginTop: 110,
                }}
                                         utils={DateFnsUtils}>
                    <KeyboardDatePicker
                        value={date}
                        id="time-picker"
                        error={dateError}

                        ampm={false}

                        helperText={dateError}

                        onChange={(value) => {
                            setdateError(null)
                            dispatch(scheduleUpdate({prop: 'date', value:value}))
                        }}
                        label="Data de Vacinação"

                    />
                </MuiPickersUtilsProvider>
            </div>

            </div>


            <div style={{display:'flex', flexDirection: 'row', }}>
                <TextField
                    InputLabelProps={{ shrink: true }}
                    label="Grupo"
                    error={groupError}
                    helperText={groupError}
                    className={{marginLeft: 10, marginRight: 10,}}
                    style={{width: '100%', marginRight: 10}}
                    value={group}
                    onChange={(e) => {
                       setGroupError(null)
                        dispatch(scheduleUpdate({prop: 'group', value: e.target.value,}))
                    }}
                    margin="normal"
                />




                <TextField
                    InputLabelProps={{ shrink: true }}
                    label="Idade (Opcional)"
                    /*    error={nameError}
                        helperText={nameError}*/
                    className={{marginLeft: 10, marginRight: 10,}}
                    style={{width: '100%', marginRight: 10}}
                    value={age}
                    onChange={(e) => {

                        dispatch(scheduleUpdate({prop: 'age', value: e.target.value,}))
                    }}
                    margin="normal"
                />
            </div>

            <div style={{display:'flex', flexDirection: 'row',}}>


                <TextField
                    InputLabelProps={{ shrink: true }}
                    label="Descrição"
                        error={descriptionError}
                        helperText={descriptionError}
                    className={{marginLeft: 10, marginRight: 10,}}
                    style={{width: '100%', marginRight: 10}}
                    value={description}
                    onChange={(e) => {
                        setDescriptionError(null)
                        dispatch(scheduleUpdate({prop: 'description', value: e.target.value,}))
                    }}
                    margin="normal"
                />
            </div>

            <div style={{display: 'flex', flexDirection: 'row', marginTop: 20}}>
            <Button
                fontSize={14}
                color={saving? '#ccc':purpleTitle}
                title={saving? 'Salvando aguarde':'Salvar'}
                onClick={handleSave}/>
            </div>
        </div>
    )
}

export default SaveGroupVaccination;