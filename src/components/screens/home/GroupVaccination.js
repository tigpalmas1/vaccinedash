import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";
import {deleteScheduleAction, fetchCovidVaccination, filterUpdate,} from "../../../redux/actions";
import {getFormatedDate} from "../../../common/functions";
import DropDown from "../../../common/components/DropDown";
import PaginationComponent from "../../../common/components/PaginationComponent";


const GroupVaccinationFragment = () => {
    const dispatch = useDispatch();
    const data = useSelector(state => state.covid.data);
    const filter = useSelector(state => state.covid.filter);
    const {city, cities} = filter || {}
    const {value} = city || {}

    const [activePage, setActivePage] = useState(1);


    console.log(filter)


    useEffect(() => {
        dispatch(fetchCovidVaccination(city, activePage))
    }, [city,activePage])


    const handleDelete = (item) => {
        dispatch(deleteScheduleAction(item._id, callback))
    }

    const callback = (success, message) => {
        if (success) {
            dispatch(fetchCovidVaccination(city, activePage))
        }
    }

    const Group = ({item}) => {
        const {group, descrption, date, age} = item
        return (
            <div style={{padding: 5}}>
                <div style={{display: 'flex', flexDirection: 'row'}}>
                    <div style={{marginRight: 5, fontSize: 20}}>{group}</div>
                    <div style={{marginRight: 5}}>{age}</div>
                    <div>{getFormatedDate(date)}</div>

                    <div
                        onClick={() => handleDelete(item)}
                        style={{paddingHorizontal: 5, cursor: 'pointer', marginLeft: 20}}> x
                    </div>


                </div>

                <div>{descrption}</div>
            </div>
        )
    }

    const renderList = () => {
        return data.map((item) => {
            return (
                <Group item={item}/>
            )
        })
    }

    return (
        <div style={{flex: 1,display: 'flex',flexDirection: 'column', padding: 20, backgroundColor: '#f8f8f8', borderRadius: 10}}>
            <div style={{display: 'flex', flexDirection: 'row'}}>
                <div style={{fontSize: 18}}>Grupos de Vacinação</div>

                <div style={{marginTop: -20, marginLeft: 20}}>
                    <DropDown
                        error={''}
                        value={value}
                        setValue={(value) => {
                            setActivePage(1);
                            dispatch(filterUpdate({prop: 'city', value: value,}))
                        }}
                        data={cities}

                    />
                </div>
            </div>
            <div style={{flex: 1, }}>
                {renderList()}
            </div>

            <div style={{marginTop: 10}}>
                <PaginationComponent
                    activePage={activePage}
                    setActivePage={(value)=>setActivePage(value)}
                />
            </div>
        </div>
    )
}

export default GroupVaccinationFragment;