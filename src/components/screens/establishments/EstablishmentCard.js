import React from 'react';
import {makeStyles, withStyles} from '@material-ui/core/styles';
import Switch from '@material-ui/core/Switch';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom'
import history from '../../../history'
import {editEstablishmentAction, setActiveEstablishmentAction} from '../../../redux/actions'

import Card from '@material-ui/core/Card';

import CardMedia from '@material-ui/core/CardMedia';

import {lightPurple, purpleTitle} from "../../../common/colors/index";

import CardActionArea from '@material-ui/core/CardActionArea';


const useStyles = makeStyles({
    card: {
        maxWidth: 200,
    },
    media: {
        height: 150,
    },
});


const PurpleSwitch = withStyles({
    switchBase: {
        color: lightPurple,
        '&$checked': {
            color: purpleTitle,
        },
        '&$checked + $track': {
            backgroundColor: lightPurple,
        },
    },
    checked: {},
    track: {},
})(Switch);


const EstablishmentCard = (props) => {

    const classes = useStyles();


    const {openDialog, alertMessage} = props;
    const {width, height, route, establishment, setActiveEstablishmentAction, editEstablishmentAction} = props;
    const { _id,imageId,  addressId, name} = establishment || {}
    const { smallUrl} = imageId || {}
    const {cityId} = addressId || {}

    const {index} = props








    const callback = (message) => {
        alertMessage(message)
    }


    return (


        <Link
            style={{maxWidth: 200, maxHeight: 150}}
            to={`/establishments/show/${_id}`}
            onClick={() => {
                setActiveEstablishmentAction(establishment)
                history.push(route)
            }}

            key={_id} className="card">
            <Card style={{position: 'relative'}} className={classes.card}>
                <CardActionArea>
                    <CardMedia
                        className={classes.media}
                        image={smallUrl? smallUrl : 'https://www.camilaporto.com.br/wp-content/plugins/accelerated-mobile-pages/images/SD-default-image.png'}
                        title={name}
                    />
                </CardActionArea>
            </Card>

        </Link>)


}


export default connect(null, {setActiveEstablishmentAction, editEstablishmentAction})(EstablishmentCard);
