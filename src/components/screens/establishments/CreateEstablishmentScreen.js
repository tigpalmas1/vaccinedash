import React, {useEffect, useState} from 'react';

import {connect} from "react-redux";
import TextField from '@material-ui/core/TextField';
import {
    editEstablishmentAction,
    establishmentUpdateAction,
    establishmentUpdateImage,
    saveEstablishmentAction,
    setEditEstablishmentAction
} from "../../../redux/actions";
import {subTitleStyle} from "../../../common/styles";
import {FilePicker} from 'react-file-picker'
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import _ from 'lodash'

import MapNewFragment from '../../fragments/MapNewFragment'
import {mainOrange, titleColor} from "../../../common/colors/index";
import AppBar from '../../../common/components/AppBar'
import {withRouter} from 'react-router-dom'
import DropDown from '../../../common/components/DropDown'
import history from '../../../history'
import PhoneFragment from "./fragment/PhoneFragment";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import {withStyles} from "@material-ui/core/styles";
import Checkbox from "@material-ui/core/Checkbox";
import {getBase64File} from "../../../common/functions/imageUtil";

const PurpleCheckBox = withStyles({
    root: {
        color: 'grey',
        '&$checked': {
            color:mainOrange,
        },
    },
    checked: {},
})(props => <Checkbox color="default" {...props} />);

const CreateEstablishmentScreen = (props) => {

    const {location} = props || {}
    const {pathname} = location || {}



    const {setEditEstablishmentAction, establishmentUpdateAction, establishmentUpdateImage,
        editEstablishmentAction,
        saveEstablishmentAction,} = props
    const {newEstablishment,  activeEstablishment, users} = props
    const [saving, setSaving] = useState(false)



    const {
       name, phone, email, cnpj,  principalId,  address, imageId,isPartner,regCode
    } = newEstablishment || {}
    const {smallUrl} = imageId||{}

    const {city} = address? address.length > 0? address[0] :{}:{}




    var reducedUsers = _.map(users, function (obj) {
        const {user} = obj || {}
        const {name, _id, lastName} = user || {}
        return {
            value: _id,
            label: `${name} ${lastName}`
        }
    });

    reducedUsers.unshift({value: '', label: 'Selecione'})



    const [nameError, setNameError] = useState('')
    const [emailError, setEmailError] = useState('')
    const [cityError, setCityError] = useState('')
    const [bannerError, setBannerError] = useState('')
    const [phoneError, setPhoneError] = useState('')



    useEffect(() => {
        if (activeEstablishment) {
            setEditEstablishmentAction(activeEstablishment)
        }



    }, [])


    const setFile = async (fileObject,) => {
        const base64 =await  getBase64File(fileObject)
        var path = (window.URL || window.webkitURL).createObjectURL(fileObject);
        establishmentUpdateImage( path)
        establishmentUpdateAction({prop: 'localBanner',  value: fileObject, })
        establishmentUpdateAction({prop: 'image64',  value: base64, })
        if (name === 'mainCover') {
            setBannerError('')
        }

    }

    const setError = (error) => {
        alert(error)
    }



    const saveEstablishment = () => {

        if (!checkError()) {
            setSaving(true)
           if(activeEstablishment){
                console.log('entrou edit')
               editEstablishmentAction(newEstablishment,  callback)
           }else{
               console.log('entrou save')
               saveEstablishmentAction(newEstablishment, activeEstablishment, callback)
           }
            //
        }

    }

    const callback = (success, message) => {
        setSaving(false)
        alert(message)
        if(success){
            history.push("/establishments");

        }
    }

    const checkError = () => {
        let errors = false;

        if (!name) {
            setNameError('Nome obrigátorio')
            errors = true
        }
        if (!email) {
            setEmailError('Email obrigátorio')
            errors = true
        }
        if (email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
            setEmailError('Email está no formato incorreto')
            errors = true
        }

        if (!phone) {
            setPhoneError('Campo obrigátorio')
            errors = true
        }
        if (!city) {
            setCityError('Campo obrigátorio')
            errors = true
        }



        return errors
    }

    return (
        <div style={{display: 'flex', flex: 1, flexDirection: 'column'}}>
            {pathname === '/editEstablishment' &&
            <AppBar title={'Editar Informações'}/>
            }

            <div style={{display: 'flex', flex: 1, padding: pathname === '/editEstablishment'? 20: 0,flexDirection: 'column'}}>
                <div style={{display: 'flex', width: '100%', flexDirection: 'row'}}>
                    {/*INFORMAÇÔES*/}
                    <div style={{display: 'flex', flex: 1, flexDirection: 'column', paddingRight: 20, paddingLeft: 20}}>
                        <div style={subTitleStyle}>Informações da Unidade</div>



                        <div style={{display: 'flex', flexDirection: 'row', marginTop: 10}}>
                            <FormControlLabel
                                control={
                                    <PurpleCheckBox
                                        checked={isPartner? isPartner: false}
                                        onChange={() => {
                                            establishmentUpdateAction({prop: 'isPartner', value: !isPartner})
                                        }}
                                        value="checkedI"
                                    />
                                }
                                label="Unidade Parceira ?"
                            />
                        </div>
                        <div style={{display: 'flex', flexDirection: 'row'}}>
                            <TextField
                                id="standard-basic"
                                label="Nome da Unidade"
                                error={nameError}
                                helperText={nameError}
                                className={{marginLeft: 10, marginRight: 10,}}
                                style={{width: '100%', marginRight: 10}}
                                value={name}
                                onChange={(e) => {
                                    setNameError('')
                                    establishmentUpdateAction({prop: 'name', value: e.target.value,})
                                }}
                                margin="normal"
                            />

                            <TextField
                                id="standard-basic"
                                label="Código de registro"


                                className={{marginLeft: 10, marginRight: 10,}}
                                style={{width: '100%', marginRight: 10}}
                                value={regCode}
                                onChange={(e) => {

                                    establishmentUpdateAction({prop: 'regCode', value: e.target.value,})
                                }}
                                margin="normal"
                            />

                        </div>

                        <div style={{display: 'flex', flexDirection: 'row'}}>

                            <TextField
                                id="standard-basic"
                                label="CNPJ"
                                error={''}
                                helperText={''}
                                className={{marginLeft: 10, marginRight: 10,}}
                                style={{width: '100%', marginRight: 10}}
                                value={cnpj}
                                onChange={(e) => {
                                    establishmentUpdateAction({prop: 'cnpj', value: e.target.value,})
                                }}
                                margin="normal"
                            />



                            <TextField
                                id="standard-basic"
                                label="Email"
                                error={emailError}

                                helperText={emailError}
                                className={{marginLeft: 10, marginRight: 10,}}
                                style={{width: '100%', marginRight: 10}}
                                value={email}
                                onChange={(e) => {
                                    setEmailError('')
                                    establishmentUpdateAction({prop: 'email', value: e.target.value,})
                                }}
                                margin="normal"
                            />
                        </div>




                        <div style={{marginTop: -10, marginLeft: -10}}>
                            <DropDown
                                error={''}
                                value={principalId}
                                setValue={(value) => {
                                    establishmentUpdateAction({prop: 'principalId', value: value,})
                                }}
                                data={reducedUsers}
                                label="Selecionar o  Responsável"
                            />
                        </div>



                    </div>

                    {/*BANNER*/}
                    <div style={{
                        display: 'flex',
                        flex: 1,
                        flexDirection: 'column',
                        paddingRight: 10,
                        paddingLeft: 20,
                    }}>
                        <div style={subTitleStyle}>Imagem da Unidade</div>
                        <div style={{padding: 20}}>

                            <FilePicker
                                style={{borderWidth: 1, borderColor: 'red'}}
                                maxSize={500000}
                                extensions={['png', 'jpeg', 'jpg', 'gif']}
                                onChange={FileObject => (setFile(FileObject))}
                                onError={errMsg => (setError(errMsg))}>
                                <Card style={{
                                    display: 'flex',
                                    flex: 1,
                                    borderColor: bannerError ? 'red' : titleColor,
                                    width: 350,
                                    height: 250,
                                    borderWidth: 1,
                                    flexDirection: 'column',
                                    borderRadius: 10,
                                }}>
                                    <div
                                        style={{
                                            flex: 1,
                                            borderRadius: 10, borderColor: bannerError ? 'red' : titleColor,
                                            position: "relative", display: 'flex',
                                        }}>
                                        <CardMedia
                                            style={{
                                                flex: 1,
                                                borderRadius: 10,
                                                borderColor: bannerError ? 'red' : titleColor,
                                            }}
                                            image={smallUrl}
                                        />


                                    </div>
                                </Card>
                            </FilePicker>




                        </div>

                    </div>


                </div>

                <PhoneFragment newEstablishment={newEstablishment}/>



                <div style={{
                    display: 'flex',
                    width: '100%',
                    flexDirection: 'row',
                    paddingRight: 20,
                    paddingLeft: 10,
                    paddingTop: 20,
                    marginBottom: 80
                }}>

                    <div style={{flex: 6,}}>
                        <MapNewFragment
                            setCityError={() => setCityError('')}
                            cityError={cityError}
                            address={address}
                            updateUseAlternateAddress={() => alert('implementar')}/>

                    </div>
                    <div style={{flex: 4,}}/>


                </div>
                <div
                    onClick={() => saving ? null : saveEstablishment()}
                    style={{
                        position: 'fixed',
                        bottom: 50, right: 50,
                        fontSize: 22,
                        padding: 20, borderRadius: 10,
                        color: 'white',
                        fontWeight: '700', alignItems: 'center', justifyContent: 'center',
                        backgroundColor: saving ? 'grey' : mainOrange
                    }}>
                    {saving ? 'Salvando, aguarde' : 'Salvar'}
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = (state) => {


    return {
        users: state.user.users,
        newEstablishment: state.establishment.newEstablishment,
        preference: state.filter.preference,
        activeEstablishment: state.establishment.activeEstablishment

    }
}


export default connect(mapStateToProps, {
    establishmentUpdateAction,
    editEstablishmentAction,
    establishmentUpdateImage,
    setEditEstablishmentAction,
    saveEstablishmentAction
})(withRouter(CreateEstablishmentScreen))
