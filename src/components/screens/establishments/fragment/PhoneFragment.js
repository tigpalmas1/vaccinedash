import React, {useState} from 'react';

import {connect} from "react-redux";
import PhoneItem from "./PhoneItem";
import Tag from "../../../../common/components/Tag";
import {addPhoneAction, removePhoneAction, phoneUpdateAction} from "../../../../redux/actions/establishments_action";
import {titleColor} from "../../../../common/colors/index";


const PhoneFragment = (props) => {
    const {addPhoneAction, removePhoneAction, phoneUpdateAction} = props || {}

    const {newEstablishment, error, } = props || {}
    const {phone} = newEstablishment || {}


    const [, forceUpdate] = useState(0);

    const addPhone = () => {

        addPhoneAction({type: '', areaCode: '', number: ''})

    }

    const removePhone = (index) => {
        removePhoneAction(index)
    }

    const setItemValue = (value, index) => {
        phoneUpdateAction(value, index)
    }

    return (
        <div style={{width: '100%'}}>

            <div style={{flexDirection: 'row', display: 'flex', alignItems: 'center',}}>

                <div style={{fontSize: 18, color: error ? 'red' : titleColor, marginTop: 20, fontWeight: '700'}}>
                    Telefones
                </div>


                <div style={{marginLeft: 20, marginTop: 20}}>
                    <Tag onClick={() => {
                        addPhone()
                    }}
                         fontSize={8} title={'Adicionar'} iconName={"plus"}/>
                </div>
            </div>

            {error &&
            <div style={{fontSize: 14, color: 'red', marginTop: 8,}}>{error}</div>
            }


            <div>
                {phone?.map((item, index) => (
                    <PhoneItem
                        setItemValue={(value) => setItemValue(value, index)}
                        removePhone={() => removePhone(index)}
                        item={item}/>
                ))}
            </div>


        </div>
    )
}

const styles = {}


const mapStateToProps = (state) => {


    return {}
}


export default connect(mapStateToProps, {addPhoneAction, removePhoneAction, phoneUpdateAction})(PhoneFragment)
