import React,{useState} from 'react';
import TextField from '@material-ui/core/TextField';
import Tag from "../../../../common/components/Tag";
import DropDown from '../../../../common/components/DropDown'
import {constPhoneTypes} from "../../../../common/constants/index";




const PriceItem = (props) => {
    const {item, removePhone, setItemValue} = props ||{}
    const {type, areaCode, number} = item ||{}


    return (
        <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
            <div style={{width: 200, marginTop: -5,}}> <DropDown
                value={type}
                setValue={(value)=>{
                    setItemValue({prop: 'type', value: value,})
                }}
                data={constPhoneTypes}
                label="TIpo de Telefone"
            /></div>
            <TextField
                id="standard-basic"
                label="DDD"
                error={''}
                helperText={''}
                placeholder="Digite o Valor"

                style={{width: 50, marginRight: 10,}}
                value={areaCode}
                onChange={(e) => {
                    setItemValue({prop: 'areaCode', value: e.target.value, })
                }}
                margin="dense"
            />
            <TextField
                id="standard-basic"
                label="Número"
                error={''}
                helperText={''}
                placeholder="Ex: Cover, administração etc"

                style={{width: 150, marginRight: 10}}
                value={number}
                onChange={(e) => {
                    setItemValue({prop: 'number', value: e.target.value, })
                }}
                margin="dense"
            />



            <div>
                <Tag onClick={removePhone}
                     backgroundColor={'red'}
                     fontSize={8} title={''} iconName={"remove"}/>
            </div>

        </div>
    )
}

const styles = {}

export default PriceItem;