import React from 'react';
import {NavLink, Route, withRouter} from 'react-router-dom'

import CreateEstablishmentScreen from "./CreateEstablishmentScreen";
import EstablishmentsListScreen from "./EstablishmentsListScreen";
import {mainOrange} from "../../../common/colors/index";
import AppBar from '../../../common/components/AppBar'


const EstablishmentsContainer = (props) => {
    const {location} = props||{}
    const {pathname} = location ||{}
    console.log(pathname)
    const title = pathname === '/establishments'? 'Unidades': 'Criar Nova Unidade'

    return (


        <div style={{display: 'flex', flex: 1,  flexDirection: 'column'}}>
            <AppBar title={title}/>
            <div style={{display: 'flex', flex: 1, padding: 20, flexDirection: 'column'}}>



                <Route path={'/establishments'} exact component={EstablishmentsListScreen}/>
                <Route path={'/establishments/create'} exact component={CreateEstablishmentScreen}/>

            </div>


            { (pathname === '/establishments') &&
                <NavLink
                to={'/establishments/create'}
                style={{
                position: 'fixed',
                bottom: 50, right: 50,
                fontSize: 22,
                padding: 20, borderRadius: 10,
                color: 'white',
                fontWeight: '700', alignItems: 'center', justifyContent: 'center',
                backgroundColor:   mainOrange
            }}>
                {  'Nova Unidade'}
                </NavLink>
            }


        </div>


    )
}

export default withRouter(EstablishmentsContainer);