import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import {clearEstablishmentList, fetch_establishments} from "../../../redux/actions";
import EstablishmentCard from './EstablishmentCard'
import EmptyState from "../../fragments/EmptyState";
import AlertDialog from '../../../common/components/AlertDialog'
import EditEstablishmentDialog from '../../dialogs/EditEstablishmentDialog'
import SnackBar from '../../../common/components/SnackBar'


const EstablishmentsListScreen = (props) => {
    const {fetch_establishments, clearEstablishmentList} = props||{}
    const { preference, establishments, message} = props || {}
    const [filterDialog, setfilterDialog] = useState(false)
    const [establishmentDialog, setestablishmentDialog] = useState(false)
    const [establishment] = useState({})
    const [openSnack, setOpenSnack] = useState(false)
    const [snackMessage, setSnackMessage] = useState(false)

    const { selectedCity} = preference||{}


    useEffect(() => {
        clearEstablishmentList()
        fetch_establishments(0, 50, preference)
    }, [selectedCity])



    const renderList = () => {
        return establishments.map(item => {

            return (

                <div
                    key={item._id}
                    style={{marginTop: 10,
                        height: 170, width: 200,
                        overflow: 'hidden',
                        marginRight: 10}}>


                    <EstablishmentCard
                        alertMessage={(message)=>{
                            setSnackMessage(message)
                            setOpenSnack(true)}

                        }
                        establishment={item}/>
                    <div style={{fontWeight: '700', fontSize: 16}}>{item.name}</div>


                </div>



            )
        })
    }
    return (
        <div style={{display: 'flex', flex: 1, flexDirection: 'column'}}>



            <div style={{  display: 'flex', width: '100%', flexDirection: 'row', flexWrap: 'wrap'  }}>
                {renderList()}

                {establishments.length === 0 && message &&

                <EmptyState message={message}/>
                }

            </div>


            <EditEstablishmentDialog
                establishment={establishment}
                handleClose={() => setestablishmentDialog(false)}
                open={establishmentDialog}/>



            <AlertDialog
                hasCity={true}
                onDialogDismiss={() => setfilterDialog(false)}
                isDialogOpen={filterDialog}/>


            <SnackBar
                message={snackMessage}
                open={openSnack}
                setOpen={(status)=>setOpenSnack(status)}
            />

        </div>



    )


}

const mapStateToProps = (state) => {


    return {
        establishments: state.establishment.establishments,
        message: state.establishment.message,
        preference: state.filter.preference,
    }
}


export default connect(mapStateToProps, {fetch_establishments, clearEstablishmentList})(EstablishmentsListScreen)