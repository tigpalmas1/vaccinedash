import React from 'react';
import {NavLink, Route, BrowserRouter} from 'react-router-dom'

import AppBar from '../../../common/components/AppBar'
import ListNurseScreen from "./ListNurseScreen";
import CreateNurseScreen from "./CreateNurseScreen";
import {purpleTitle} from "../../../common/colors/index";
import {setNurseAction} from "../../../redux/actions";
import {connect} from "react-redux";

const NurseContainerScreen  = (props)=>{
    const {location, setNurseAction} = props||{}
    const {pathname} = location ||{}

    return(


        <div style={{display: 'flex', flex: 1, flexDirection: 'column'}}>
            <AppBar title={'Aplicadores'}/>

            <div style={{display: 'flex', flex: 1, flexDirection: 'column', padding: 20}}>
                <Route path={'/nurse'} exact  component={ListNurseScreen}/>
                <Route path={'/nurse/create'} exact component={CreateNurseScreen}/>

            </div>

            { (pathname === '/nurse') &&
            <NavLink
                to={'/nurse/create'}
                onClick={()=>{

                    setNurseAction(null)
                }}
                style={{
                    position: 'fixed',
                    bottom: 50, right: 50,
                    fontSize: 22,
                    padding: 20, borderRadius: 10,
                    color: 'white',
                    fontWeight: '700', alignItems: 'center', justifyContent: 'center',
                    backgroundColor:   purpleTitle
                }}>
                {  'Novo Aplicador'}
            </NavLink>
            }




        </div>


    )
}



const mapStateToProps = (state) => {


    return {



    }
}


export default connect(mapStateToProps, {
    setNurseAction,
})(NurseContainerScreen)
