import React, {useState, useEffect} from 'react';
import DropDown from '../../../common/components/DropDown'
import _ from 'lodash';
import {connect} from "react-redux";
import {withRouter} from 'react-router-dom'
import MultipleSelect from '../../../common/components/MultipleSelect'
import {nurseUpdateAction, saveNurseAction, updateNurseAction, deleteNurseAction} from "../../../redux/actions";
import HorarionFragment from './fragment/HorarioFragment';
import {purpleTitle} from "../../../common/colors/index";


const CreateNurseScreen  = (props)=>{

    const {nurseUpdateAction, saveNurseAction, updateNurseAction, deleteNurseAction} = props ||{}
    const {newNurse, users, establishments,vacinesGlobal, } = props ||{}
    const {userId, laboratory,vaccines,_id,  } = newNurse ||{}
    const {name  } = userId ||{}
    const [saving, setSaving] = useState(false);
    const [deleting, setDeleting] = useState(false);




    useEffect(()=>{
       if(_id){
         //  nurseUpdateAction({prop: 'userId', value: userId._id,})
           nurseUpdateAction({prop: 'laboratory', value: laboratory._id,})

       }
    },[])



    var reducedVaccines = _.map(vacinesGlobal, function (obj) {
        const {name} = obj || {}
        return name
    });

    var reducedSelectedVaccines = _.map(vaccines, function (obj) {
        const {name} = obj || {}
        return name
    });






    var reducedUsers = _.map(users, function (obj) {
        const {user} = obj || {}
        const {name, _id, lastName} = user || {}
        return {
            value: _id,
            label: `${name} ${lastName}`
        }
    });


    var reducerLabs = _.map(establishments, function (obj) {

        const {name, _id,} = obj || {}
        return {
            value: _id,
            label: `${name} `
        }
    });
    reducedUsers.unshift({value: '', label: 'Selecione'})
    reducerLabs.unshift({value: '', label: 'Selecione'})

    const getVaccines = (selectedTags) => {

        const newArray = []
        for (var i in vacinesGlobal) {
            const objectI = vacinesGlobal[i];

            for (var j in selectedTags) {
                const objectJ = selectedTags[j];
                if (objectI.name === objectJ) {
                    newArray.push(objectI)
                }
            }
        }

        nurseUpdateAction({prop: 'vaccines', value: newArray,})
    }

    const saveNurse = ()=>{
        setSaving(true);
        if(_id){
            updateNurseAction(newNurse, callback)
        }else{
            saveNurseAction(newNurse, callback);

        }
    }


    const deleteNurse = ()=>{
        setDeleting(true)
        deleteNurseAction(_id,callback );
    }
    const callback = (message)=>{
        setDeleting(false)
        setSaving(false)
        alert(message);
    }

    return(
        <div >
            <div style={{fontSize: 24, fontWeight: '700'}}>{name}</div>


            <div style={{flexDirection: 'row', alignItems: 'center', display: 'flex', padding: 20}}>



                {!_id &&
                <div style={{marginTop: -10, marginLeft: -10}}>
                    <DropDown
                        error={''}
                        value={userId}
                        setValue={(value) => {
                            nurseUpdateAction({prop: 'userId', value: value,})
                        }}
                        data={reducedUsers}
                        label="Selecionar o  Usuário"
                    />
                </div>
                }


                <div style={{marginTop: -10, marginLeft: -10}}>
                    <DropDown
                        error={''}
                        value={laboratory}
                        setValue={(value) => {
                            nurseUpdateAction({prop: 'laboratory', value: value,})
                        }}
                        data={reducerLabs}
                        label="Selecione o  Laboratório"
                    />
                </div>

                <MultipleSelect
                    style={{width: 300}}
                    tags={reducedVaccines}
                    selectedTags={reducedSelectedVaccines}
                    label={'Vacinas '}
                    setTags={(value) => {
                        getVaccines(value)
                    }}
                />
            </div>

            <div style={{flexDirection: 'row', alignItems: 'center', display: 'flex', padding: 20}}>
                <HorarionFragment newNurse={newNurse}/>


            </div>

            <div>




            </div>

            {_id &&
            <div
                onClick={() => deleting ? null : deleteNurse()}
                style={{
                    position: 'fixed',
                    top: 50, right: 50,
                    fontSize: 16,
                    padding: 10, borderRadius: 10,
                    color: 'white',
                    fontWeight: '700', alignItems: 'center', justifyContent: 'center',
                    backgroundColor: deleting ? 'grey' : 'red'
                }}>
                {deleting ? 'Apagando, aguarde' : 'Apagar'}
            </div>
            }



            <div
                onClick={() => saving ? null : saveNurse()}
                style={{
                    position: 'fixed',
                    bottom: 50, right: 50,
                    fontSize: 22,
                    padding: 20, borderRadius: 10,
                    color: 'white',
                    fontWeight: '700', alignItems: 'center', justifyContent: 'center',
                    backgroundColor: saving ? 'grey' : purpleTitle
                }}>
                {saving ? 'Salvando, aguarde' : 'Salvar'}
            </div>



        </div>
    )
}


const mapStateToProps = (state) => {


    return {
        users: state.user.users,
        newNurse: state.nurses.newNurse,
        establishments: state.establishment.establishments,
        vacinesGlobal: state.event.kits,


    }
}


export default connect(mapStateToProps, {
    nurseUpdateAction, saveNurseAction,updateNurseAction, deleteNurseAction
})(withRouter(CreateNurseScreen))
