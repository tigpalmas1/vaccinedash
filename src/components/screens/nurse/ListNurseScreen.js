import React, {useEffect, useState} from 'react';
import {Link} from 'react-router-dom'
import {connect} from "react-redux";
import {clearListAction, fetchNurseAction} from "../../../redux/actions";
import NurseItem from './NurseItem'
import EmptyState from "../../fragments/EmptyState";
import PaginationComponent from "../../../common/components/PaginationComponent";
import PermissionDialog from '../../dialogs/PermissionDialog'

const ListNurseScreen = (props) => {

    const {fetchNurseAction} = props
    const {nurses, message} = props
    const [activePage, setActivePage] = useState(1)

    const [open, setOpen] = useState(false);

    useEffect(() => {
        fetchNurseAction();

    }, [])



    const renderList = () => {
        return nurses.map(item => {
            return (

                <NurseItem
                    setItem={()=>{
                        setOpen(true)

                    }}
                    key={'vai que vai'}
                    item={item}/>
            )
        })
    }

    const handlePageClick = () => {

    }


    return (


        <div style={{display: 'flex', flex: 1, flexDirection: 'column', }}>
            <div style={{  display: 'flex', width: '100%', flexDirection: 'row', flexWrap: 'wrap'  }}>
                {renderList()}
                {nurses.length === 0 && message &&
                <EmptyState message={message}/>
                }
            </div>
            <div style={{marginTop: 10}}>
                <PaginationComponent
                    activePage={activePage}
                    setActivePage={(value)=>setActivePage(value)}
                />
            </div>

            <div style={{display: 'flex', flexDirection: 'row', justifyContent:'flex-end', marginTop:10}}>
                <PermissionDialog
                    setOpen={()=>setOpen(false)}
                    open={open}
                />
            </div>
        </div>



    )

};

const mapStateToProps = (state) => {


    return {
        nurses: state.nurses.nurses,
        message: state.nurses.message,

    }
}


export default connect(mapStateToProps, {fetchNurseAction})(ListNurseScreen)