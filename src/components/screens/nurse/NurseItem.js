import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {titleColor} from "../../../common/colors/index";

import {connect} from "react-redux";

import Tag from "../../../common/components/Tag";
import {setNurseAction} from "../../../redux/actions";
import {Link} from 'react-router-dom'


const useStyles


    = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: 10,
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
}));


const NurseItem = (props) => {
    const {setNurseAction} = props || {}
    const {item, } = props || {}
    const classes = useStyles();

    const {userId, laboratory, vaccines, workHours} = item || {}


    const {name, lastName, } = userId || {}
    const {name: labName,} = laboratory || {}


    const {textStyle} = styles


    return (
        <div className={classes.root}>
            <ExpansionPanel>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon/>}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <div style={{display: 'flex', flexDirection: 'row', fontWeight: '700', color: titleColor}}>
                        <div style={textStyle}>{name} {lastName}</div>
                        <div style={textStyle}>{labName}</div>

                    </div>

                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div style={{display: 'flex', flexDirection: "column"}}>

                        <div style={{display: 'flex', flexDirection: 'row',}}>


                        </div>
                        <div style={{display: 'flex', flexDirection: 'row'}}>
                            <div style={{display: 'flex1', flexDirection: 'column'}}>
                                <div style={{display: 'flex', flexDirection: 'row',}}>
                                    <div style={{marginRight: 5, fontWeight: '700', marginBottom: 10}}>{'Vacinas'}</div>

                                    <Link
                                        style={{maxWidth: 200, maxHeight: 150}}
                                        to={`/nurse/create`}>
                                    <Tag
                                        onClick={()=>{
                                            setNurseAction(item)
                                        }}
                                        title={'editar'}/>
                                    </Link>
                                </div>
                                <div style={{display: 'flex', flexDirection: 'row',}}>
                                    {vaccines.map((item) => (
                                        <div style={{marginRight: 10}}>{item.name} - </div>
                                    ))}
                                </div>

                                <div style={{
                                    marginRight: 5,
                                    fontWeight: '700',
                                    marginBottom: 10,
                                    marginTop: 10
                                }}>{'Horário de Trabalho'}</div>
                                <div style={{display: 'flex', flexDirection: 'row',}}>
                                    {workHours.map((item) => (
                                        <div style={{marginRight: 10}}>
                                            <div>{item.weekDay}</div>
                                            <div>{item.beginTime}</div>
                                            <div>{item.endTime}</div>
                                        </div>

                                    ))}
                                </div>


                            </div>


                        </div>
                        {/*
                        <Button variant="outlined" color="primary" onClick={setItem}>
                            Permissões
                        </Button>*/}


                    </div>

                </ExpansionPanelDetails>
            </ExpansionPanel>

        </div>
    );
}

const styles = {
    textStyle: {
        marginRight: 10,
        width: 150,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
}


const mapStateToProps = (state) => {

    return {}
}


export default connect(mapStateToProps, {setNurseAction})(NurseItem)
