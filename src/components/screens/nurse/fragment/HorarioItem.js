import React, {useState} from 'react';
import TextField from '@material-ui/core/TextField';
import Tag from "../../../../common/components/Tag";
import DropDown from '../../../../common/components/DropDown'
import {constHorarioType, constPhoneTypes} from "../../../../common/constants/index";
import {MuiPickersUtilsProvider, KeyboardTimePicker} from '@material-ui/pickers';
import DateFnsUtils from "@date-io/date-fns";


const HorarioItem = (props) => {
    const {item, removePhone, setItemValue} = props || {}
    const {weekDay, beginTime, endTime} = item || {}




    return (
        <div style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between'
        }}>
            <div style={{marginTop: -5, }}>
                <DropDown
                    value={weekDay}
                    setValue={(value) => {
                        setItemValue({prop: 'weekDay', value: value,})
                    }}
                    data={constHorarioType}
                    label="Dia"
                /></div>
            <div style={{width: 100, marginRight: 10,}}>
                <MuiPickersUtilsProvider style={{
                    marginTop: 110,
                }}
                                         utils={DateFnsUtils}>
                    <KeyboardTimePicker
                        InputLabelProps={{shrink: true}}
                        format="hh:mm"
                        value={beginTime}
                        id="time-picker"
                        ampm={false}


                        onChange={(value) => {
                            setItemValue({prop: 'beginTime', value: value})
                        }}
                        label="Inicio"

                    />
                </MuiPickersUtilsProvider>
            </div>

            <div style={{width: 100, marginRight: 10,}}>
                <MuiPickersUtilsProvider style={{marginTop: 110,}}
                                         utils={DateFnsUtils}>
                    <KeyboardTimePicker
                        InputLabelProps={{shrink: true}}
                        value={endTime}
                        id="time-picker"


                        ampm={false}
                        onChange={(value) => {
                            setItemValue({prop: 'endTime', value: value})
                        }}
                        label="Saída"

                    />
                </MuiPickersUtilsProvider>

            </div>


            <div>
                <Tag onClick={removePhone}
                     backgroundColor={'red'}
                     fontSize={8} title={''} iconName={"remove"}/>
            </div>

        </div>
    )
}

const styles = {}

export default HorarioItem;