import React,{useState} from 'react';

import {connect} from "react-redux";
import HorarioItem from "./HorarioItem";
import {subTitleStyle} from "../../../../common/styles/geralStyles";
import Tag from "../../../../common/components/Tag";
import {daysAddAction, daysRemoveAction, daysUpdateAction} from "../../../../redux/actions";


const HorarioFragment = (props) => {
    const {daysAddAction, daysRemoveAction, daysUpdateAction} = props ||{}
    const {newNurse} = props ||{}
    const {workHours} = newNurse ||{}




    const addPhone = ()=>{
        daysAddAction({ weekDay: '', beginTime: '', endTime: ''})

    }

    const removePhone = (index)=>{
        daysRemoveAction(index)
    }

    const setItemValue = (value, index)=>{
        daysUpdateAction(value, index)

    }

    return (
        <div>

            <div style={{flexDirection: 'row', display: 'flex', alignItems: 'center', }}>
                <div style={subTitleStyle}>Horários</div>
                <div style={{marginLeft: 20, marginTop:10}}>
                    <Tag onClick={() => {
                        addPhone()
                    }}
                         fontSize={8} title={'Adicionar'} iconName={"plus"}/>
                </div>
            </div>


            {workHours &&
            <div>
                {workHours.map((item, index) => (
                    <HorarioItem
                        setItemValue={(value)=>setItemValue(value, index)}
                        removePhone={()=>removePhone(index)}
                        item={item}/>
                ))}
            </div>
            }



        </div>
    )
}




const mapStateToProps = () => {


    return {


    }
}


export default connect(mapStateToProps, {daysAddAction, daysRemoveAction, daysUpdateAction })(HorarioFragment)
