import React from 'react';
import {mainOrange} from "../../../common/colors/index";
import LoginFragment from './LoginFragment'


const LoginPage  = (props)=>{
    return(
        <div style={{width: '100%', height: '100vh',
            backgroundColor: mainOrange,
            alignItems: 'center', justifyContent: 'center', display: 'flex', flexDirection: 'column'}}>
            <div style={{width:300, height: 100, display: 'flex',padding: 20,
                fontSize:34, fontWeight: '700',
                color: 'white' }}>Projeto Vaccine</div>

            <LoginFragment/>


        </div>
    )
}

export default LoginPage;