import React from 'react';
import {connect} from 'react-redux';

import {authUpdate, checkAuthentication, loginServerAction,} from "../../../redux/actions";
import {Redirect} from 'react-router-dom'
import CustomizedButtons from "../../../common/components/CustomizedButtons";
import CustomizedProgress from "../../../common/components/CustomizedProgress";
import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';
import history from '../../../history'

import {loginContainer, loginTitleStyle} from "../../../common/styles";


class LoginFragment extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
        }


        this.props.checkAuthentication()
    }



    componentWillMount() {
        this.setState({width: window.innerWidth})
    }

    onLoginPress = () => {
        if (!this.checkError()) {
            this.props.loginServerAction(this.props.auth, this.callback)
        }
    }

    callback = () => {
        //alert('logado com sucesso, vamos implementar a primeira tela')
        history.push({pathname: '/home'})
    }

    checkError() {
        const {email, password} = this.props.auth || {}
        let errors = false;

        if (!email) {
            this.setState({email: 'Email obrigátorio'})
            errors = true
        }
        if (email && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
            this.setState({email: 'Email está no formato incorreto'})
            errors = true
        }

        if (!password) {
            this.setState({password: 'Senha obrigátoria'})
            errors = true
        }
        if ((password && password.length < 8)|| password.length > 14) {
            this.setState({password: 'Senha deve ter entre 8 e 14 caracteres'})
            errors = true
        }
        return errors
    }





    handleChange =(e, name)=>  {
        this.props.authUpdate({prop: name, value: e.target.value.trim()})
    };




    render() {



        const {loading, auth, errorMessage, logged}= this.props ||{}
        const {email, password}= auth ||{}
        const {email: emailError, password: passwordError} = this.state

        if(logged){

            return(
                <Redirect to='/home' />
                )
        }

        return (
            <div>

                <Card style={{padding: 20}}>
                <div style={loginContainer}>
                    <div style={loginTitleStyle}>Entrar</div>


                    <TextField
                        error={emailError!==""}
                        helperText={emailError}
                        id="outlined-email-input"
                        label="Email"
                        value={email}
                        className={{marginLeft: 10, marginRight: 10,}}
                        style={{width: 400}}
                        type="email"
                        name="email"
                        onChange={(e)=> {
                            this.setState({email: ''})
                            this.handleChange(e, 'email')}}
                        autoComplete="email"
                        margin="normal"
                        variant="outlined"
                    />

                    <TextField
                        error={passwordError!==""}
                        helperText={passwordError}
                        id="outlined-password-input"
                        label="Password"
                        className={{marginLeft: 10, marginRight: 10,}}
                        style={{width: 400, marginBottom: 40}}
                        type="password"
                        value={password}
                        onChange={(e)=> {
                            this.setState({password: ''})
                            this.handleChange(e, 'password')}}
                        margin="normal"
                        variant="outlined"
                    />


                    {errorMessage &&
                    <div style={{color: 'red', marginBottom: 50, fontWeight: '700'}}>
                        {errorMessage}
                    </div>
                    }




                    <CustomizedButtons
                        disabled={loading}
                        label={"Entrar"} onClick={this.onLoginPress}/>



                    {loading &&
                    <div style={{width: '100%'}}>
                        <div style={{marginTop: 40}}>

                            <CustomizedProgress/>
                        </div>
                    </div>
                    }







                </div>
                </Card>

            </div>





        )

    }
}

const mapStateToProps = (state) => {
    return {
        logged: state.auth.logged,
        auth: state.auth.auth,
        loading: state.auth.loading,
        errorMessage: state.auth.errorMessage
    }
}

export default connect(mapStateToProps, {authUpdate, loginServerAction,checkAuthentication})(LoginFragment);