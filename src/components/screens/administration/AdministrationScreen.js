import React from 'react';
import {connect} from "react-redux";
import Table from '../../../common/components/Table'
import TableStyles from '../../../common/components/TableStyles'
import AppBar from '../../../common/components/AppBar'
import VersionFragment from '../../fragments/VersionFragment'

const AdministrationScreen = (props) => {

    const {preference} = props
    const {tags, cities} = preference || {}

    return (

            <div style={{display: 'flex', flex: 1, flexDirection: 'column'}}>
                <AppBar title={'Administrar'}/>


                <div style={{display: 'flex', width: '100%',padding: 10, flexDirection: 'row', flexWrap: 'wrap'}}>


                    <div style={{marginRight: 10}}>
                        <VersionFragment/>
                    </div>


                </div>

                <div style={{display: 'flex', width: '100%',padding: 10, flexDirection: 'row', flexWrap: 'wrap'}}>


                    <div style={{marginRight: 10}}>
                        <Table cities={cities}/>
                    </div>

                    <div style={{marginRight: 10}}>
                        <TableStyles tags={tags}/>
                    </div>



                </div>


            </div>

    )
}

const mapStateToProps = (state) => {


    return {
        preference: state.filter.preference
    }
}


export default connect(mapStateToProps, {})(AdministrationScreen)
