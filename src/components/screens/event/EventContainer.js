import React from 'react';
import {NavLink, Route, withRouter} from 'react-router-dom'

import SendKitScreen from "./CreateEventScreen";
import {connect} from 'react-redux';
import AppBar from '../../../common/components/AppBar'
import {purpleTitle} from "../../../common/colors/index";
import ListVaccineScreen from "./ListVaccineScreen";
import {setActiveItemAction} from "../../../redux/actions";


const KitContainer  = (props )=>{
    const { data, setActiveItemAction} = props
    const {permission, user} = data ||{}
    const {createEvent, isAdmin} = permission ||{}

    const {location} = props||{}
    const {pathname} = location ||{}

    const title = pathname === '/events'? 'Vacinas': 'Cadastrar Vacina'


    return(


        <div style={{display: 'flex', flex: 1, flexDirection: 'column'}}>
            <AppBar title={title}/>

            <div style={{flexDirection: 'row',flex: 1, display:'flex', padding: 20}}>
                <Route path={'/events'} exact  component={ListVaccineScreen}/>
                <Route path={'/events/send'} exact component={SendKitScreen}/>
            </div>


            {(createEvent || isAdmin) &&
            <div>
                {(pathname === '/events') &&
                <NavLink
                    to={'/events/send'}
                    onClick={()=>{
                        setActiveItemAction(null)
                    }}
                    style={{
                        position: 'fixed',
                        bottom: 50, right: 50,
                        fontSize: 22,
                        padding: 20, borderRadius: 10,
                        color: 'white',
                        fontWeight: '700', alignItems: 'center', justifyContent: 'center',
                        backgroundColor: purpleTitle
                    }}>
                    {'Nova Vacina'}
                </NavLink>
                }

            </div>
            }




        </div>


    )
}


const mapStateToProps = (state) => {
    return {
        data: state.user.data
    }
}

export default connect(mapStateToProps, {setActiveItemAction})(withRouter(KitContainer));