import React from 'react';

import {connect} from "react-redux";
import {addDoseAction, doseUpdateAction, removeDoseAction} from "../../../redux/actions";
import Tag from "../../../common/components/Tag";
import DoseItem from './DoseItem';
import {titleColor} from "../../../common/colors/index";

const DoseComponent = (props) => {
    const {doseArray, error, setError} = props ||[]
    const {addDoseAction, removeDoseAction, doseUpdateAction} = props ||[]

    const setItem = (value, index)=>{
        doseUpdateAction(value, index)
    }

    return (
        <div>
            <div style={{display: 'flex', flexDirection: 'row'}}>
                <div style={{fontSize: 20, color: error ? 'red':titleColor,  fontWeight: '700', marginRight:10}}>Doses</div>
                <Tag
                    onClick={()=>{
                        setError();
                        addDoseAction()}}
                    title={'Adicionar'}/>
            </div>

            <div style={{fontSize: 16, color: error ? 'red':titleColor,  fontWeight: '700', marginRight:10}}>{error}</div>


            {doseArray &&
            <div style={{flexDirection: 'row', display: 'flex'}}>
                {doseArray.map((item, index) => (
                    <DoseItem
                        clearError={setError}
                        key={index}
                        removeItem={()=>removeDoseAction(index)}
                        setItemValue={(value)=>setItem(value, index)}
                        dose={item}/>
                ))}
            </div>
            }



        </div>
    )
}

const mapStateToProps = (state) => {


    return {}
}


export default connect(mapStateToProps, {doseUpdateAction, addDoseAction, removeDoseAction})(DoseComponent)
