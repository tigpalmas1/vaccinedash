import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import {connect} from 'react-redux';
import {Link} from 'react-router-dom'

import red from '@material-ui/core/colors/red';

import { purpleTitle, titleColor} from "../../../common/colors/index";
import './css/EventCard.css'


import { setActiveItemAction} from "../../../redux/actions";
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';


const styles = theme => ({
    card: {
        width: 350,
    },
    media: {
        height: 0,
        paddingTop: '56.25%', // 16:9
    },
    actions: {
        display: 'flex',
    },
    expand: {
        transform: 'rotate(0deg)',
        marginLeft: 'auto',
        transition: theme.transitions.create('transform', {
            duration: theme.transitions.duration.shortest,
        }),
    },
    expandOpen: {
        transform: 'rotate(180deg)',
    },
    avatar: {
        backgroundColor: red[500],
    },
});



const VaccineItemCard = (props) => {


    const { setActiveItemAction} = props


    const {event,} = props
    let { name, _id, isStandard} = event || {}






    return (
        <Link
            style={{marginRight: 10, borderRadius: 10,marginTop: 10}}
            onClick={() => {
                setActiveItemAction(event)

            }}
            to={`/events/send`}
           key={_id}
            className="card">


                <div style={{flexWrap: 'wrap', display: 'flex', flexDirection: 'row',}}>

                    <div style={{flex: 9, display: 'flex', flexDirection: 'column', justifyContent: "center",}}>
                        <ExpansionPanel>
                            <ExpansionPanelSummary
                                expandIcon={<ExpandMoreIcon/>}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                            >
                                <div style={{display: 'flex', flexDirection: 'row', fontWeight: '700', color: titleColor}}>

                                    <div style={{color: isStandard? purpleTitle: 'red', marginRight: 20,}} >{isStandard? 'Padrão': 'Adicional'} </div>
                                    <div style={{marginRight: 10}}>{name} </div>


                                </div>

                            </ExpansionPanelSummary>

                        </ExpansionPanel>

                    </div>


                </div>



        </Link>
    );

}

VaccineItemCard.propTypes = {
    classes: PropTypes.object.isRequired,
};


const mapStateToProps = (state,) => {


    return {
        logged: state.user.logged,
        user: state.user.user,

    }
};

export default connect(mapStateToProps, {setActiveItemAction})(withStyles(styles)(VaccineItemCard));
