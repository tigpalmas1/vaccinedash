import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import {clearKItsList, fetchVaccineAction, stateChangedAction} from "../../../redux/actions";
import VaccineItemCard from "./VaccineItemCard"
import PaginationComponent from "../../../common/components/PaginationComponent";
import EmptyState from "../../fragments/EmptyState";
import AlertDialog from '../../../common/components/AlertDialog'
import EditEventDialog from '../../dialogs/EditEventDialog'
import SnackBar from '../../../common/components/SnackBar'
import CreatePromoteVaccine from "../../dialogs/CreatePromoteVaccine";
import PromotedVaccineFragment from "./PromotedVaccineFragment";

const ListVaccineScreen = (props) => {
    const {fetchVaccineAction, kits, message,  preference} = props

    const [filterDialog, setfilterDialog] = useState(false)
    const [establishmentDialog, setestablishmentDialog] = useState(false)
    const [event, setEvent] = useState({})

    const [openSnack, setOpenSnack] = useState(false)
    const [snackMessage, setSnackMessage] = useState(false)
    const [noDescrition,] = useState(false)
    const [activePage, setActivePage] = useState(1)


    const {selectedCity} = preference

    useEffect(() => {

        fetchVaccineAction(preference, activePage, noDescrition)
    }, [selectedCity, activePage, noDescrition])


    const paginate = (array, page_number) => {
        return array.slice((page_number - 1) * 10, page_number * 10);
    }


    const renderList = () => {
        return paginate(kits, activePage).map(item => {

            return (

                    <VaccineItemCard
                        key={item._id}
                        alertMessage={(message) => {
                            setSnackMessage(message)
                            setOpenSnack(true)
                        }}
                        openDialog={() => {
                            setEvent(item)
                            setestablishmentDialog(true)
                        }}
                         event={item}/>




            )
        })
    }
    return (
        <div style={{display: 'flex',flex: 1, flexDirection: 'column', }}>
            <PromotedVaccineFragment/>


            <div style={{display: 'flex',  flexDirection: 'column', }}>
                {renderList()}

                {kits.length === 0 && message &&

                <EmptyState message={message}/>
                }

            </div>


            <EditEventDialog
                event={event}
                handleClose={() => setestablishmentDialog(false)}
                open={establishmentDialog}/>


            <AlertDialog
                hasCity={true}
                onDialogDismiss={() => setfilterDialog(false)}
                isDialogOpen={filterDialog}/>


            <SnackBar
                message={snackMessage}
                open={openSnack}
                setOpen={(status) => setOpenSnack(status)}
            />


            <div style={{marginTop: 10}}>
                <PaginationComponent
                    activePage={activePage}
                    setActivePage={(value) => setActivePage(value)}
                />
            </div>

            <CreatePromoteVaccine visible={true}/>

        </div>


    )


}

const mapStateToProps = (state) => {


    return {
        kits: state.event.kits,
        message: state.event.message,
        preference: state.filter.preference,
        stateChanged: state.filter.stateChanged,

    }
}


export default connect(mapStateToProps, {fetchVaccineAction, clearKItsList, stateChangedAction})(ListVaccineScreen)