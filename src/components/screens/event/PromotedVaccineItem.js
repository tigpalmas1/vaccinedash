import React from 'react';
import Card from '@material-ui/core/Card';
import {purpleTitle} from "../../../common/colors";
import {getFormatedDate} from "../../../common/functions";
import CardMedia from "@material-ui/core/CardMedia";
import {makeStyles} from "@material-ui/core/styles";

const useStyles = makeStyles({
    card: {
        maxWidth: 50,
    },
    media: {
        height: 50,
    },
});

const PromotedVaccineItem = (props) => {
    const classes = useStyles();
    const{item} = props ||{}
    const{vaccine, message,expireIn, imageId } = item ||{}
    const{smallUrl } = imageId ||{}
    const{name} = vaccine ||{}



    return (
        <Card style={{
            borderRadius: 10, padding: 10, width: 250, marginTop: 10, marginRight: 10, backgroundColor: purpleTitle
        }}>
            <div style={{flexDirection: 'row', display: 'flex'}}></div>
            <div style={{display: 'flex', flexDirection: 'column',}}>
                <div style={{flexDirection: 'row', display: 'flex'}}>
                    <div style={{marginRight: 10}}>{name}</div>
                    <div>{getFormatedDate(expireIn)}</div>
                </div>

                <div>{message}</div>
            </div>

            <CardMedia
                className={classes.media}
                image={smallUrl}
                title={name}
            />
        </Card>
    )
}


export default PromotedVaccineItem
