import React, {useState,} from 'react';
import {withStyles} from '@material-ui/core/styles';

import {connect} from "react-redux";
import TextField from '@material-ui/core/TextField';
import DoseComponent from './DoseComponent'
import {subTitleStyle} from "../../../common/styles";
import {
    deleteVaccineAction,
    editVaccineAction,
    eventUpdateAction,
    saveVaccineAction,
    setEditEventAction
} from "../../../redux/actions";
import {mainOrange} from "../../../common/colors/index";
import history from '../../../history'
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';


const PurpleCheckBox = withStyles({
    root: {
        color: 'grey',
        '&$checked': {
            color:mainOrange,
        },
    },
    checked: {},
})(props => <Checkbox color="default" {...props} />);

const CreateEventScreen = (props) => {

    const {activeEvent,} = props || {};
    const { eventUpdateAction,  saveVaccineAction, setEditEventAction,deleteVaccineAction, editVaccineAction} = props || {};
    const {newEvent: newVaccine, } = props || {};
    const {name,protection, composition, dose, isStandard,_id,
        description, administrationType,administrationRegion,
        needleSize, adverseReactions, observation
    } = newVaccine || {};

    const [saving, setSaving] = useState(false);
    const [deleting, setDeleting] = useState(false);
    const [nameError, setNameError] =useState('');
    const [protectionError, setprotectionError] =useState('');
    const [administrationRegionError, setadministrationRegionError] =useState('');
    const [administrationTypeError, setadministrationTypeError] =useState('');
    const [compositionError, setcompositionError] =useState('');
    const [doseError, setDoseError] =useState('');



    useState(()=>{
        if(activeEvent){
            setEditEventAction(activeEvent)
        }else{
            setEditEventAction(null)

        }
    },[activeEvent])



    const saveEvent = () => {
        if(!checkError()){
           setSaving(true)

            if(_id){
                editVaccineAction(newVaccine, saveCallback)
                return
            }
            saveVaccineAction(newVaccine, saveCallback)
        }
    }

    const deleteVaccine = ()=>{
        setDeleting(true);
        deleteVaccineAction(newVaccine, saveCallback)
    }

    const saveCallback = () => {
        setSaving(false)
        setDeleting(false)
        history.push("/events");
    }



    const checkError = () =>{
        let errors = false;

        if (!name) {
            setNameError('Nome obrigátorio')
            errors = true
        }
        if(!protection){
            setprotectionError('Campo obrigátorio')
            errors = true
        }
        if(!administrationRegion){
            setadministrationRegionError('Campo obrigátorio')
            errors = true
        } if(!administrationType){
            setadministrationTypeError('Campo obrigátorio')
            errors = true
        }
        if(!composition){
            setcompositionError('Campo obrigátorio')
            errors = true
        }
        if(dose.length === 0){
            setDoseError('Campo obrigátorio')
            errors = true
        }
        if(dose.length > 0){
          dose.forEach((item, index)=>{

              const {recommendedAge, doseIntervalMin, type} = item ||{}
              if(!type){
                  setDoseError('Alguma dose sem o tipo')
                  errors = true
              }
              if(!recommendedAge){
                  setDoseError('Alguma dose sem idade recomendada')
                  errors = true
              }
              if(index> 0 && !doseIntervalMin){
                  setDoseError('Intervalo minimo é obrigatório caso aja mais de uma dose')
                  errors = true
              }
          })
        }


        return errors
    }

    return (
        <div style={{display: 'flex', flex: 1, flexDirection: 'column'}}>
            <div style={{display: 'flex', width: '100%', flexDirection: 'row'}}>
                {/*INFORMAÇÔES*/}
                <div style={{display: 'flex', flex: 1, flexDirection: 'column', paddingRight: 20, paddingLeft: 20}}>
                    <div style={subTitleStyle}>Informações</div>

                    <div style={{display: 'flex', flexDirection: 'row', marginTop: 10}}>
                        <FormControlLabel
                            control={
                                <PurpleCheckBox
                                    checked={isStandard}
                                    onChange={() => {
                                        eventUpdateAction({prop: 'isStandard', value: !isStandard})
                                    }}
                                    value="checkedI"
                                />
                            }
                            label="Faz Parte da Carteira ?"
                        />
                    </div>
                    <div style={{display: 'flex', flexDirection: 'row'}}>



                        <TextField

                            InputLabelProps={{ shrink: true }}
                            label="Nome da Vacina"
                            error={nameError}
                            helperText={nameError}
                            className={{marginLeft: 10, marginRight: 10,}}
                            style={{width: '100%', marginRight: 10}}
                            value={name}
                            onChange={(e) => {
                                setNameError('')
                                eventUpdateAction({prop: 'name', value: e.target.value,})
                            }}
                            margin="normal"
                        />

                        <TextField
                            InputLabelProps={{ shrink: true }}
                            label="Proteção Contra"
                           className={{marginLeft: 10, marginRight: 10,}}
                            style={{width: '100%', marginRight: 10}}
                            value={protection}
                            error={protectionError}
                            helperText={protectionError}
                            onChange={(e) => {
                                setprotectionError('')
                                eventUpdateAction({prop: 'protection', value: e.target.value,})
                            }}
                            margin="normal"
                        />

                        <TextField
                            InputLabelProps={{ shrink: true }}
                            label="Composição"
                            error={compositionError}
                            helperText={compositionError}
                             className={{marginLeft: 10, marginRight: 10,}}
                            style={{width: '100%', marginRight: 10}}
                            value={composition}
                            onChange={(e) => {
                                setcompositionError('')
                                eventUpdateAction({prop: 'composition', value: e.target.value,})
                            }}
                            margin="normal"
                        />
                    </div>







                    <div style={{display: 'flex', flexDirection: 'row'}}>
                        <TextField
                            InputLabelProps={{ shrink: true }}
                            label="Via de Administração"
                            error={administrationTypeError}
                            helperText={administrationTypeError}
                            className={{marginLeft: 10, marginRight: 10,}}
                            style={{width: '100%', marginRight: 10}}
                            value={administrationType}
                            onChange={(e) => {
                                setadministrationTypeError('');
                                eventUpdateAction({prop: 'administrationType', value: e.target.value,})
                            }}
                            margin="normal"
                        />

                        <TextField
                            InputLabelProps={{ shrink: true }}
                            label="Local de aplicação"
                            error={administrationRegionError}
                            helperText={administrationRegionError}
                            className={{marginLeft: 10, marginRight: 10,}}
                            style={{width: '100%', marginRight: 10}}
                            value={administrationRegion}
                            onChange={(e) => {
                                setadministrationRegionError('')
                                eventUpdateAction({prop: 'administrationRegion', value: e.target.value,})
                            }}
                            margin="normal"
                        />

                        <TextField
                            InputLabelProps={{ shrink: true }}
                            label="Agulha Recomendada"

                            className={{marginLeft: 10, marginRight: 10,}}
                            style={{width: '100%', marginRight: 10}}
                            value={needleSize}
                            onChange={(e) => {

                                eventUpdateAction({prop: 'needleSize', value: e.target.value,})
                            }}
                            margin="normal"
                        />
                    </div>


                    <TextField
                        label="Descrição"
                        InputLabelProps={{ shrink: true }}
                        className={{marginLeft: 10, marginRight: 10,}}
                        style={{width: '100%', marginRight: 10, }}
                        multiline={3}
                        lines={3}
                        value={description}
                        onChange={(e) => {
                            eventUpdateAction({prop: 'description', value: e.target.value,})
                        }}
                        margin="normal"
                    />

                    <TextField
                        label="Efeitos Adversos"
                        InputLabelProps={{ shrink: true }}
                        className={{marginLeft: 10, marginRight: 10,}}
                        style={{width: '100%', marginRight: 10, }}
                        multiline={3}
                        lines={3}
                        value={adverseReactions}
                        onChange={(e) => {
                            eventUpdateAction({prop: 'adverseReactions', value: e.target.value,})
                        }}
                        margin="normal"
                    />


                    <TextField
                        label="Observações"
                        InputLabelProps={{ shrink: true }}
                        className={{marginLeft: 10, marginRight: 10,}}
                        style={{width: '100%', marginRight: 10, }}
                        multiline={3}
                        lines={3}
                        value={observation}
                        onChange={(e) => {
                            eventUpdateAction({prop: 'observation', value: e.target.value,})
                        }}
                        margin="normal"
                    />



                    <div style={{marginTop: 20}}>

                        <DoseComponent
                            setError={()=>{setDoseError('')}}
                            error={doseError}
                            doseArray={dose}
                        />
                    </div>


                </div>



            </div>


            {_id &&
            <div
                onClick={() => deleting ? null : deleteVaccine()}
                style={{
                    position: 'fixed',
                    top: 50, right: 150,
                    fontSize: 20,
                    padding: 20, borderRadius: 10,
                    color: 'white',
                    fontWeight: '700', alignItems: 'center', justifyContent: 'center',
                    backgroundColor: saving ? 'grey' : 'red'
                }}>
                {deleting? 'Apagando': 'Apagar' }
            </div>

            }



            <div
                onClick={() => /*saving ? null : */saveEvent()}
                style={{
                    position: 'fixed',
                    bottom: 50, right: 50,
                    fontSize: 20,
                    padding: 20, borderRadius: 10,
                    color: 'white',
                    fontWeight: '700', alignItems: 'center', justifyContent: 'center',
                    backgroundColor: saving ? 'grey' : mainOrange
                }}>
                {saving ? 'Salvando Evento..' :_id? 'Editar': 'Salvar'}
            </div>
        </div>
    )
}

const mapStateToProps = (state) => {


    return {
        data: state.user.data,
        newEvent: state.event.newEvent,
        activeEvent: state.event.activeEvent,
    }
}


export default connect(mapStateToProps, {eventUpdateAction,editVaccineAction,deleteVaccineAction, setEditEventAction,  saveVaccineAction})(CreateEventScreen)
