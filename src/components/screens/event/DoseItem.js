import React from 'react';
import Tag from "../../../common/components/Tag";
import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';


const DoseItem = (props) => {
    const {dose, setItemValue, removeItem, clearError} = props || {}
    const {type, recommendedAge, doseIntervalRec, doseIntervalMin} = dose || {}


    return (
        <Card style={{
            borderRadius: 10, padding: 10, width: 250, marginTop: 10, marginRight: 10
        }}>
            <div style={{display: 'flex', flexDirection: 'row',}}>
                <div>
                    <TextField

                        label="Tipo"
                        InputLabelProps={{shrink: true}}
                        className={{marginLeft: 10, marginRight: 10,}}
                        style={{width: '100%', marginRight: 10}}
                        value={type}
                        onChange={(e) => {
                            clearError()
                            setItemValue({prop: 'type', value: e.target.value,})

                        }}
                        margin="normal"
                    />

                    <TextField

                        label="Idade Recomendada"
                        InputLabelProps={{shrink: true}}
                        className={{marginLeft: 10, marginRight: 10,}}
                        style={{width: '100%', marginRight: 10}}
                        value={recommendedAge}
                        onChange={(e) => {
                            clearError()
                            setItemValue({prop: 'recommendedAge', value: e.target.value,})

                        }}
                        margin="normal"
                    />

                    <TextField

                        label="Intervalo Recomendado"
                        InputLabelProps={{shrink: true}}
                        className={{marginLeft: 10, marginRight: 10,}}
                        style={{width: '100%', marginRight: 10}}
                        value={doseIntervalRec}
                        onChange={(e) => {
                            clearError()
                            setItemValue({prop: 'doseIntervalRec', value: e.target.value,})

                        }}
                        margin="normal"
                    />

                    <TextField
                        id="standard-basic"
                        label="Intervalo Mínimo"
                        InputLabelProps={{shrink: true}}
                        className={{marginLeft: 10, marginRight: 10,}}
                        style={{width: '100%', marginRight: 10}}
                        value={doseIntervalMin}
                        onChange={(e) => {
                            clearError()
                            setItemValue({prop: 'doseIntervalMin', value: e.target.value,})

                        }}
                        margin="normal"
                    />
                </div>


                <div><Tag
                    onClick={removeItem}
                    title={' X '}/></div>
            </div>
        </Card>
    )
}


export default DoseItem
