import React, {useEffect, useState} from 'react';
import {connect} from 'react-redux';
import Tag from "../../../common/components/Tag";
import {purpleTitle} from "../../../common/colors";
import CreatePromoteVaccine from "../../dialogs/CreatePromoteVaccine";
import {fetchPromotedVaccinesAction} from "../../../redux/actions";
import PromotedVaccineItem from "./PromotedVaccineItem";


const PromotedVaccineFragment = (props) => {
    const {fetchPromotedVaccinesAction, }= props ||{};
    const {promotedVaccines, }= props ||{};
    const [open, setOpen] = useState(false);


    console.log(promotedVaccines);

    useEffect(()=>{
        fetchPromotedVaccinesAction();
    },[])

    const renderList = () => {
        return promotedVaccines.map(item => {

            return (
             <PromotedVaccineItem
                item={item}
             />





            )
        })
    }


    return (
        <div style={{width: '100%', }}>
            <div style={{flexDirection: 'row', display: 'flex'}}>
                <div style={{marginRight: 10, fontSize: 20, fontWeight: '700'}}>Vacinas em destaque</div>
                <Tag
                    onClick={()=>setOpen(true)}
                    title={'Adicionar'} color={purpleTitle}/>


            </div>

            <div style={{flexDirection: 'row', display: 'flex'}}>
                {renderList()}
            </div>

            <CreatePromoteVaccine
                handleClose={()=>setOpen(false)}
                open={open}/>


        </div>

    );

}


const mapStateToProps = (state, ownProps) => {


    return {
        promotedVaccines: state.promotedVaccine.promotedVaccines,

    }
};

export default connect(mapStateToProps, {fetchPromotedVaccinesAction})(PromotedVaccineFragment);
