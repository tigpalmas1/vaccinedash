import React, {useEffect, useState} from 'react';
import {connect, useDispatch, useSelector} from "react-redux";
import {fetchWarehouseItensAction, clearWharehouseList, deleteWarehouseItemAction, setActiveItemWharehouse, setEditWharehouseAction} from "../../../redux/actions";
import EmptyState from "../../fragments/EmptyState";
import CreateNewsDialog from '../../dialogs/CreateWareHouseDialog'
import {mainOrange, titleColor} from "../../../common/colors/index";
import AlertDialog from '../../../common/components/AlertDialog'
import AppBar from '../../../common/components/AppBar'
import StockItem from './StockListItem'
import PaginationComponent from "../../../common/components/PaginationComponent";

const WareHouseListSccreen = (props) => {
    const news  = useSelector(state => state.wharehouse.news)
    const message  = useSelector(state => state.wharehouse.message)
    const activeEstablishment  = useSelector(state => state.establishment.activeEstablishment)


    const dispatch = useDispatch()

    const [activePage, setActivePage] = useState(1);


    const [createDialogOpen, setcreateDialogOpen] = useState(false)
    const [filterDialog, setfilterDialog] = useState(false)


    useEffect(() => {
        dispatch(clearWharehouseList())
        dispatch(fetchWarehouseItensAction( activeEstablishment, activePage))
    }, [activePage])


    const renderList = () => {
        return news.map((item) => {


            return (
               <StockItem
                   editItem={()=>{
                       dispatch(setActiveItemWharehouse(item))
                       setcreateDialogOpen(true)
                   }}
                   deleteItem={()=>dispatch(deleteWarehouseItemAction(item._id, callback, activeEstablishment))}
                   item={item}/>

            )
        })
    }


    const callback = (message)=>{
        alert(message);
    }
    return (
        <div style={{display: 'flex', flex: 1, flexDirection: 'column'}}>
            <AppBar title={'Estoque de Vacinas'}/>
            <div style={{display: 'flex', flex: 1, flexDirection: 'column', padding: 20}}>


                <div style={{display: 'flex', flex: 1, flexDirection: 'column',  }}>
                    {renderList()}

                    {news.length === 0 && message &&

                    <EmptyState message={message}/>
                    }


                </div>

                <div style={{marginTop: 10}}>
                    <PaginationComponent
                        activePage={activePage}
                        setActivePage={(value)=>setActivePage(value)}
                    />
                </div>
            </div>


            <CreateNewsDialog

                handleClose={() =>{
                    dispatch(setEditWharehouseAction(null))
                    dispatch(setActiveItemWharehouse(null));
                    setcreateDialogOpen(false)}}
                open={createDialogOpen}

            />

            <AlertDialog
                hasCity={true}
                onDialogDismiss={() => setfilterDialog(false)}
                isDialogOpen={filterDialog}/>

            <div
                onClick={() => {
                    dispatch(setActiveItemWharehouse(null))
                    setcreateDialogOpen(true)}}
                style={{
                    position: 'fixed',
                    bottom: 50, right: 50,
                    fontSize: 22,
                    padding: 20, borderRadius: 10,
                    color: 'white',
                    fontWeight: '700', alignItems: 'center', justifyContent: 'center',
                    backgroundColor: mainOrange
                }}>
                {'Novo Item'}
            </div>


        </div>



    )


}



export default WareHouseListSccreen