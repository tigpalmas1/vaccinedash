import React from 'react';

import {connect} from 'react-redux';

import {setActiveItemWharehouse} from '../../../redux/actions'
import Card from '@material-ui/core/Card';

import './NewsCard.css'

class NewsCard extends React.Component {
    state = {expanded: false};



    render() {
        const { width,  height} = this.props;
        const {userId} = this.props
        const {name, lastName} = userId || {}




        return (

            <div className="card" class="container" style={{width: width|| '100%', height: height||  '100%', borderRadius: 10}}>
                <Card   style={{width: width|| '100%', height: height, borderRadius: 10}}>
                    <div
                        style={{display: 'flex', flexWrap: 'wrap', borderRadius: 10, flexDirection: 'row', overflow: 'hidden'}}
                        class="information">
                        {name} {lastName}</div>
                </Card>



            </div>

        );
    }
}

export default connect(null, {setActiveItemNews: setActiveItemWharehouse}) (NewsCard);


