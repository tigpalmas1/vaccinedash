import React, {useState, useEffect} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import {purpleTitle, titleColor} from "../../../common/colors/index";
import {formataDinheiro, } from "../../../common/functions/index";

import {connect} from "react-redux";
import {deleteWarehouseItemAction} from "../../../redux/actions";

import Tag from "../../../common/components/Tag";


const useStyles


    = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: 10,
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        fontWeight: theme.typography.fontWeightRegular,
    },
}));


const StockItem = (props) => {
    const {user, item,  setCallback, deleteItem, editItem} = props || {}
    const classes = useStyles();



    const {batch, price, vaccine, quantity, _id, laboratory} = item || {}
    const {name} = vaccine || {}
    const {name: labName} = laboratory || {}




    const {textStyle} = styles





    const callback = (message) => {
        alert(message)
    }

    return (
        <div className={classes.root}>
            <ExpansionPanel>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon/>}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                >
                    <div style={{display: 'flex', flexDirection: 'row', fontWeight: '700', color: titleColor}}>
                        <div style={textStyle}>{labName} </div>
                        <div style={textStyle}>{name} </div>
                        <div style={textStyle}>lote: {batch} </div>
                        <div style={textStyle}>{formataDinheiro(price)} </div>
                        <div style={textStyle}>quantidade {quantity} </div>


                    </div>

                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div style={{display: 'flex', flexDirection: "column"}}>

                        <div style={{display: 'flex', flexDirection: 'row',}}>


                        </div>
                        <div style={{display: 'flex', flexDirection: 'row'}}>
                       {/*     <div style={{display: 'flex1', flexDirection: 'column'}}>
                                <div style={{display: 'flex', flexDirection: 'row',}}>
                                    <div style={{marginRight: 5}}>{name}</div>
                                    <div style={{marginRight: 5}}>{lastName}</div>
                                </div>


                            </div>
*/}

                        <Tag
                            onClick={deleteItem}
                            backgroundColor={'grey'}
                            title={'Apagar'}
                        />

                            <Tag
                                onClick={editItem}
                                backgroundColor={'grey'}
                                title={'Editar'}
                            />
                        </div>



                    </div>

                </ExpansionPanelDetails>
            </ExpansionPanel>

        </div>
    );
}

const styles = {
    textStyle: {
        marginRight: 10,
        width: 150,
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center'
    }
}


const mapStateToProps = (state) => {

    return {}
}


export default connect(mapStateToProps, {deleteWarehouseItemAction})(StockItem)
