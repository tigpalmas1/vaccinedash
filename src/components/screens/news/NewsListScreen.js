import React, {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchNews, setActiveItemNews} from "../../../redux/actions";
import CreateNewsDialog from '../../dialogs/CreateNewsDialog'
import {purpleTitle} from "../../../common/colors/index";
import NewsCard from './NewsCard';
import AppBar from '../../../common/components/AppBar'
import {Button} from "../../../common/components/Button";

const NewsListScreen = (props) => {
    const dispatch = useDispatch();
    const news = useSelector(state =>state.news.news)

    const [createDialogOpen, setcreateDialogOpen] = useState(false)

    useEffect(() => {
        dispatch(fetchNews())
    }, [ ])

    const renderList = () => {
        return news.map(news => {

            return (
                <div style={{marginRight: 10, marginTop: 10}}>
                    <NewsCard
                        onClick={(news)=>{
                            dispatch(setActiveItemNews(news))
                            setcreateDialogOpen(true)
                        }}
                        height={250}
                        width={300}
                        news={news} key={news._id}
                    />
                </div>
            )
        })
    }
    return (
        <div style={{display: 'flex', flex: 1, flexDirection: 'column'}}>
            <AppBar title={'Noticias'}/>
            <div style={{display: 'flex', flex: 1, flexDirection: 'column', padding: 20}}>

                <div style={{display: 'flex', width: '100%', flexDirection: 'row', marginTop: 20, flexWrap: 'wrap'}}>
                    {renderList()}


                </div>
            </div>


            <CreateNewsDialog
                handleClose={() => {
                    setcreateDialogOpen(false)

                }}
                open={createDialogOpen}
            />



            <div

                style={{
                    position: 'fixed',
                    bottom: 50, right: 50,

                }}>
               <Button
                   onClick={() => {
                       dispatch(setActiveItemNews(null))
                       setcreateDialogOpen(true)}}
                   color={purpleTitle}
                title={'Nova noticia'}
               />
            </div>


        </div>



    )


}



export default NewsListScreen