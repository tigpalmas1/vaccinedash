import React from 'react';

import {connect} from 'react-redux';

import {setActiveItemNews} from '../../../redux/actions'
import Card from '@material-ui/core/Card';

import CardMedia from '@material-ui/core/CardMedia';

import {titleColor} from "../../../common/colors/index";
import './NewsCard.css'
import {Textfit} from 'react-textfit';
import {getBeginTime, getFormatedDate} from "../../../common/functions/index";

class NewsCard extends React.Component {
    state = {expanded: false};



    render() {
        const { width,  height, onClick} = this.props;
        const {news} = this.props
        const {title, imageId, establishmentId,  releaseDate, expirationDate} = news || {}
        const {smallUrl} = imageId || {}
        const {personalDataId} = establishmentId || {}
        const {avatar} = personalDataId || {}




        return (

            <Card
                onClick={()=>onClick(news)}
                style={{
                display: 'flex',
                flex: 1,
                width: width, height: height, flexDirection: 'column', borderRadius: 10,
            }}>
                <div
                    style={{flex: 1, position: "relative", display: 'flex',}}>
                    <CardMedia
                        style={{flex: 1, borderRadius: 10,}}
                        image={smallUrl}

                    />

                </div>


                <div style={{flexWrap: 'wrap', display: 'flex', flexDirection: 'row', padding: 5}}>

                    <div style={{flex: 9, display: 'flex', flexDirection: 'column', justifyContent: "center",}}>

                        <div style={{flex: 1, fontWeight: '500', color: titleColor}}>
                            <Textfit
                                max={18}
                                mode="multi">
                                {title}
                            </Textfit>

                            <div style={{display: 'flex', flexDirection: 'row'}}>
                                <Textfit
                                    max={14}
                                    mode="multi">
                                    {`de ${getFormatedDate(releaseDate)}  ${getBeginTime(releaseDate)}`}
                                </Textfit>

                                <Textfit
                                    max={14}
                                    mode="multi">
                                    {`até ${getFormatedDate(expirationDate)}  ${getBeginTime(expirationDate)}`}
                                </Textfit>

                            </div>
                        </div>



                    </div>


                </div>


            </Card>

        );
    }
}

export default connect(null, {setActiveItemNews}) (NewsCard);


