import React from 'react';
import {NavLink, Route} from 'react-router-dom'

import SendKitScreen from "../event/CreateEventScreen";
import NewsListScreen from "./NewsListScreen";


const NewsContainer  = ({ match })=>{

    return(


        <div style={{display: 'flex', flex: 1, flexDirection: 'column'}}>

            <div style={{flexDirection: 'row', display:'flex'}}>
                <NavLink style={{marginRight: 20}}to={'/news'}> <h1>Noticias</h1></NavLink>
                <NavLink

                    to={'/news/create'}> <h1>Criar Noticia</h1></NavLink>
            </div>


            <Route path={'/news'} exact  component={NewsListScreen}/>
            <Route path={'/news/create'} exact component={SendKitScreen}/>

        </div>


    )
}

export default NewsContainer;