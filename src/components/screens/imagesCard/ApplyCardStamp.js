import React, {useEffect, useState} from "react";
import {useDispatch, useSelector} from "react-redux";

import {Button} from "../../../common/components/Button";
import {purpleTitle} from "../../../common/colors";
import TextField from "@material-ui/core/TextField";
import {KeyboardDatePicker, MuiPickersUtilsProvider} from "@material-ui/pickers";
import DateFnsUtils from "@date-io/date-fns";
import {
    applyCardUpdate,
    applyDashVaccine,
    clearDataStampAction,
    setActiveStamp
} from "../../../redux/actions/card_action";


const ApplyCardStamp = (props) => {
    const {selectedVaccine, clearVaccine} = props || {}
    const dispatch = useDispatch();
    const dataStamp = useSelector(state => state.card.dataStamp);
    const {date,notes, dashNurse, lote, dashLab, local, localImage} = dataStamp || {}
    const [saving, setSaving] = useState(false)
    const activeStamp = useSelector(state => state.card.activeStamp);
    const userTable = useSelector(state => state.card.userTable);
    const {type} = activeStamp || {}



    const {vaccine} = selectedVaccine || {}


    const handleSave = () => {
         setSaving(true)
        dataStamp.dashStamp = true
        dispatch(applyDashVaccine(dataStamp, callback))
    }

    const callback = (success, message)=>{
        setSaving(false);
        alert(message);
        if(success){
           clearVaccine()
            dispatch(clearDataStampAction())
            dispatch(setActiveStamp(null))
        }
    }

    useEffect(() => {
        dispatch(applyCardUpdate({prop: 'userTable', value: userTable}))
        dispatch(applyCardUpdate({prop: 'activeStamp', value: activeStamp}))
        dispatch(applyCardUpdate({prop: 'vaccine', value: vaccine}))

        return (() => {
            dispatch(setActiveStamp(null))
        })
    }, [])


    return (
        <div style={{flex: 1, padding: 20, backgroundColor: '#f8f8f8', borderRadius: 10}}>
            <div style={{fontSize: 18}}>{type}</div>


            <div style={{display: 'flex'}}>
                <div style={{flex: 7}}>
                    <div style={{display: 'flex', flexDirection: 'row', marginTop: 20}}>


                        <div style={{marginRight: 20, marginTop: 15}}>
                            <MuiPickersUtilsProvider style={{
                                marginTop: 110,
                            }}
                                                     utils={DateFnsUtils}>
                                <KeyboardDatePicker
                                    value={date}
                                    id="time-picker"
                                    ampm={false}
                                    onChange={(value) => {
                                        dispatch(applyCardUpdate({prop: 'date', value: value}))
                                    }}
                                    label="Data que vacina foi aplicada"

                                />
                            </MuiPickersUtilsProvider>
                        </div>

                        <TextField
                            InputLabelProps={{shrink: true}}
                            label="Lote"
                            /*    error={nameError}
                                helperText={nameError}*/
                            className={{marginLeft: 10, marginRight: 10,}}
                            style={{width: '100%', marginRight: 10}}
                            value={lote}
                            onChange={(e) => {

                                dispatch(applyCardUpdate({prop: 'lote', value: e.target.value,}))
                            }}
                            margin="normal"
                        />
                    </div>


                    <div style={{display: 'flex', flexDirection: 'row',}}>
                        <TextField
                            InputLabelProps={{shrink: true}}
                            label="Aplicador"
                            /*    error={nameError}
                                helperText={nameError}*/
                            className={{marginLeft: 10, marginRight: 10,}}
                            style={{width: '100%', marginRight: 10}}
                            value={dashNurse}
                            onChange={(e) => {

                                dispatch(applyCardUpdate({prop: 'dashNurse', value: e.target.value,}))
                            }}
                            margin="normal"
                        />

                        <TextField
                            InputLabelProps={{shrink: true}}
                            label="Laboratorio"
                            /*    error={nameError}
                                helperText={nameError}*/
                            className={{marginLeft: 10, marginRight: 10,}}
                            style={{width: '100%', marginRight: 10}}
                            value={dashLab}
                            onChange={(e) => {

                                dispatch(applyCardUpdate({prop: 'dashLab', value: e.target.value,}))
                            }}
                            margin="normal"
                        />
                    </div>

                    <div style={{display: 'flex', flexDirection: 'row',}}>
                        <TextField
                            InputLabelProps={{shrink: true}}
                            label="Cidade, Empresa, outros..."
                            /*    error={nameError}
                                helperText={nameError}*/
                            className={{marginLeft: 10, marginRight: 10,}}
                            style={{width: '100%', marginRight: 10}}
                            value={notes}
                            onChange={(e) => {

                                dispatch(applyCardUpdate({prop: 'notes', value: e.target.value,}))
                            }}
                            margin="normal"
                        />


                    </div>




                    <div style={{display: 'flex', flexDirection: 'row', marginTop: 20}}>
                        <Button
                            fontSize={14}
                            color={saving ? '#ccc' : purpleTitle}
                            title={saving ? 'Salvando aguarde' : 'Salvar'}
                            onClick={handleSave}/>
                    </div>
                </div>
                <div style={{flex: 3}}>
                    <img
                        className="img-previ" src={localImage} alt="Destination"/>
                </div>
            </div>
        </div>
    )
}

export default ApplyCardStamp;