import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom'

import red from '@material-ui/core/colors/red';
import {purpleTitle, titleColor} from "../../../common/colors/index";


import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';

import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import {getFormatedDate} from "../../../common/functions";
import {setActiveCardAction} from "../../../redux/actions/card_action";
import {useDispatch} from "react-redux";


const ItemCard = (props) => {
    const dispatch = useDispatch();


    const {item} = props;
    const {_id, createAt, status, userId} = item||{}
    const {name, lastName} = userId||{}


    const getColor = ()=>{
        if(status ==='Aguardando verificação'){
            return 'red'
        }else if(status === 'Verificada'){
            return purpleTitle
        }else if(status === 'Imagem Ilegivel'){
            return 'orange'
        }
    }




    return (
        <Link
            style={{marginRight: 10, borderRadius: 10, marginBottom: 10}}
            onClick={() => {
                dispatch(setActiveCardAction(item))
            }}
            to={`/cards/verify`}
           key={_id}
            className="card">


                <div style={{flexWrap: 'wrap', display: 'flex', flexDirection: 'row',}}>

                    <div style={{flex: 9, display: 'flex', flexDirection: 'column', justifyContent: "center",}}>
                        <ExpansionPanel>
                            <ExpansionPanelSummary
                                expandIcon={<ExpandMoreIcon/>}
                                aria-controls="panel1a-content"
                                id="panel1a-header"
                            >
                                <div style={{display: 'flex', flexDirection: 'row', fontWeight: '700', color: titleColor}}>
                                    <div style={{marginRight: 10,  width: 100}}>{getFormatedDate(createAt)} </div>
                                    <div style={{marginRight: 10, width: 100}}>{name} {lastName} </div>
                                    <div style={{color: getColor()}} >{status} </div>



                                </div>

                            </ExpansionPanelSummary>

                        </ExpansionPanel>

                    </div>


                </div>



        </Link>
    );

}

ItemCard.propTypes = {
    classes: PropTypes.object.isRequired,
};




export default ItemCard
