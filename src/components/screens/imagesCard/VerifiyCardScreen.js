import React, {useCallback, useEffect, useRef, useState} from 'react';


import {useDispatch, useSelector} from "react-redux";

import {makeStyles} from "@material-ui/core/styles";
import {changeCardStatusAction, fetchUserVacineTable, setActiveStamp} from "../../../redux/actions/card_action";
import SelectedVaccineComponent from "./SelectedVaccineComponent";
import ApplyCardStamp from "./ApplyCardStamp";
import DropDown from "../../../common/components/DropDown";
import "react-image-crop/dist/ReactCrop.css";
import ImageCropper from "./ImageCropper";

const useStyles = makeStyles({
    card: {
        maxWidth: 800,
    },
    media: {
        height: 600,
    },
});





const VerififyCardScreen = () => {
    const imgRef = useRef(null);
    const previewCanvasRef = useRef(null);
    const dispatch = useDispatch();
    const activeCard = useSelector(state => state.card.activeCard);
    const userTable = useSelector(state => state.card.userTable);
    const {table} = userTable ||[]
    const [selectedVaccine, setSelectedVaccine] = useState(null)
    const activeStamp = useSelector(state => state.card.activeStamp);

    const options =  [
        {value: 'Aguardando verificação', label:'Aguardando verificação'},
        {value: 'Verificada', label:'Verificada'},
        {value: 'Imagem Ilegivel', label:'Imagem Ilegivel'},
    ]

    const {imageId, userId, status, _id } = activeCard ||{}
    const {bigUrl, mediumUrl} = imageId ||{}


    console.log(userId);

    const classes = useStyles();

    useEffect(() => {
        dispatch(fetchUserVacineTable(userId?._id))
    }, [])

    const renderList = () => {
        return table?.map((item)=>{
            return(
                <div
                    onClick={()=>{
                        setSelectedVaccine(item)
                        dispatch(setActiveStamp(null))
                    }}
                    style={{fontSize: 12, padding: 3, cursor: 'pointer'}}>{item.vaccine.name}</div>
            )
        })
    }

    const onLoad = useCallback((img) => {

        imgRef.current = img;
    }, []);



    return (
        <div style={{display: 'flex',flex: 1,  flexDirection: 'column'}}>
            <div>
                <DropDown
                    error={''}
                    value={status}
                    setValue={(value) => {
                        dispatch(changeCardStatusAction(_id, value))
                    }}
                    data={options}

                />
            </div>

            <div style={{flexDirection: 'row', display: 'flex'}}>
                <ImageCropper
                 src={mediumUrl}
                />

            </div>




            <div style={{display: 'flex',flex: 1, padding: 10, flexDirection: 'row'}}>
                <div style={{ padding: 10, flex: 2 }}>


                {renderList()}
                </div>
                <div style={{ padding: 10, flex: 2}}>
                    <SelectedVaccineComponent
                        item={selectedVaccine}
                    />

                </div>

                <div style={{flex: 6,padding: 10}}>
                    {activeStamp &&
                    <ApplyCardStamp
                        clearVaccine={()=>{
                            dispatch(fetchUserVacineTable(userId._id))
                            setSelectedVaccine(null)}}
                        selectedVaccine={selectedVaccine}/>
                    }

                </div>
            </div>



        </div>



    )


}



export default VerififyCardScreen