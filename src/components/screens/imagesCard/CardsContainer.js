import React from 'react';
import {Route} from 'react-router-dom'

import AppBar from '../../../common/components/AppBar'
import ListCardScreen from "./ListCardScreen";
import VerififyCardScreen from "./VerifiyCardScreen";


const CardsContainer  = ({ match })=>{

    return(


        <div style={{display: 'flex', flex: 1, flexDirection: 'column'}}>
            <AppBar title={'Verificação de Carteiras'}/>

            <div style={{display: 'flex', flex: 1, flexDirection: 'column', padding: 20}}>
                <Route path={'/cards'} exact  component={ListCardScreen}/>
                <Route path={'/cards/verify'} exact component={VerififyCardScreen}/>

            </div>




        </div>


    )
}

export default CardsContainer;