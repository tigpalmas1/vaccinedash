import React, {useRef, useState} from 'react';
import Cropper from "react-cropper";
import "cropperjs/dist/cropper.css";
import "./ImageCropper.css"
import {Button} from "../../../common/components/Button";
import {purpleTitle} from "../../../common/colors";
import {useDispatch, useSelector} from "react-redux";
import {applyCardUpdate} from "../../../redux/actions/card_action";
import {DataURIToBlob, getBase64File} from "../../../common/functions/imageUtil";
import Tag from "../../../common/components/Tag";

const ImageCropper = (props) => {
    const [cropper, setCropper] = useState()
    const [degree, setDegree] = useState(0)
    const cropperRef = useRef(null);
    const {src} = props || {}
    const imageElement = useRef()
    const dispatch = useDispatch();
    const [imageDestination, setImageDestination] = useState();
    const dataStamp = useSelector(state => state.card.dataStamp);
    const activeStamp = useSelector(state => state.card.activeStamp);
    const {localImage} = dataStamp || {}

    console.log(src);


    const onCrop = () => {
        const imageElement = cropperRef?.current;
        const cropper = imageElement?.cropper;
        setImageDestination(cropper.getCroppedCanvas().toDataURL());
        dispatch(applyCardUpdate({prop: 'localImage', value: null}))
    };


  /*  useEffect(() => {
        var cropper = new Cropper(imageElement.current, {
            zoomable: true,
            scalable: true,
            rotatable: true,
            dragMode: "move",
            aspectRatio: "free",
            crop: () => {
                const canvas = cropper.getCroppedCanvas();
                setImageDestination(canvas.toDataURL("image/png"))
                dispatch(applyCardUpdate({prop: 'localImage', value: null}))
            },

        })

    }, [])
*/
    const handleAddImage = async ()=>{
        const file =await DataURIToBlob(imageDestination)
        const base64 = await getBase64File(file);
        dispatch(applyCardUpdate({prop: 'localImage', value: imageDestination}))
        dispatch(applyCardUpdate({prop: 'image64', value: base64}))
    }


    return (
        <div>

        <div style={{display: 'flex'}}>
            <div  style={{width: 700, height: 500, }}>
                <Cropper
                    src={src}
                    style={{ height: '100%', width: "100%" }}
                    // Cropper.js options
                    initialAspectRatio={1 / 1}
                    guides={false}
                    crop={onCrop}
                    draggable={true}
                    ref={cropperRef}
                    onInitialized={(instance) => {
                        setCropper(instance);
                    }}
                />


            </div>




            <div style={{display: 'flex', flexDirection: 'column', bacgroundColor: 'pink'}}>
                <img
                    className="img-previ" src={imageDestination} alt="Destination"/>


                {activeStamp &&
                <div style={{display: 'flex', marginLeft: 20, flexDirection: 'row', marginTop: 20}}>
                    <Button
                        fontSize={14}
                        color={purpleTitle}
                        title={localImage ? 'Imagem Adicionada' : 'Adicionar Image'}
                        onClick={handleAddImage}/>
                </div>
                }


            </div>
        </div>
            <div style={{display: 'flex'}}>
                <Tag
                    onClick={()=>{
                        setDegree(degree - 45)

                        cropper.rotateTo(degree)
                    }}
                    backgroundColor={purpleTitle}
                    title={'Esquerda'}
                />
                <Tag
                    onClick={()=>{
                        setDegree(degree + 45)

                        cropper.rotateTo(degree)
                    }}
                    backgroundColor={purpleTitle}
                    title={'Direita'}
                />
            </div>
        </div>
    )

}

export default ImageCropper

