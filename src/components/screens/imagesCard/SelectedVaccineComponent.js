import React from 'react';
import {useDispatch} from "react-redux";
import {setActiveStamp} from "../../../redux/actions/card_action";
import {purpleTitle} from "../../../common/colors";


const SelectedVaccineComponent  = (props)=>{
    const dispatch = useDispatch();
    const {item} = props ||{}
    const {vaccine, stamp} = item ||{}
    const {name} = vaccine ||{}


    const renderList = () => {
        return stamp?.map((item)=>{
            const {alreadyHad} = item ||{}

            return(
                <div
                    onClick={()=>dispatch(setActiveStamp(item))}
                    style={{width: 100,alignItems: 'center',justifyContent: 'center',display: 'flex',
                    marginRight: 5, borderRadius: 10,
                    height: 100,

                        backgroundColor: alreadyHad? purpleTitle : '#ccc', borderWidth: 1, borderColor: 1}}>
                <div
                    style={{fontSize: 12,

                        padding: 3, cursor: 'pointer'}}>{item.type}</div>
                </div>
            )
        })
    }


    return(


        <div style={{display: 'flex', flex: 1, flexDirection: 'column'}}>
            <div style={{fontSize: 18}}>{name}</div>

            <div style={{display: 'flex',marginTop: 20, flexDirection: 'row'}}>
            {renderList()}
            </div>
        </div>


    )
}

export default SelectedVaccineComponent;