import React from 'react';
import {titleColor} from "../../common/colors/index";






const EmptyState = ({message}) => {


    return (
        <div style={{
            width: '100%',
            height: 500,
            fontSize: 24,
            color: titleColor,
            fontWeight: '700',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center'
        }}>




            <div style={{marginTop:20, color: 'grey'}}>{message}</div>

        </div>
    )
}

const styles = {}

export default EmptyState;