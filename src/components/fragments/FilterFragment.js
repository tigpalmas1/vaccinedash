import React from 'react';
import {purpleTitle} from "../../common/colors/index";
import Tag from '../../common/components/Tag'
import {connect} from 'react-redux';


class FilterFragment extends  React.Component{


    checkDate(date){
        if(date===""){
            return "Todos os Dias";
        } else if(date==="today"){
            return "Hoje";
        }

        else if(date==="week"){
            return "Essa Semana";
        }
        else if(date==="weekend"){
            return "Fim de Semana";
        }
        else if(date==="weekend"){
            return "Fim de Semana";
        }

    }

    checkType(type){
          if(type==="all"){
            return "Todos os tipos";
        }
       else if(type==="nightclub"){
            return "Baladas";
        } else if(type==="bar"){
            return "Bares";
        }
        else if(type==="showClubs"){
            return "Casas de Show";
        }


    }

    checkValue(value){
        if(value==="all"){
            return "Todos os Valores";
        }
        else if(value==="paid"){
            return "Pagos";
        } else if(value==="free"){
            return "Gratuitos";
        }

    }


    render () {

        const {hasCity, hasDate,hasFavorit,
            hasMap,hasStyle, onMapPress,hasType,hasValue,noDescriptionClick, noDescription,
            onDateClick, onCityClick, onStyleClick, onFavoritClick, onTypeClick, onValueClick} = this.props;
        const {favorit,  distance, date, type, selectedCity, tags, selectedTags, value} = this.props.preference||{}

        return (
            <div   style={{marginTop: -5,  flexDirection: 'row',display: 'flex', justifyContent:'space-between'}}>
                <div style={{display: 'flex', flexDirection: 'row'}}>
                    <div  >
                        <Tag
                            onClick={onCityClick}
                            title={`${selectedCity.name} ${selectedCity.state}`} iconName={"map marker alternate"}/>
                    </div>

                    {hasFavorit &&
                    <Tag
                        onClick={onFavoritClick}
                        fontSize={8} title={"Favoritos"} iconName={"heart"}/>
                    }


                    {noDescription &&
                    <Tag
                        onClick={noDescriptionClick}
                        fontSize={8} title={"Sem Descrição"} iconName={"edit"}/>
                    }




                    {hasDate &&
                    <Tag onClick={onDateClick}
                         fontSize={8} title={this.checkDate(date)} iconName={"calendar"}/>
                    }

                    {hasType &&
                    <div style={{display: 'flex', flexDirection: 'row'}}>

                        <Tag
                            onClick={onTypeClick}
                            fontSize={8} title={this.checkType(type)} iconName={"glass martini"}/>


                    </div>
                    }



                    {hasStyle &&
                    <div style={{display: 'flex', flexDirection: 'row'}}>
                        {selectedTags.length === 0 &&
                        <Tag
                            onClick={onStyleClick}
                            fontSize={8} title={"Todos os Estilos"} iconName={"music"}/>
                        }

                        {selectedTags.map (tag =>
                            <Tag
                                onClick={onStyleClick}
                                fontSize={8} title={tag.name} iconName={"music"}/>
                        ) }

                    </div>
                    }

                    {hasValue &&
                    <div style={{display: 'flex', flexDirection: 'row'}}>

                        <Tag
                            onClick={onValueClick}
                            fontSize={8} title={this.checkValue(value)} iconName={"dollar sign"}/>




                    </div>
                    }





                </div>
                {hasMap &&
                    <Tag
                        onClick={onMapPress}
                        fontSize={12} title={"Ver No Mapa"} iconName={"map"}/>

                }


            </div>
        )
    }

}


const mapStateToProps = (state) => {

    return {
       preference: state.filter.preference,
    }


}

export default connect(mapStateToProps)(FilterFragment);
