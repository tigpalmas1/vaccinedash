import React, {useState} from 'react';
import {titleColor} from "../../common/colors/index";
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import PurpleCheckBox from '../../common/components/PurpleCheckBox'
import TextField from '@material-ui/core/TextField';
import Tag from "../../common/components/Tag";
import {subTitleStyle} from "../../common/styles/geralStyles";
import DropDown from '../../common/components/DropDown'
import _ from 'lodash'
import {addressUpdateAction, searchCepAction} from "../../redux/actions";
import {connect, useDispatch} from "react-redux";

const MapNewFragment = (props) => {
    const dispatch = useDispatch();
    const {addressUpdateAction, address, cityError, setCityError} = props || {}


    const {street, number, neighborhood,  postalCode, city, state, lat, long, complement} = address[0] || []



    const  callback = (data)=>{
      const {localidade, logradouro, bairro, uf,} = data ||{}
      addressUpdateAction({prop: 'street', value: logradouro,})
      addressUpdateAction({prop: 'city', value: localidade,})
      addressUpdateAction({prop: 'state', value: uf,})
      addressUpdateAction({prop: 'neighborhood', value: bairro,})
    }

    return (
        <div style={{width: '100%'}}>
            <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                <div style={{fontSize: 24, color:  titleColor,
                    marginTop: 20, fontWeight: '700'}}>Localização</div>



            </div>




            <div>

                <div style={{display: 'flex', flexDirection: 'row'}}>
                    <TextField
                        id="standard-basic"
                        label="CEP"
                        onBlur={()=>{
                            dispatch(searchCepAction(postalCode,callback))
                        }}
                        error={''}
                        helperText={''}
                        placeholder="Número do Cep"
                        className={{marginLeft: 10, marginRight: 10,}}
                        style={{width: '100%', marginRight: 10}}
                        value={postalCode}
                        onChange={(e) => {
                            addressUpdateAction({prop: 'postalCode', value: e.target.value,}, 0)
                        }}
                        margin="normal"
                    /></div>

                <div style={{display: 'flex', flexDirection: 'row'}}>
                    <TextField
                        id="standard-basic"
                        label="Rua"
                        error={''}
                        helperText={''}
                        placeholder="Nome da rua"
                        className={{marginLeft: 10, marginRight: 10,}}
                        style={{width: '100%', marginRight: 10, flex: 7}}
                        value={street}
                        onChange={(e) => {
                            addressUpdateAction({prop: 'street', value: e.target.value,})
                        }}
                        margin="normal"
                    />

                    <TextField
                        id="standard-basic"
                        label='Número'
                        error={''}
                        helperText={''}
                        placeholder="Número"
                        className={{marginLeft: 10, marginRight: 10,}}
                        style={{width: '100%', marginRight: 10, flex: 3}}
                        value={number}
                        onChange={(e) => {
                            addressUpdateAction({prop: 'number', value: e.target.value,})
                        }}
                        margin="normal"
                    />
                </div>

                <div style={{display: 'flex', flexDirection: 'row'}}>
                    <TextField
                        id="standard-basic"
                        label="Cidade"
                        error={cityError}
                        helperText={cityError}
                        placeholder="Cidade"
                        className={{marginLeft: 10, marginRight: 10,}}
                        style={{width: '100%', marginRight: 10, flex: 4}}
                        value={city}
                        onChange={(e) => {
                            setCityError()
                            addressUpdateAction({prop: 'city', value: e.target.value,})
                        }}
                        margin="normal"
                    />


                    <TextField
                        id="standard-basic"
                        label="Estado"
                        error={''}
                        helperText={''}
                        placeholder="Estado"
                        className={{marginLeft: 10, marginRight: 10,}}
                        style={{width: '100%', marginRight: 10, flex: 4}}
                        value={state}
                        onChange={(e) => {
                            addressUpdateAction({prop: 'state', value: e.target.value,})
                        }}
                        margin="normal"
                    />


                </div>

                <div style={{display: 'flex', flexDirection: 'row'}}>
                    <TextField
                        id="standard-basic"
                        label="Bairro"
                        error={''}
                        helperText={''}
                        placeholder="Nome do Bairro"
                        className={{marginLeft: 10, marginRight: 10,}}
                        style={{width: '100%', marginRight: 10, flex: 4}}
                        value={neighborhood}
                        onChange={(e) => {
                            addressUpdateAction({prop: 'neighborhood', value: e.target.value,})
                        }}
                        margin="normal"
                    />


                    <TextField
                        id="standard-basic"
                        label="Complemento"
                        error={''}
                        helperText={''}
                        placeholder="Complemento"
                        className={{marginLeft: 10, marginRight: 10,}}
                        style={{width: '100%', marginRight: 10, flex: 4}}
                        value={complement}
                        onChange={(e) => {
                            addressUpdateAction({prop: 'complement', value: e.target.value,})
                        }}
                        margin="normal"
                    />


                </div>

                <div style={{display: 'flex', flexDirection: 'row'}}>
                    <TextField
                        id="standard-basic"
                        label="Latitude"
                        error={''}
                        helperText={''}
                        placeholder="Latitude"
                        className={{marginLeft: 10, marginRight: 10,}}
                        style={{width: '100%', marginRight: 10, flex: 4}}
                        value={lat}

                        onChange={(e) => {
                            addressUpdateAction({prop: 'lat', value: e.target.value,})
                        }}
                        margin="normal"
                    />

                    <TextField
                        id="standard-basic"
                        label="Longiutde"
                        error={''}
                        helperText={''}
                        placeholder="Longitde"
                        className={{marginLeft: 10, marginRight: 10,}}
                        style={{width: '100%', marginRight: 10, flex: 4}}
                        value={long}
                        onChange={(e) => {
                            addressUpdateAction({prop: 'long', value: e.target.value,})
                        }}
                        margin="normal"
                    />


                </div>


            </div>




        </div>
    )
}

const styles = {}


const mapStateToProps = (state) => {


    return {


    }
}


export default connect(mapStateToProps, {addressUpdateAction})(MapNewFragment)
