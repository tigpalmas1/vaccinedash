import React,{useState,useEffect} from 'react';
import {titleColor} from "../../common/colors/index";
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import TextField from '@material-ui/core/TextField';
import Tag from "../../common/components/Tag";
import {subTitleStyle} from "../../common/styles/geralStyles";
import {connect} from "react-redux";
import {fetch_version} from "../../redux/actions";


const VersionFragment = (props) => {
    const {fetch_version} = props ||{}
    const {data} = props ||{}
    const {androidVersion, iosVersion, dashVersion} = data ||{}
    console.log(data)


    useEffect(()=>{
        fetch_version()
    },[])


    return (
        <div style={{width: '80%'}}>
            <div style={subTitleStyle}>Versão</div>
            <div style={{flexDirection: 'column', display: 'flex', alignItems: 'center'}}>

                <div>{androidVersion}</div>
                <div>{iosVersion}</div>
                <div>{dashVersion}</div>



            </div>


            <div>

            </div>


        </div>
    )
}

const styles = {}



const mapStateToProps = (state) => {


    return {
        data: state.administration.data
    }
}


export default connect(mapStateToProps, {fetch_version})(VersionFragment)
