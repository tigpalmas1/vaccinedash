import React from 'react';
import {Router, Route, Switch, Redirect} from 'react-router-dom';
import Home from "../../Home";
import UserContainerScreen from "../../screens/user/UserContainerScreen";
import LoginPage from "../../screens/auth/LoginPage";
import {PublicLayout} from "../../container/PublicLayout";
import PrivateLayout from "../../container/PrivateLayout";
import history from '../../../history';
import EventContainer from "../../screens/event/EventContainer";
import EstablishentsContainer from "../../screens/establishments/EstablishmentsContainer";
import WareHouseListSccreen from "../../screens/warehouse/WareHouseListSccreen";
import NewsListScreen from "../../screens/news/NewsListScreen";
import AdministrationScreen from "../../screens/administration/AdministrationScreen";
import CreateEstablishmentScreen from "../../screens/establishments/CreateEstablishmentScreen";
import NurseContainerScreen from "../../screens/nurse/NurseContainerScreen";
import ListCardScreen from "../../screens/imagesCard/ListCardScreen";
import CardsContainer from "../../screens/imagesCard/CardsContainer";
import TablesContainer from "../../screens/tables/TableContainer";

class MainContainer extends React.Component {
    render() {
        return(


            <div style={{display: 'flex', flex: 1}}>
                <Router history={history}>
                    <Switch>
                        <PublicLayout path="/login" exact component={LoginPage} />
                        <Redirect from="/" exact to="/login" />

                        <PrivateLayout path="/home" exact component={Home} />

                        <PrivateLayout path="/establishments" exact component={EstablishentsContainer} />
                        <PrivateLayout path="/establishments/create" exact component={EstablishentsContainer} />
                        <PrivateLayout path="/editEstablishment"  component={CreateEstablishmentScreen} />


                        <PrivateLayout path="/tables"  component={TablesContainer} />
                        <PrivateLayout path="/cards"  component={CardsContainer} />

                        <PrivateLayout path="/wharehouse" exact component={WareHouseListSccreen} />
                        <PrivateLayout path="/news" exact component={NewsListScreen} />

                        <PrivateLayout path="/events" exact component={EventContainer} />
                        <PrivateLayout path="/events/send"  component={EventContainer} />

                        <PrivateLayout path="/nurse" exact component={NurseContainerScreen} />
                        <PrivateLayout path="/nurse/create"  component={NurseContainerScreen} />

                        <PrivateLayout path="/contact" exact component={UserContainerScreen} />
                        <PrivateLayout path="/contact/create"  component={UserContainerScreen} />




                        <PrivateLayout path="/administration"  component={AdministrationScreen} />


                        <Redirect from="*" exact to="/login" />


                    </Switch>
                </Router>
            </div>

        )
    }
}

export default MainContainer;