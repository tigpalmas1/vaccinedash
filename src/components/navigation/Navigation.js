import {connect} from 'react-redux';

import React from 'react';
import {NavLink} from 'react-router-dom'
import { logoBlue, mainOrange} from "../../common/colors/index";
import{logoutAction, setActiveEstablishmentAction} from "../../redux/actions";
import {Icon} from 'semantic-ui-react'

const Navigation =(props)=> {

    const {logoutAction, data, activeEstablishment, setActiveEstablishmentAction} = props
    const {permission, user} = data ||{}
    const {name: userName} = user ||{}
    const {isAdmin, establishmentId,  isManager} = permission ||{}



        return(
            <div style={{display:'flex', flexDirection: 'column',  height: '100vh',
                backgroundColor: mainOrange}}>


                <div style={{display: 'flex',flexDirection: 'row',  padding: 20, alignItems: 'center'}}>
                    {activeEstablishment &&
                    <div
                        onClick={()=> setActiveEstablishmentAction(null)}
                        style={{ padding: 5, borderRadius: 15, marginRight: 5}}>
                        <Icon style={{marginRight: 0, fontSize: 18}}  name='arrow left'/>

                    </div>
                    }

                    <div style={{ display: 'flex',
                        fontSize: 20, fontWeight: '700',
                         }}

                    >
                        {userName? userName: ''}
                    </div>
                </div>



                <NavLink to="/home"  exact
                         style={{color: 'white', fontWeight: '700', fontSize: 18, marginTop: 10, paddingLeft: 20, paddingRight: 20 }}
                         activeStyle={{color: logoBlue,  fontSize: 22, marginTop: 10, background: '#00000050',}}>Home</NavLink>

                {isAdmin && !activeEstablishment &&
                <NavLink to="/establishments"  exact
                         style={{color: 'white', fontWeight: '700', fontSize: 18, marginTop: 10,  paddingLeft: 20, paddingRight: 20}}
                         activeStyle={{color: logoBlue, fontSize: 22, marginTop: 10,  background: '#00000050',}}>Unidades</NavLink>
                }

                <NavLink to="/events"  exact
                         style={{color: 'white', fontWeight: '700', fontSize: 18, marginTop: 10,  paddingLeft: 20, paddingRight: 20}}
                         activeStyle={{color: logoBlue, fontSize: 22, marginTop: 10,  background: '#00000050',}}>Vacinas</NavLink>


                {( isAdmin) &&
                <NavLink to="/wharehouse" exact
                         style={{
                             color: 'white',
                             fontWeight: '700',
                             fontSize: 18,
                             marginTop: 10,
                             paddingLeft: 20,
                             paddingRight: 20
                         }}
                         activeStyle={{color: logoBlue, fontSize: 22, marginTop: 10, background: '#00000050',}}>Estoque</NavLink>

                }

                {isAdmin && !activeEstablishment &&
                <NavLink to="/tables"  exact
                         style={{color: 'white', fontWeight: '700', fontSize: 18, marginTop: 10,  paddingLeft: 20, paddingRight: 20}}
                         activeStyle={{color: logoBlue, fontSize: 22, marginTop: 10,  background: '#00000050',}}>Carteiras</NavLink>
                }


                {isAdmin && !activeEstablishment &&
                <NavLink to="/cards"  exact
                         style={{color: 'white', fontWeight: '700', fontSize: 18, marginTop: 10,  paddingLeft: 20, paddingRight: 20}}
                         activeStyle={{color: logoBlue, fontSize: 22, marginTop: 10,  background: '#00000050',}}>Imagens</NavLink>
                }

                {isAdmin && !activeEstablishment &&
                <NavLink to="/news"  exact
                         style={{color: 'white', fontWeight: '700', fontSize: 18, marginTop: 10,  paddingLeft: 20, paddingRight: 20}}
                         activeStyle={{color: logoBlue, fontSize: 22, marginTop: 10,  background: '#00000050',}}>Noticias</NavLink>
                }

                {(isAdmin || isManager) &&
                <NavLink to="/nurse"  exact
                         style={{color: 'white', fontWeight: '700', fontSize: 18, marginTop: 10,  paddingLeft: 20, paddingRight: 20}}
                         activeStyle={{color: logoBlue, fontSize: 22, marginTop: 10,  background: '#00000050',}}>Aplicadores</NavLink>

                }


                {(isAdmin || isManager) &&
                <NavLink to="/contact"  exact
                         style={{color: 'white', fontWeight: '700', fontSize: 18, marginTop: 10,  paddingLeft: 20, paddingRight: 20}}
                         activeStyle={{color: logoBlue, fontSize: 22, marginTop: 10,  background: '#00000050',}}>Usuários</NavLink>

                }


                {(establishmentId  || activeEstablishment) &&
                <NavLink to="/editEstablishment"  exact
                         style={{color: 'white', fontWeight: '700', fontSize: 18, marginTop: 10,  paddingLeft: 20, paddingRight: 20}}
                         activeStyle={{color: logoBlue, fontSize: 22, marginTop: 10,  background: '#00000050',}}>Editar Casa</NavLink>

                }

               {/* {(isAdmin && !activeEstablishment) &&
                <NavLink to="/administration"  exact
                         style={{color: 'white', fontWeight: '700', fontSize: 18, marginTop: 10,  paddingLeft: 20, paddingRight: 20}}
                         activeStyle={{color: logoBlue, fontSize: 22, marginTop: 10,  background: '#00000050',}}>Administração</NavLink>

                }
*/}



                <NavLink
                    to="/login"
                    onClick={()=>logoutAction()}  exact
                         style={{color: 'white', fontWeight: '700', fontSize: 14, marginTop: 40,  paddingLeft: 20, paddingRight: 20}}
                         activeStyle={{color: logoBlue, fontSize: 22, marginTop: 5,  background: '#00000050',}}>Sair</NavLink>


            </div>
        )


}

const mapStateToProps = (state) => {
    return {
        data: state.user.data,
        activeEstablishment: state.establishment.activeEstablishment
    }
}

export default connect(mapStateToProps, {logoutAction, setActiveEstablishmentAction})(Navigation);