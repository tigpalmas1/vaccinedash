import React, {useState, useEffect} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import MultipleSelect from '../../common/components/MultipleSelect'


import {connect} from "react-redux";
import {setPermissionAction, setUserAction, userUpdateAction} from "../../redux/actions";
import {userStatus, oneUserType} from "../../common/constants/index";
import DropDown from "../../common/components/DropDown";
import _ from 'lodash'

const PermissionDialog = (props) => {

    const { userUpdateAction } = props
    const { item, establishments, setPermissionAction, setUserAction,setOpen, open } = props

    const {user, permission} = item || {}
    const {name} = user || {}
    const {
        _id, schoolId, isNurse, isManager, isAdmin, isDoctor
    } = permission ||{}



    console.log(establishments)



    const [saving, setSaving] = useState(false)







    const savePermission = () => {

        setSaving(true)
         setPermissionAction(_id, permission, callback)
    }

    const callback = (message)=>{
        alert(message)
        setSaving(false)
        setOpen(false)
    }

    return (
        <div>

            <Dialog
                fullWidth={true}
                open={open} onClose={() => setOpen(false)} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">{`Permissões ${name}`}</DialogTitle>


                {!saving &&
                <div>
                    <DialogContent>
                        <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center', }}>

                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={isAdmin}
                                        onChange={() => userUpdateAction({
                                            prop: 'isAdmin',
                                            value: !isAdmin
                                        })}
                                        value="checkedI"
                                    />
                                }
                                label="Administrador"
                            />

                        </div>

                    </DialogContent>


                    <DialogContent>
                        <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center', }}>

                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={isManager}
                                        onChange={() => userUpdateAction({
                                            prop: 'isManager',
                                            value: !isManager
                                        })}
                                        value="checkedI"
                                    />
                                }
                                label="Diretor"
                            />


                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={isDoctor}
                                        onChange={() => userUpdateAction({
                                            prop: 'isDoctor',
                                            value: !isDoctor
                                        })}
                                        value="checkedI"
                                    />
                                }
                                label="Doutor"
                            />

                            <FormControlLabel
                                control={
                                    <Checkbox
                                        checked={isNurse}
                                        onChange={() => userUpdateAction({
                                            prop: 'isNurse',
                                            value: !isNurse
                                        })}
                                        value="checkedI"
                                    />
                                }
                                label="Aplicador"
                            />

                        </div>

                    </DialogContent>

                    <DialogContent>
                        <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center', }}>


                        </div>

                    </DialogContent>

                    <DialogContent>



                    </DialogContent>










                    <DialogActions>
                        <Button onClick={setOpen} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={savePermission} color="primary">
                            Salvar
                        </Button>
                    </DialogActions>
                </div>
                }

                {saving &&
                    <DialogContent>
                    <div>Salvando, aguarde...</div>
                    </DialogContent>
                }



            </Dialog>
        </div>
    );
}

const mapStateToProps = (state) => {

    return {
        item: state.user.item,
        establishments: state.establishment.establishments,
    }
}


export default connect(mapStateToProps, {setPermissionAction, setUserAction, userUpdateAction})(PermissionDialog)