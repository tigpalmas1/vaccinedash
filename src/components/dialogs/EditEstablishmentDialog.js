import React,{useState, useEffect} from 'react';
import Button from '@material-ui/core/Button';
import {withStyles} from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import {connect} from "react-redux";

import Switch from '@material-ui/core/Switch';
import {purpleTitle, lightPurple} from "../../common/colors/index";
import {saveEstablishmentAction} from "../../redux/actions/establishments_action";

const PurpleSwitch = withStyles({
    switchBase: {
        color: lightPurple,
        '&$checked': {
            color: purpleTitle,
        },
        '&$checked + $track': {
            backgroundColor: lightPurple,
        },
    },
    checked: {},
    track: {},
})(Switch);


const EditEstablishmentDialog = (props) => {
    const {open, handleClose, establishment, } = props||{}
    const {name} = establishment ||{}
    const {saveEstablishmentAction } = props||{}

    const [priority, setPrioryty] = useState()
    const [automaticEventLoad, setautomaticEventLoad] = useState()
    const [number, setNumber] = useState()
    const [saving, setSaving] = useState(false)

    useEffect(()=>{
        console.log('chamou aqui')
        console.log(establishment)
        setPrioryty(establishment.priority)
        setautomaticEventLoad(establishment.automaticEventLoad)
        setNumber(establishment.number)
    },[establishment])

    const editEstablishment = ()=>{
        setSaving(true)
        establishment.number = number
        establishment.automaticEventLoad = automaticEventLoad
        establishment.priority = priority
        saveEstablishmentAction(establishment, establishment, callback)
    }

    const callback = (message)=>{
        setSaving(false)
        alert(message)
    }

    return (
        <div>

            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Editar {name}</DialogTitle>
                <DialogContent>

                    <div style={{display: 'flex', flexDirection: 'row'}}>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            value={priority}
                            label="Prioridade"
                            onChange={(e) => {
                                setPrioryty(e.target.value)
                            }}
                            type="email"
                            fullWidth
                        />
                    </div>

                    <div style={{display: 'flex', flexDirection: 'row'}}>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            value={number}
                            label="Número"
                            onChange={(e) => {
                                setNumber(e.target.value)
                            }}
                            type="email"
                            fullWidth
                        />
                    </div>




                    <div style={{display: 'flex', flexDirection: 'row'}}>
                        <PurpleSwitch
                            onClick={(ev) => {
                                ev.preventDefault()
                                ev.stopPropagation();
                                ev.nativeEvent.stopImmediatePropagation();

                            }}
                            checked={automaticEventLoad}
                            onChange={()=> setautomaticEventLoad(!automaticEventLoad)}
                            value="checkedA"
                        />
                    </div>




                </DialogContent>

                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancelar
                    </Button>
                    <Button
                        onClick={editEstablishment}
                        color="primary">
                        Salvar
                    </Button>
                </DialogActions>


                <DialogActions>
                    <div>{saving? 'Salvando aguarde': ''}</div>
                </DialogActions>





            </Dialog>
        </div>
    );
}


const mapStateToProps = (state) => {


    return {


    }
}


export default connect(mapStateToProps, {saveEstablishmentAction})(EditEstablishmentDialog)
