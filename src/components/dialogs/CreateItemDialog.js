import React, {useState, useEffect} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {MuiPickersUtilsProvider, DateTimePicker} from "material-ui-pickers";
import DateFnsUtils from "@date-io/date-fns";
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import {FilePicker} from 'react-file-picker'
import {connect} from "react-redux";
import {saveItemAction, itemUpdateAction, setActiveItem, deleteItemAction} from "../../redux/actions";


const CreateItemDialog = (props) => {

    const {open, handleClose, category, newItem,data, itemUpdateAction, setActiveItem} = props || []
    const {name, _id: categoryId} = category || []
    const {saveItemAction, deleteItemAction} = props || {}
    const {activeEstablishment} = props || {};



    const {permission} = data ||{}
    const { establishmentId: isEstablishmentLevel} = permission ||{}



    const {label, description, value, size, imageUrl,_id: itemId} = newItem ||{}

    const [saving, setSaving] = useState(false)
    const [idEstablishment, setIdEstablihsment] = useState('')



    useEffect(()=>{
        if( activeEstablishment){
            setIdEstablihsment(activeEstablishment._id)
        }
        if( isEstablishmentLevel){
            setIdEstablihsment(isEstablishmentLevel)
        }
    },[])



    const setFile = (fileObject,) => {
        var path = (window.URL || window.webkitURL).createObjectURL(fileObject);
        itemUpdateAction({prop: 'imageUrl', value: path})
        itemUpdateAction({prop: 'localImage', value: fileObject})

    }


    const setError = (error) => {

    }

    const saveItem = () => {
        setSaving(true)
        newItem.categoryId = categoryId
        newItem.establishmentId = idEstablishment
        saveItemAction(newItem, callback)
    }

    const deleteItem = ()=>{
        setSaving(true)
        deleteItemAction(newItem, callback)
    }

    const callback = (message) => {

        setSaving(false)
        localClose()

    }


    const localClose = ()=>{
        setActiveItem(null)
        handleClose()
    }
    return (
        <div>

            <Dialog open={open} onClose={()=>{


                localClose()
            }} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Novo Item para {name}</DialogTitle>
                <DialogContent>

                    <FilePicker
                        maxSize={500000}
                        extensions={['png', 'jpeg', 'jpg', 'gif']}
                        onChange={FileObject => (setFile(FileObject))}
                        onError={errMsg => (setError(errMsg))}>
                        <Card style={{
                            display: 'flex',
                            flex: 1,
                            width: '100%', height: 250, flexDirection: 'column', borderRadius: 10,
                        }}>
                            <div
                                style={{flex: 1, position: "relative", display: 'flex',}}>
                                <CardMedia
                                    style={{flex: 1, borderRadius: 10,}}
                                    image={imageUrl}
                                />
                            </div>
                        </Card>
                    </FilePicker>


                    <div style={{display: 'flex', flexDirection: 'row', width: 400}}>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            value={label}
                            label="Nome da categoria"
                            placeholder="Cerveja de garrafa, pizza grande"
                            onChange={(e) => {
                                itemUpdateAction({prop: 'label', value: e.target.value})

                            }}
                            type="email"
                            fullWidth
                        />


                    </div>


                    <div style={{display: 'flex', flexDirection: 'row', width: 400}}>
                        <div style={{width: 150, marginRight: 10}}>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                value={size}
                                label="Tamanho"
                                placeholder="500ml..."
                                onChange={(e) => {
                                    itemUpdateAction({prop: 'size', value: e.target.value})

                                }}
                                fullWidth
                            />

                        </div>
                        <div style={{width: 150}}>
                            <TextField
                                autoFocus
                                margin="dense"
                                id="name"
                                value={value}
                                label="Valor R$"
                                placeholder="Digite o valor do item"
                                onChange={(e) => {
                                    itemUpdateAction({prop: 'value', value: e.target.value})

                                }}
                                type="email"
                                fullWidth
                            />
                        </div>

                    </div>


                    <div style={{display: 'flex', flexDirection: 'row', width: 400}}>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            value={description}
                            label="Descrição"
                            placeholder="informe detalhes..."
                            onChange={(e) => {
                                itemUpdateAction({prop: 'description', value: e.target.value})

                            }}
                            fullWidth
                        />


                    </div>


                </DialogContent>

                {!saving &&

                <DialogActions>


                    {itemId &&

                    <Button onClick={()=>{
                        deleteItem()
                    }} color="primary">
                        Apagar Item
                    </Button>
                    }


                    <Button onClick={()=>{
                        localClose()
                    }} color="primary">
                        Cancelar
                    </Button>
                    <Button onClick={() => {saveItem()
                    }} color="primary">
                        {itemId ? 'Editar' :'Salvar'}
                    </Button>
                </DialogActions>
                }

                {saving &&
                <DialogActions>
                    <div>Salvando, aguarde...</div>
                </DialogActions>
                }



            </Dialog>
        </div>
    );
}


const mapStateToProps = (state) => {


    return {
        data: state.user.data,
        activeEstablishment: state.establishment.activeEstablishment,
        newItem: state.cardapio.newItem

    }
}


export default connect(mapStateToProps, {saveItemAction, itemUpdateAction, setActiveItem, deleteItemAction})(CreateItemDialog)
