import React,{useState, useEffect} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import myJson from '../../common/constants/estados-cidades.json'
import {connect} from "react-redux";
import {filterUpdateAction, setFilterAction, stateChangedAction} from "../../redux/actions";

import _ from 'lodash'
import DropDownsStates from './DropDownStates'
import DropDownCities from './DropDownCities'
import DropDownFixedTypes from './DropDownFixedTypes'
import {CalcPicker} from "./CalcPicker";
import {fixedTypeItems} from "../../common/constants/index";


const  CalcDialog = ({filter, filterUpdateAction, stateChangedAction})=> {
    const {searchType, value, state, city, fixedType, } = filter || {}

    const [open, setOpen] = useState(false);
    const [states, setStates] = useState([]);
    const [cities, setCities] = useState([]);


    useEffect(()=>{
        const newArray= _.map(myJson.estados, o => _.extend({label: o.nome, value: o.nome}, o));
        setStates(newArray)
    },[])



    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        console.log('handle close')
        stateChangedAction(true)
        setOpen(false);
    };

    const setState = (value)=>{
      filterUpdateAction({prop: 'state', value: value})
        var tempState = _.find(states, {nome:value});
        const {cidades} = tempState ||{}
        const newCities= _.map(cidades, o => _.extend({label: o, value: o}, o));

        setCities( newCities)
    }

    const setCity = (value)=>{
        filterUpdateAction({prop: 'city', value: value})

    }


    const setValue = name => event => {
        filterUpdateAction({prop: 'value', value: event.target.value})
    }

    return (
        <div >
            <Button variant="outlined" color="primary" onClick={handleClickOpen}>
                Calculadora de Kits
            </Button>
            <Dialog
                fullWidth={true}
                open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Calculadora de Kits</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        <CalcPicker
                            type={searchType}
                            typeUpdate={(type)=>{filterUpdateAction({prop: 'searchType', value: type})}}
                        />
                    </DialogContentText>
                    <DialogContentText>
                        <TextField
                            id="standard-number"
                            label={'Consumo'}
                            value={value}
                            onChange={setValue()}
                            type={'number'}

                            InputLabelProps={{
                                shrink: true,
                            }}
                            margin="normal"
                        />
                    </DialogContentText>
                    <DialogContentText>
                      <DropDownsStates
                          value={state}
                          selectData={(value)=>setState(value)}
                          data={states}/>
                    </DialogContentText>
                    <DialogContentText>
                        <DropDownCities
                            value={city}
                            selectData={(value)=>setCity(value)}
                            data={cities}/>
                    </DialogContentText>

                    <DialogContentText>
                        <DropDownFixedTypes
                            value={fixedType}
                            selectData={(value)=>  filterUpdateAction({prop: 'fixedType', value: value})}
                            data={fixedTypeItems}/>
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={handleClose} color="primary">
                        Calcular
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

const mapStateToProps = (state) => {

    return {
        filter: state.filter.filter,
    }
}


export default connect(mapStateToProps, {filterUpdateAction, setFilterAction, stateChangedAction})(CalcDialog)