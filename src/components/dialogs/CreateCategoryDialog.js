import React, {useState, useEffect} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {MuiPickersUtilsProvider, DateTimePicker} from "material-ui-pickers";
import DateFnsUtils from "@date-io/date-fns";
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import {FilePicker} from 'react-file-picker'
import {connect} from "react-redux";
import {saveCategoryAction} from "../../redux/actions";


const CreateCategoryDialog =(props) =>{

    const {open, handleClose} = props
    const {saveCategoryAction} = props
    const { activeEstablishment, data} = props || {};

    const {permission} = data ||{}
    const { establishmentId: isEstablishmentLevel} = permission ||{}



    const [nameCategory, setNameCategory] = useState('')
    const [idEstablishment, setIdEstablihsment] = useState('')


    useEffect(()=>{
        if( activeEstablishment){
            setIdEstablihsment(activeEstablishment._id)
        }
        if( isEstablishmentLevel){
            setIdEstablihsment(isEstablishmentLevel)
        }
    },[])


    const saveCategory= ()=>{
        saveCategoryAction(
            {name: nameCategory, establishmentId:idEstablishment },
            callback
        )
    }

    const callback = (message)=>{

        handleClose()

    }
    return (
        <div>

            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Nova Categoria</DialogTitle>
                <DialogContent>

                    <div style={{display: 'flex', flexDirection: 'row', width: 400}}>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            value={nameCategory}
                            label="Nome da categoria"
                            placeholder="Porções, hamburgueres, bebidas...etc"
                            onChange={(e) => {
                                setNameCategory(e.target.value)

                            }}
                            type="email"
                            fullWidth
                        />


                    </div>




                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancelar
                    </Button>
                    <Button onClick={saveCategory} color="primary">
                        Salvar
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}



const mapStateToProps = (state) => {


    return {
        data: state.user.data,
        activeEstablishment: state.establishment.activeEstablishment

    }
}


export default connect(mapStateToProps, {saveCategoryAction})(CreateCategoryDialog)
