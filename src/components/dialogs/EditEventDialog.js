import React,{useState, useEffect} from 'react';
import Button from '@material-ui/core/Button';
import {withStyles} from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import {connect} from "react-redux";

import Switch from '@material-ui/core/Switch';
import {purpleTitle, lightPurple} from "../../common/colors/index";
import {editEventAction} from "../../redux/actions";

const PurpleSwitch = withStyles({
    switchBase: {
        color: lightPurple,
        '&$checked': {
            color: purpleTitle,
        },
        '&$checked + $track': {
            backgroundColor: lightPurple,
        },
    },
    checked: {},
    track: {},
})(Switch);


const EditEventDialog = (props) => {
    const {open, handleClose, event, } = props||{}
    const {editEventAction } = props||{}

    const [priority, setPrioryty] = useState()
    const [isEnabled, setIsEnabled] = useState()

    const [saving, setSaving] = useState(false)

    useEffect(()=>{
        setPrioryty(event.priority)
        setIsEnabled(setIsEnabled.isEnabled)

    },[event])



    const editEvent = ()=>{
        setSaving(true)
        event.priority = priority
        event.isEnabled = isEnabled
        editEventAction(event, callback)
    }

    const callback = (message)=>{
        setSaving(false)
        alert(message)
        handleClose()
    }

    return (
        <div>

            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Editar Evento</DialogTitle>
                <DialogContent>


                    <div style={{display: 'flex', flexDirection: 'row'}}>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            value={priority}
                            label="Prioridade do Evento"
                            onChange={(e) => {
                                setPrioryty(e.target.value)
                            }}
                            type="email"
                            fullWidth
                        />
                    </div>




                    <div style={{display: 'flex', flexDirection: 'row'}}>
                        <PurpleSwitch
                            onClick={(ev) => {
                                ev.preventDefault()
                                ev.stopPropagation();
                                ev.nativeEvent.stopImmediatePropagation();

                            }}
                            checked={isEnabled}
                            onChange={()=> setIsEnabled(!isEnabled)}
                            value="checkedA"
                        />
                    </div>




                </DialogContent>

                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancelar
                    </Button>
                    <Button
                        onClick={editEvent}
                        color="primary">
                        Salvar
                    </Button>
                </DialogActions>


                <DialogActions>
                    <div>{saving? 'Salvando aguarde': ''}</div>
                </DialogActions>





            </Dialog>
        </div>
    );
}


const mapStateToProps = (state) => {


    return {


    }
}


export default connect(mapStateToProps, {editEventAction})(EditEventDialog)
