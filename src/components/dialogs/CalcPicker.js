import React, {Component} from 'react';
import {tag, tagWrapper} from "../../common/styles/geralStyles";
import {mainOrange} from "../../common/colors/index";





const CalcPicker = ({typeUpdate, type, typeError}) => {
    const {pickerContainer} = styles;

    return (

        <div style={{marginTop:0}}>

           <div label={'Calcular por:'} error={typeError}/>
            <div style={pickerContainer}>
                <div onClick={() => typeUpdate('kWh')}>
                    <div
                        style={{padding: 5, backgroundColor: mainOrange, flexWrap: 'wrap', borderRadius: 12,  marginRight: 5,    flexDirection: 'row', alignItems:'center',backgroundColor: type === 'kWh' ? mainOrange : 'grey'}}>
                        <div style={tag}>kWh</div>
                    </div>
                </div>
                <div onClick={() => typeUpdate('kWp')}>
                    <div
                        style={{padding: 5, backgroundColor: mainOrange, flexWrap: 'wrap', borderRadius: 12,  marginRight: 5,    flexDirection: 'row', alignItems:'center',backgroundColor: type === 'kWp' ? mainOrange : 'grey'}}>
                        <div style={tag}>kWp</div>
                    </div>

                </div>
                <div onClick={() => typeUpdate('modulesNumber')}>
                    <div
                        style={{padding: 5, backgroundColor: mainOrange, flexWrap: 'wrap', borderRadius: 12,  marginRight: 5,    flexDirection: 'row', alignItems:'center',backgroundColor: type === 'modulesNumber' ? mainOrange : 'grey'}}>
                        <div style={tag}>Número de Módulos</div>
                    </div>

                </div>

            </div>
            <div style={{color: 'red', fontWeight: '700', fontSize: 8}}>{typeError}</div>
        </div>


    )
}

const styles = {
    pickerContainer: {
        width: '100%',
        display:'flex',
        flexDirection: 'row',
        alignItems: 'center'
    }
}

export {CalcPicker};

