import React,{useEffect} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {MuiPickersUtilsProvider, DatePicker, TimePicker} from "material-ui-pickers";
import DateFnsUtils from "@date-io/date-fns";
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import {FilePicker} from 'react-file-picker'
import {connect} from "react-redux";
import {bonusUpdateAction, saveBonusAction} from "../../redux/actions";
import _ from 'lodash'
import DropDown from '../../common/components/DropDown'
import {NotePicker} from "./NotePicker";


const CreateNoteDialog =(props) =>{

    const {open, handleClose} = props
    const {newBonus, activeEvent, classId, preference, chave} = props||{}
    const {selectedStudent} = preference||{}
    const {bonusUpdateAction, saveBonusAction} = props
    const { time, schoolClassId, description, message, type, studentId} = newBonus ||{}



    useEffect(()=>{
        bonusUpdateAction({prop: 'schoolClassId', value: classId})
    },[])







    const saveBonus= ()=>{
        newBonus.to = 'student'
        newBonus.studentId = selectedStudent._id
        newBonus.key = chave
        saveBonusAction(newBonus, callback)
    }

    const callback = (message)=>{
        alert(message)
        handleClose()

    }
    return (
        <div>

            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Nova Nota</DialogTitle>
                <DialogContent>



                    <div style={{display: 'flex', flexDirection: 'row', width: '100%', alignItems: 'center', marginTop: 10}}>
                        <NotePicker
                            type={type}
                            typeUpdate={(value)=> bonusUpdateAction({prop: 'type', value: value,})}
                        />



                    </div>

                    <div style={{display: 'flex', flexDirection: 'row'}}>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            value={message}
                            label="Mensagem"
                            onChange={(e) => {

                                bonusUpdateAction({prop: 'message', value: e.target.value,})
                            }}
                            type="email"
                            fullWidth
                        />


                    </div>






                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancelar
                    </Button>
                    <Button onClick={saveBonus} color="primary">
                        Salvar
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}



const mapStateToProps = (state) => {


    return {
        newBonus: state.bonus.newBonus,
        activeEvent: state.event.activeEvent,
        preference: state.filter.preference,



    }
}


export default connect(mapStateToProps, {bonusUpdateAction, saveBonusAction})(CreateNoteDialog)
