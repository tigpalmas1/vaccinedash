import React,{useState, useEffect} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {MuiPickersUtilsProvider, DateTimePicker} from "material-ui-pickers";
import DateFnsUtils from "@date-io/date-fns";
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import {FilePicker} from 'react-file-picker'
import {connect} from "react-redux";
import DropDown from '../../common/components/DropDown'
import _ from 'lodash';
import {promotedVaccineUpdate, savePromotedVaccineAction} from "../../redux/actions";

const CreatePromoteVaccine = (props) => {

    const {open, handleClose} = props||{}
    const {promotedVaccineUpdate,savePromotedVaccineAction} = props||{}
    const {newPromotedVaccine,kits} = props||{}
    const {message, expireIn, vaccine, imageUrl} = newPromotedVaccine || {}



    const [saving, setSaving] = useState(false)

    const setFile = (fileObject,) => {
        const path = (window.URL || window.webkitURL).createObjectURL(fileObject);
        promotedVaccineUpdate({prop: 'imageUrl', value: path,})
        promotedVaccineUpdate({prop: 'localImage', value: fileObject,})
    }



    const reducedVaccine = _.map(kits, function (obj) {
        const {name, _id} = obj || {}

        return {
            value: _id,
            label: name
        }
    });

    reducedVaccine.unshift({value: '', label: 'Selecione'})


    useEffect(()=>{

    },[])


    const setError = () => {

    }


    const savePromotedVaccine = () => {
        setSaving(true)



        savePromotedVaccineAction(newPromotedVaccine, callback)
    }

    const callback = () => {
        setSaving(false)
        handleClose()

    }
    return (
        <div style={{width: 300}}>

            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Destacar Vacina no App</DialogTitle>
                <DialogContent>

                    <FilePicker
                        maxSize={500000}
                        extensions={['png', 'jpeg', 'jpg', 'gif']}
                        onChange={FileObject => (setFile(FileObject))}
                        onError={errMsg => (setError(errMsg))}>
                        <Card style={{
                            display: 'flex',
                            flex: 1,
                            width: 300, height: 250, flexDirection: 'column', borderRadius: 10,
                        }}>
                            <div
                                style={{flex: 1, position: "relative", display: 'flex',}}>
                                <CardMedia
                                    style={{flex: 1, borderRadius: 10,}}
                                    image={imageUrl}
                                />
                            </div>
                        </Card>
                    </FilePicker>


                    <div style={{display: 'flex', flexDirection: 'row'}}>
                        <TextField
                            autoFocus
                            value={message}
                            margin="dense"
                            id="name"
                            label="Mensagem"
                            onChange={(e) => {

                                promotedVaccineUpdate({prop: 'message', value: e.target.value,})
                            }}
                            type="email"
                            fullWidth
                        />


                    </div>


                    <div style={{display: 'flex', flexDirection: 'row'}}>


                        <div style={{width: 200, marginRight: 10}}>
                            <MuiPickersUtilsProvider

                                utils={DateFnsUtils}>
                                <DateTimePicker
                                    value={expireIn}

                                    error={''}
                                    helperText={''}
                                    ampm={false}
                                    onChange={(value) => {
                                        promotedVaccineUpdate({prop: 'expireIn', value: value})
                                    }}
                                    label="expira Em"
                                    showTodayButton
                                />
                            </MuiPickersUtilsProvider>
                        </div>





                    </div>


                    <div style={{display: 'flex', flexDirection: 'row'}}>


                        <div style={{width: 200, marginRight: 10}}>
                            <DropDown

                                value={vaccine}
                                setValue={(value) => {
                                    promotedVaccineUpdate({prop: 'vaccine', value: value,})
                                }}
                                data={reducedVaccine}
                                label="Selecione a Vacina"
                            />
                        </div>




                    </div>





                </DialogContent>
                {!saving &&
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancelar
                    </Button>
                    <Button onClick={savePromotedVaccine} color="primary">
                        Salvar
                    </Button>
                </DialogActions>
                }


                {saving &&
                <DialogActions>
                  <div>Salvando, aguarde...</div>
                </DialogActions>
                }
            </Dialog>
        </div>
    );
}


const mapStateToProps = (state) => {


    return {
        newPromotedVaccine: state.promotedVaccine.newPromotedVaccine,
        kits: state.event.kits,



    }
}


export default connect(mapStateToProps, { promotedVaccineUpdate, savePromotedVaccineAction})(CreatePromoteVaccine)
