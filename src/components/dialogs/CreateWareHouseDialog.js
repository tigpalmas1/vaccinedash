import React, {useState, useEffect} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';

import DialogTitle from '@material-ui/core/DialogTitle';
import {MuiPickersUtilsProvider, DateTimePicker} from "material-ui-pickers";
import DateFnsUtils from "@date-io/date-fns";

import { useDispatch, useSelector} from "react-redux";
import {
    wharehouseUpdateAction,

    editWarehouseItemAction, saveWarehouseItemAction,

} from "../../redux/actions/wharehouse_action";
import DropDown from '../../common/components/DropDown'
import _ from 'lodash';

const CreateWareHouseDialog = (props) => {
    const dispatch = useDispatch();
    const newWharehouse = useSelector(state => state.wharehouse.newWharehouse);
    const establishments = useSelector(state =>       state.establishment.establishments,);
    const kits = useSelector(state => state.event.kits);
    const [reducedVaccines, setReducedVaccines] = useState([]);
    const [reducedLabs, setreducedLabs] = useState([]);

    const {open, handleClose} = props || {}




    const {price, quantity, vaccine, laboratory, batch, expirationDate, _id, manufacturer} = newWharehouse || {}
    const [saving, setSaving] = useState(false)
    const [vaccineError, setVaccineError] = useState(null);
    const [labError, setlabError] = useState(null);
    const [batchError, setbatchError] = useState(null);
    const [quantityError, setquantityError] = useState(null);
    const [priceError, setpriceError] = useState(null);
    const [validadeError, setvalidadeError] = useState(null);
    const [manufacturerError, setmanufacturerError] = useState(null);

    useEffect(()=>{
        kits.unshift({_id: -1, name: 'Selecione'})
        setReducedVaccines(_.map(kits, function (obj) {
            const {name, _id} = obj || {}
            return {
                value: _id,
                label: name
            }
        }))

        establishments.unshift({_id: -1, name: 'Selecione'})
        setreducedLabs(_.map(establishments, function (obj) {
            const {name, _id} = obj || {}
            return {
                value: _id,
                label: name
            }
        }))

    }, [])



    const checkError = () => {

        let errors = false;

        if (!vaccine || vaccine === -1) {
            setVaccineError('Selecione a Vacina')
            errors = true
        }

        if (!laboratory || laboratory === -1) {
            setlabError('Selecione o Laboratório')
            errors = true
        }
        if (!batch ) {
            setbatchError('Informe o Lote')
            errors = true
        } if (!quantity ) {
            setquantityError('Informe a quantidade')
            errors = true
        }
        if (!price ) {
            setpriceError('Informe o valor')
            errors = true
        }

        if (!expirationDate ) {
            setvalidadeError('Informe o vencimento')
            errors = true
        }

        if (!manufacturer ) {
            setmanufacturerError('Informa a empresa')
            errors = true
        }


        return errors
    }


    const saveNews = () => {
        if(!checkError()){
            setSaving(true)
            if (!_id) {
                dispatch(saveWarehouseItemAction(newWharehouse, callback,))

            } else {
                dispatch(editWarehouseItemAction(newWharehouse, callback,))
            }
        }

    }

    const callback = (message) => {
        setSaving(false)
        handleClose()

    }
    return (
        <div>

            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle
                    id="form-dialog-title">{_id ? 'Editar Item no estoque' : 'Novo Item no Estoque'}</DialogTitle>
                <DialogContent>


                    <div style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>


                        <div style={{width: 200, marginRight: 10}}>
                            <DropDown
                                error={vaccineError}
                                value={vaccine}
                                setValue={(value) => {
                                    setVaccineError(null);
                                    dispatch(wharehouseUpdateAction({prop: 'vaccine', value: value,}))
                                }}
                                data={reducedVaccines}
                                label="Selecionar a Vacina"
                            />
                        </div>

                        <div style={{width: 200, marginRight: 10}}>
                            <DropDown
                                error={labError}
                                value={laboratory}
                                setValue={(value) => {
                                    setlabError(null);
                                    dispatch(wharehouseUpdateAction({prop: 'laboratory', value: value,}))
                                }}
                                data={reducedLabs}
                                label="Selecionar o Laboratorio"
                            />
                        </div>
                    </div>

                    <TextField
                        error={manufacturerError}
                        autoFocus
                        margin="dense"
                        id="name"
                        value={manufacturer}
                        label="Empresa Produtora"
                        type="manufacturer"
                        onChange={(e) => {
                            setmanufacturerError(null);
                            dispatch(wharehouseUpdateAction({prop: 'manufacturer', value: e.target.value,}))
                        }}
                        fullWidth
                    />


                    <TextField
                        error={batchError}
                        autoFocus
                        margin="dense"
                        id="name"
                        value={batch}
                        label="Lote"
                        type="email"
                        onChange={(e) => {
                            setbatchError(null);
                            dispatch(wharehouseUpdateAction({prop: 'batch', value: e.target.value,}))
                        }}
                        fullWidth
                    />

                    <TextField
                        error={quantityError}
                        autoFocus
                        margin="dense"
                        id="name"
                        value={quantity}
                        label="Quantidade"
                        type="email"
                        onChange={(e) => {
                            setquantityError(null);
                            dispatch(wharehouseUpdateAction({prop: 'quantity', value: e.target.value,}))
                        }}
                        fullWidth
                    />

                    <TextField
                        error={priceError}
                        autoFocus
                        margin="dense"
                        id="name"
                        value={price}
                        label="Preço"
                        onChange={(e) => {
                            setpriceError(null);
                            dispatch(wharehouseUpdateAction({prop: 'price', value: e.target.value,}))
                        }}
                        type="email"
                        fullWidth
                    />

                    <div style={{width: 200, marginRight: 10}}>
                        <MuiPickersUtilsProvider

                            utils={DateFnsUtils}>
                            <DateTimePicker
                                value={expirationDate}

                                error={validadeError}
                                helperText={''}
                                ampm={false}
                                onChange={(value) => {
                                    setvalidadeError(null);
                                    dispatch(wharehouseUpdateAction({prop: 'expirationDate', value: value}))
                                }}
                                label="Validade"
                                showTodayButton
                            />
                        </MuiPickersUtilsProvider>
                    </div>
                </DialogContent>
                {!saving &&
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancelar
                    </Button>
                    <Button onClick={saveNews} color="primary">
                        {_id ? 'Editar' : 'Salvar'}
                    </Button>
                </DialogActions>
                }


                {saving &&
                <DialogActions>
                    <div>Salvando, aguarde...</div>
                </DialogActions>
                }
            </Dialog>
        </div>
    );
}





export default CreateWareHouseDialog
