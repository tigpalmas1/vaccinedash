import React from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles(theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(0),
        marginRight: theme.spacing(1),
        width: 200,
    },
    dense: {
        marginTop: -5,
    },
    menu: {
        width: 200,
    },
}));




const  DropDownState = (props) => {

    const {selectData, setStatus, data,value} = props


    const classes = useStyles();



    const handleChange = name => event => {
        selectData(event.target.value)
    };

    return (


            <TextField
                id="standard-select-currency"
                select
                label="Cidade"
                className={classes.textField}
                value={value}
                onChange={handleChange('currency')}
                SelectProps={{
                    MenuProps: {
                        className: classes.menu,
                    },
                }}


            >
                {data.map(option => (
                    <MenuItem key={option.value} value={option.value}>
                        {option.value}
                    </MenuItem>
                ))}
            </TextField>


    );
}


export default DropDownState