import React, {Component} from 'react';
import {tag, tagWrapper} from "../../common/styles/geralStyles";
import {mainOrange} from "../../common/colors/index";





const NotePicker = ({typeUpdate, type, typeError}) => {
    const {pickerContainer} = styles;

    return (

        <div style={{marginTop:0}}>

           <div label={'Tipo de Nota'} error={typeError}/>
            <div style={pickerContainer}>
                <div onClick={() => typeUpdate('information')}>
                    <div
                        style={{padding: 5, backgroundColor: mainOrange, flexWrap: 'wrap', borderRadius: 12,  marginRight: 5,    flexDirection: 'row', alignItems:'center',backgroundColor: type === 'information' ? mainOrange : 'grey'}}>
                        <div style={tag}>Mensagem</div>
                    </div>
                </div>
                <div onClick={() => typeUpdate('work')}>
                    <div
                        style={{padding: 5, backgroundColor: mainOrange, flexWrap: 'wrap', borderRadius: 12,  marginRight: 5,    flexDirection: 'row', alignItems:'center',backgroundColor: type === 'work' ? mainOrange : 'grey'}}>
                        <div style={tag}>Tarefa</div>
                    </div>

                </div>
                <div onClick={() => typeUpdate('medicine')}>
                    <div
                        style={{padding: 5, backgroundColor: mainOrange, flexWrap: 'wrap', borderRadius: 12,  marginRight: 5,    flexDirection: 'row', alignItems:'center',backgroundColor: type === 'medicine' ? mainOrange : 'grey'}}>
                        <div style={tag}>Medicação</div>
                    </div>

                </div>

            </div>
            <div style={{color: 'red', fontWeight: '700', fontSize: 8}}>{typeError}</div>
        </div>


    )
}

const styles = {
    pickerContainer: {
        width: '100%',
        display:'flex',
        flexDirection: 'row',
        alignItems: 'center'
    }
}

export {NotePicker};

