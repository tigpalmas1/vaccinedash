import React, {useState, useEffect} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import {MuiPickersUtilsProvider, DateTimePicker} from "material-ui-pickers";
import DateFnsUtils from "@date-io/date-fns";
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import {FilePicker} from 'react-file-picker'
import {connect, useDispatch, useSelector} from "react-redux";
import {deleteNewsAction, fetchNews, newsUpdateAction, saveNewsAction, updateNewsAction} from "../../redux/actions";
import Resizer from 'react-image-file-resizer';
import {getBase64File, resizeFile} from "../../common/functions/imageUtil";


const CreateNewsDialog = (props) => {
    const dispatch = useDispatch();
    const {open, handleClose} = props || {}
    const newNews = useSelector(state => state.news.newNews);

    const {description, imageUrl, releaseDate, expirantionDate, externalLink, title, _id} = newNews || {}


    const [saving, setSaving] = useState(false)
    const [message, setMessage] = useState(false)

    const setFile = async (fileObject,) => {
        const image = await resizeFile(fileObject);
        const base64 = await getBase64File(fileObject);
        const path = (window.URL || window.webkitURL).createObjectURL(image);
        dispatch(newsUpdateAction({prop: 'imageUrl', value: path,}))
        dispatch(newsUpdateAction({prop: 'image64', value: base64}))
    }


    const deleteNews = () => {
        setMessage('Apagando, aguarde')
        dispatch(deleteNewsAction(newNews._id, callback))
    }


    const setError = () => {

    }


    const saveNews = () => {
        setSaving(true)
        setMessage('Salvando aguarde');

        if (_id) {
            dispatch(updateNewsAction(_id, newNews, callback))
        } else {
            dispatch(saveNewsAction(newNews, callback))
        }

    }

    const callback = () => {
        setSaving(false)
        setMessage('')
        dispatch(fetchNews(0, 10,))
        handleClose()

    }
    return (
        <div>

            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">

                <DialogContent>

                    <FilePicker
                        maxSize={500000}
                        extensions={['png', 'jpeg', 'jpg', 'gif']}
                        onChange={FileObject => (setFile(FileObject))}
                        onError={errMsg => (setError(errMsg))}>
                        <Card style={{
                            display: 'flex',
                            flex: 1,
                            width: '100%', height: 250, flexDirection: 'column', borderRadius: 10,
                        }}>
                            <div
                                style={{flex: 1, position: "relative", display: 'flex',}}>
                                <CardMedia
                                    style={{flex: 1, borderRadius: 10,}}
                                    image={imageUrl}
                                />
                            </div>
                        </Card>
                    </FilePicker>


                    <div style={{display: 'flex', flexDirection: 'row'}}>
                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Titulo"
                            value={title}
                            onChange={(e) => {

                                dispatch(newsUpdateAction({prop: 'title', value: e.target.value,}))
                            }}
                            type="email"
                            fullWidth
                        />


                    </div>


                    <div style={{display: 'flex', flexDirection: 'row'}}>


                        <div style={{width: 200, marginRight: 10}}>
                            <MuiPickersUtilsProvider

                                utils={DateFnsUtils}>
                                <DateTimePicker
                                    value={releaseDate}

                                    error={''}
                                    helperText={''}
                                    ampm={false}
                                    onChange={(value) => {
                                        dispatch(newsUpdateAction({prop: 'releaseDate', value: value}))
                                    }}
                                    label="Inicio"
                                    showTodayButton
                                />
                            </MuiPickersUtilsProvider>
                        </div>


                        <div>
                            <MuiPickersUtilsProvider
                                style={{marginTop: 120, marginLeft: 20, marginRight: 20, width: '100%'}}
                                utils={DateFnsUtils}>
                                <DateTimePicker
                                    value={expirantionDate}

                                    error={''}
                                    helperText={''}
                                    ampm={false}
                                    onChange={(value) => {
                                        dispatch(newsUpdateAction({prop: 'expirantionDate', value: value}))
                                    }}
                                    label="Fim"
                                    showTodayButton
                                />
                            </MuiPickersUtilsProvider>
                        </div>
                    </div>


                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        value={description}
                        label="Descrição da Noticia"
                        type="email"
                        onChange={(e) => {

                            dispatch(newsUpdateAction({prop: 'description', value: e.target.value,}))
                        }}
                        fullWidth
                    />

                    <TextField
                        autoFocus
                        margin="dense"
                        id="name"
                        value={externalLink}
                        label="Link Externo"
                        onChange={(e) => {

                            dispatch(newsUpdateAction({prop: 'externalLink', value: e.target.value,}))
                        }}
                        type="email"
                        fullWidth
                    />
                </DialogContent>
                {!saving &&
                <DialogActions>
                    {_id &&
                    <Button onClick={deleteNews} color="red">
                        Apagar
                    </Button>
                    }

                    <Button onClick={handleClose} color="primary">
                        Cancelar
                    </Button>
                    <Button onClick={saveNews} color="primary">
                        {_id ? 'Editar' : 'Salvar'}
                    </Button>
                </DialogActions>
                }


                {saving &&
                <DialogActions>
                    <div>{message}</div>
                </DialogActions>
                }
            </Dialog>
        </div>
    );
}


export default CreateNewsDialog
