import React,{useEffect, useState} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {MuiPickersUtilsProvider, DatePicker, TimePicker} from "material-ui-pickers";
import DateFnsUtils from "@date-io/date-fns";
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import {FilePicker} from 'react-file-picker'
import {connect} from "react-redux";
import {fetchAllStudentsAction, addStudentAction} from "../../redux/actions";
import _ from 'lodash'
import DropDown from '../../common/components/DropDown'


const AddStudentDialog =(props) =>{

    const {open, handleClose} = props
    const { classId, allStudents} = props
    const {fetchAllStudentsAction, addStudentAction} = props
    const [selectedStudent, setSelectedeStudent] = useState()
    const [saving, setSaving] = useState(false)


    useEffect(()=>{
        fetchAllStudentsAction()
    },[])


    var reducedStudents = _.map(allStudents, function (obj) {
        const {name, _id, lastName} = obj || {}
        return {
            value: _id,
            label: `${name} ${lastName}`
        }
    });

    reducedStudents.unshift({value: '', label: 'Selecione'})





    const saveStudentClick= ()=>{
        setSaving(true)
        addStudentAction(classId, selectedStudent, callback)
    }

    const callback = (message)=>{
        setSaving(false)
        alert(message)
        handleClose()

    }
    return (
        <div>

            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Adicionar Aluno</DialogTitle>
                <DialogContent>

                    <div style={{marginTop: -10, marginLeft: -10}}>
                        <DropDown
                            error={''}
                            value={selectedStudent}
                            setValue={(value) => {
                                console.log(value)

                               setSelectedeStudent(value)
                            }}
                            data={reducedStudents}
                            label="Selecionar um Aluno"
                        />
                    </div>







                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancelar
                    </Button>
                    <Button onClick={saveStudentClick} color="primary">
                        Salvar
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}



const mapStateToProps = (state) => {


    return {
        newBonus: state.bonus.newBonus,
        activeEvent: state.event.activeEvent,
        allStudents: state.viplist.allStudents,


    }
}


export default connect(mapStateToProps, {fetchAllStudentsAction, addStudentAction})(AddStudentDialog)
