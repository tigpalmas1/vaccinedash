import React, {useEffect, useState} from 'react';
import {connect} from "react-redux";
import {fetch_cities_action, fetch_tags, searchAction} from "../redux/actions";

import AppBar from '../common/components/AppBar'
import CustomizedInputBase from "../common/components/CustomizedInputBase";
import _ from 'lodash';
import EstablishmentCard from "./screens/establishments/EstablishmentCard"
import {fetchVaccineAction, fetch_establishments, fetchUserAction, fetchNurseAction, fetchWarehouseItensAction} from "../redux/actions";
import GroupVaccinationFragment from "./screens/home/GroupVaccination";
import SaveGroupVaccination from "./screens/home/SaveGroupVaccination";

const Home = (props) => {
    const {fetch_cities_action, fetch_tags, searchAction, activeEstablishment, fetchVaccineAction, fetch_establishments, fetchUserAction, fetchNurseAction, fetchWarehouseItensAction} = props
    const {foundItens} = props
    const [regularContent, setRegularContent] = useState(true)



    useEffect(() => {
        fetch_cities_action()
        fetch_tags()
        fetchNurseAction();
        fetch_establishments();
        fetchUserAction();
        fetchVaccineAction();
        fetchNurseAction();
        fetchWarehouseItensAction();
    }, [])

    const searchTherm = (therm) => {

    }

    const onSubmitEditing = (value) => {
        /*   this.props.clearSearchAction();*/
        if (value.length > 2) {
            searchAction(value, activeEstablishment)
            setRegularContent(false)
        }
        if (value.length <= 2) {
            setRegularContent(true)
        }
    }

    const termSearch = _.debounce((term) => {
        onSubmitEditing(term)

    }, 300)


    const renderList = () => {
        return foundItens.map(item => {
            if (item.model === "establishment") {
                console.log(item)
                return (

                    <div
                        key={item._id}
                        style={{
                            marginTop: 10,
                            height: 200, width: 200,
                            overflow: 'hidden',
                            backgroundColor: 'pink',
                            marginRight: 10
                        }}>
                        <EstablishmentCard

                            establishment={item}/>

                    </div>

                )
            }

            if (item.model === "news") {
                return (

                    <div>Noticia</div>
                )
            }
            if (item.model === "bonus") {
                return (

                    <div>Bonus</div>
                )
            }
        })
    }


    return (
        <div style={{display: 'flex', flex: 1, flexDirection: 'column'}}>
            <AppBar
                search
                title={'Home'}/>


                <div>{process.env.REACT_APP_MY_API_KEY}</div>


            <div style={{with: 300, margin: 10}}>
                <CustomizedInputBase
                    handleChange={termSearch}
                />
            </div>


            {regularContent &&
            <div style={{display: 'flex', flex: 1, flexDirection: 'column',}}>
                <div style={{
                    display: 'flex',

                    width: '100%',
                    paddingRight: 20,
                    flex: 2,
                    paddingLeft: 20,
                    flexDirection: 'row',
                    overflow: 'hidden',
                }}>


                   <GroupVaccinationFragment/>
                   <div style={{width: 20}}/>
                   <SaveGroupVaccination/>




                </div>
                <div style={{display: 'flex', flex: 1, padding: 20, flexDirection: 'row',}}>





                </div>
            </div>

            }

            {!regularContent &&
            <div style={{
                display: 'flex',
                width: '100%',
                overflow: 'hidden',
                flexDirection: 'row',
                padding: 20,
                flexWrap: 'wrap'
            }}>

                {renderList()}

            </div>

            }


        </div>
    )
}


const mapStateToProps = (state) => {


    return {
        foundItens: state.administration.foundItens,
        activeEstablishment: state.establishment.activeEstablishment,


    }
}


export default connect(mapStateToProps, {fetch_cities_action, fetch_tags, searchAction, fetchVaccineAction, fetch_establishments, fetchUserAction, fetchNurseAction, fetchWarehouseItensAction})(Home)
