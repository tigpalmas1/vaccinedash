import React from 'react';
import {NavLink, Route} from 'react-router-dom'


export const PublicLayout = ({ component: Component, ...rest }) => {
    return <Route {...rest} render={(props) => (
        <React.Fragment>
            {<Component {...props} />}
        </React.Fragment>
    )} />
};