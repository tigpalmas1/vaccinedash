import React from 'react';
import {NavLink, Route, Redirect} from 'react-router-dom'
import Navigation from '../navigation/Navigation'
import {connect} from 'react-redux';


class PrivateLayout extends React.Component  {

    constructor(props) {
        super(props)

    }

    render(){

        const {component: Component, ...rest} = this.props
        const {logged} = this.props

        return <Route {...rest} render={(props) => (
            <div style={{display: 'flex', flex: 1, }}>

                <div style={{flexDirection: 'row', display: 'flex',flex: 1, }}>
                    <Navigation/>



                       {!logged ?
                           <Redirect to={{pathname: '/login', state: {from: props.location}}}/> :
                           <div style={{ display: 'flex', flex: 1, }}>

                               <Component {...props} />
                           </div>}



                </div>

            </div>
        )}/>
    }

};



const mapStateToProps = (state) => {
    return {
        logged: state.auth.logged,

    }
}

export default connect(mapStateToProps, {})(PrivateLayout);
;