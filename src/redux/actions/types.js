


//AUTH
export const AUTH_LOADING = 'auth_loading';
export const AUTH_UPDATE = 'auth_update';
export const IMAGE_USER_UPDATE = 'image_user_update';

export const LOGIN_SUCCESS = 'login_success';
export const LOGIN_ERROR = 'login_error';
export const LOGIN_LOADING = 'login_loading';
export const SIGNUP_SUCCESS = 'signup_success';
export const SIGNUP_ERROR = 'signup_error';
export const SIGNUP_LOADING = 'signup_loading';

//USER
export const FETCH_USERS = 'fetch_users';
export const FETCH_USERS_LOADING = 'fetch_users_loading';
export const CLEAR_USERS = 'clear_users';


//STUDENTS
export const SET_ACTIVE_STUDENT = 'set_active_student';


//EVENT
export const SET_ACTIVE_ITEM ='set_active_item';
export const SET_EDIT_EVENT ='set_edit_event';
export const EVENT_UPDATE ='event_update';
export const PRICE_UDPATE ='price_update';
export const ADDRESS_UPDATE ='address_update';
export const ADD_PRICE ='add_price';
export const REMOVE_PRICE ='remove_price';
export const FETCH_NO_DESCRIPTION_EVENT = 'fetch_no_description_event'



//ESTABLISHMENT
export const FETCHE_ESTABLISHMENTS = 'fetch_establishments';
export const FETCHE_EXPLORER_ESTABLISHMENTS = 'fetch_explorer_establishments';
export const CLEAR_EXPLORER_ESTABLISHMENTS = 'clear_explorer_establishments';
export const FETCHE_ESTABLISHMENTS_LOADING = 'fetch_establishments_loading';
export const CLEAR_ESTABLISHMENT_LIST = 'clear_establihsment_list';
export const CLEAR_ESTABLISHMENT_INFO = 'clear_establihsment_info';
export const SET_ACTIVE_ESTABLISHMENT ='set_active_establishment';
export const CLEAR_EXPLORER = 'clear_explorer';

export const ESTABLISHMENT_UPDATE = 'establishment_update';
export const PERSONAL_DATA_UPDATE = 'personal_data_update';

export const PHONE_UPDATE ='phone_update';
export const PHONE_ADD ='phone_add';
export const PHONE_REMOVE ='phone_remove';

export const MEDIA_UPDATE ='media_update';
export const MEDIA_ADD ='media_add';
export const MEDIA_REMOVE ='media_remove';

export const DAYS_UPDATE ='dasy_update';
export const DAYS_ADD ='dasys_add';
export const DAYS_REMOVE ='days_remove';

export const SET_EDIT_ESTABLISHMENT = 'set_edit_establishment'




//WHAREHOUSE
export const WHAREHOUSE_UPDATE_ACTION = 'wharehouse_update_action'
export const SET_ACTIVE_WHAREHOUSE = 'set_active_wharehouse'
export const SET_EDIT_WHAREHOUSE = 'set_edit_wharehouse'
export const FETCH_WHAREHOUSE = 'fetch_wharehouse'
export const FETCH_WHAREHOUSE_LOADING = 'fetch_wharehouse_loading'
export const CLEAR_WHAREHOUSE_LIST = 'clear_wharehouse_list'



//REPORT

export const FETCH_REPORTS = 'fetch_reports';
export const FETCH_ESTABLISHMENT_INFO = 'fetch_establishment_info';


//VERSION
export const FETCH_VERSION = 'fetch_version';


//VIPLIST
export const FETCH_EVENT_VIPLIST_LOADING = 'fetch_event_viplist_loading';


//SEARCH
export const LOADING_SEARCH_ITENS = 'loading_search_items';
export const FETCH_FOUND_ITENS = 'fetch_found_items';



//INSTALER
export const BANNER_IMAGE_UPDATE = 'banner_image_update';

//FILTER
export const FILTER_UPDATE = 'filter_update';
export const FILTER_UPDATE_TYPE = 'filter_update_type';

export const STATE_FILTER_ORDER_CHANGED = 'state_filter_order_changed';

export const FILTER_USER_UPDATE = 'filter_user_update';
export const STATE_FILTER_USER_CHANGED = 'state_filter_user_changed';
export const ADD_STUDENT_ACTION = 'add_student_action';



export const FETCH_FILTER = 'fetch_filter';
export const HAS_FILTER = 'has_filter';
export const FAVORIT_UPDATE = 'favorit_update'
export const STATE_CHANGED = 'state_changed'
export const FETCHE_TAGS = 'fetch_tags';
export const FETCH_CITIES = 'fetch_cities';
export const NO_FILTER = "no_filter"


//KITS
export const FETCH_KITS = 'fetch_kits';
export const FETCH_KITS_LOADING = 'fetch_kits_loading';
export const CLEAR_KITS_LIST = 'clear_kits_list';
export const SET_KIT = 'set_main_kit';
export const ADD_ITEM = 'add_item';
export const REMOVE_ITEM = 'remove_item';


//PROMOTIONAL
export const FETCH_KITS_PROMOTIONAL = 'fetch_kits_promotional';
export const FETCH_KITS_PROMOIONAL_LOADING = 'fetch_kits_promotional_loading';


//ORDERS
export const FETCH_ORDERS = 'fetch_orders';
export const CLEAR_ORDER_LIST = 'clear_order_list';

//USER INFO



//NEWS
export const FETCH_NEWS = 'fetch_news';
export const FETCH_EXPLORER_NEWS = 'fetch_explorer_news';
export const FETCH_NEWS_ESTABLISHMENT = 'fetch_news_establishmetn';
export const LOADING_ESTABLISHMENT_NEWS = "loading_establishment_news";
export const CLEAR_NEWS_ESTABLISHMENT_LIST = "clear_bonus_establishment_list";
export const FETCH_NEWS_LOADING = 'fetch_news_loading';
export const CLEAR_NEWS_LIST = 'clear_news_list';
export const SET_ACTIVE_NEWS ='set_active_news';
export const NEWS_UPDATE_ACTION ='news_update_action';
export const RELOAD_NEWS ='reload_news';

//COVDI
export const FETCH_COVID = 'fetch_covid';
export const SCHEDULE_UPDATE ='schedule_update';
export const FILTER_COVID_UPDATE ='filter_covid_update';
export const CLEAR_SCHEDULE_FORM ='clear_schedule_form';

//BONUS
export const FETCH_BONUS_LOADING = 'fetch_bonus_loading';
export const FETCH_BONUS = 'fetch_bonus';

//PROMOTED_VACCINE
export const PROMOTED_VACCINE_UPDATE = 'promoted_vaccine_update';
export const FETCH_PROMOTED_VACCINE = 'fetch_promoted_vaccine';



//NURSE
export const FETCH_NURSE_LOADING = 'fetch_nurse_loading';
export const FETCH_NURSE = 'fetch_nurse';
export const SET_NURSE = 'set_nurse';


//CART
export const FETCH_CARDS_LOADING = 'fetch_cards_loading';
export const FETCH_CARDS = 'fetch_cards';
export const SET_ACTIVE_CARD = 'set_active_card';
export const SET_ACTIVE_STAMP = 'set_active_stamp';
export const FETCH_USER_TABLE = 'fetch_user_table';
export const SET_CARD_STATUS = 'set_card_status';
export const APPLY_CARD_UPDATE ='apply_card_update';
export const CLEAR_DATA_STAMP ='clear_data_stamp';



//LOGOUT
export  const LOGOUT ='logout'

