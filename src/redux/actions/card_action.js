import axios from 'axios';
import {
    APPLY_CARD_UPDATE, CLEAR_DATA_STAMP,

    FETCH_CARDS,
    FETCH_CARDS_LOADING,
    FETCH_USER_TABLE,
    SCHEDULE_UPDATE,
    SET_ACTIVE_CARD,
    SET_ACTIVE_STAMP,
    SET_CARD_STATUS,

} from "./types";
import {checkResponseError} from "../../common/functions/index";
const {REACT_APP_BASE_URL} = process.env

export const setActiveCardAction = (item) => async (dispatch) => {

    dispatch({
        type: SET_ACTIVE_CARD,
        payload: item,
    })


}

export const setActiveStamp = (item) => async (dispatch) => {

    dispatch({
        type: SET_ACTIVE_STAMP,
        payload: item,
    })


}


export const applyCardUpdate = ({prop, value}) => async (dispatch) => {
    dispatch({
        type: APPLY_CARD_UPDATE,
        payload: {prop, value}
    })
};

export const clearDataStampAction = () => async (dispatch) => {
    dispatch({
        type: CLEAR_DATA_STAMP,
    })
};

export const fetchCardsAction = (activePage, option) => async (dispatch) => {

    dispatch({
        type: FETCH_CARDS_LOADING,
        payload: true,
    })

    try {
        var url = `${REACT_APP_BASE_URL}/table-image?`;

        let config = {
            params: {
                skip: (activePage -1) * 10,
                limit: 10,
                status: option === 'Todos'? undefined : option,
               },
        }
        const {data} = await axios.get(url, config)
        dispatch({
            type: FETCH_CARDS,
            payload: {
                cards: data,
                endList: data.length === 0,
                message: data.length > 0 ? '' : 'Sem ítems no estoque no momento',
            }
        })
    } catch (e) {
        console.log(checkResponseError(e));
        dispatch({
            type: FETCH_CARDS_LOADING,
            payload: false,
        })
    }
}



export const fetchUserVacineTable = (userId) => async (dispatch) => {


    try {
        var url = `${REACT_APP_BASE_URL}/user-vaccine-table?`;

        let config = {
            params: {
                userId
            },
        }
        const {data} = await axios.get(url, config)


        dispatch({
            type: FETCH_USER_TABLE,
            payload: {
                userTable: data,
            }
        })
    } catch (e) {
        console.log(checkResponseError(e));

    }
}

export const changeCardStatusAction = (_id, receivedStatus) => async (dispatch) => {


    try {
        var url = `${REACT_APP_BASE_URL}/table-image/${_id}`;

        const {data} = await axios.put(url, {status: receivedStatus})
        const {status} = data ||{}
        dispatch({
            type: SET_CARD_STATUS,
            payload: status
        })


    } catch (e) {
        console.log(e.message);
        console.log(checkResponseError(e));

    }
}

export const applyDashVaccine = (item, callback) => async (dispatch) => {

    const {userTable, vaccine, activeStamp, lote,
        dashLab, dashNurse, notes,dashStamp,
        localImage,date, image64}= item ||{}
    const {_id: tableId}= userTable ||{}
    const {_id: vacineId}= vaccine ||{}
    const {_id: stampId, dose}= activeStamp ||{}
    const {_id: doseId, type}= dose ||{}

    console.log(item);

    try {
        const object = {
            image64,
            dose: doseId,
            vaccine: vacineId,
            stamp: [{
                date: date,
                _id: stampId,
                batch: lote,
                type: type,
                dashStamp,
                dashNurse,
                dashLab,
                notes,
            }]
        }

        /*const form = new FormData();*/
      /*  if (localImage) {
            const file =await DataURIToBlob(localImage)
            form.append('file', file);
        }
        form.append('object', JSON.stringify(object));*/
        var url = `${REACT_APP_BASE_URL}/user-vaccine-table/${tableId}`;
        const {data} = await axios.put(url, object)

        callback(true, 'Salvo com sucess');



    } catch (e) {
        callback(false, 'algo deu errado');
        console.log(checkResponseError(e));

    }
}

function DataURIToBlob(dataURI) {
    const splitDataURI = dataURI.split(',')
    const byteString = splitDataURI[0].indexOf('base64') >= 0 ? atob(splitDataURI[1]) : decodeURI(splitDataURI[1])
    const mimeString = splitDataURI[0].split(':')[1].split(';')[0]

    const ia = new Uint8Array(byteString.length)
    for (let i = 0; i < byteString.length; i++)
        ia[i] = byteString.charCodeAt(i)

    return new Blob([ia], { type: mimeString })
}