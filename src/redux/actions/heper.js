import axios from "axios";
import {checkResponseError} from "../../common/functions";
const {REACT_APP_BASE_URL} = process.env



export const getData = async (dispatch, route, config, type,) => {
    try {
        var url = `${REACT_APP_BASE_URL}/${route}?`;
        const {data} = await axios.get(url, config)



        dispatch({
            type,
            payload: {
                data,
                endList: data.length === 0 ? true : false,
                message: data.length > 0 ? '' : 'Sem dados encontrados',

            }
        })
    } catch (e) {
        console.log(e.message);
        console.log(e.response);
    }
}

export const postData = async (route, body, callback) => {
    try {
        var url = `${REACT_APP_BASE_URL}/${route}?`;
        await axios.post(url, body)
        callback(true, 'Salvo com sucesso');

    } catch (e) {

        callback(false, checkResponseError(e));
    }
}

export const putData = async (route, id, body, callback) => {
    try {
        var url = `${REACT_APP_BASE_URL}/${route}/${id}?`;
        await axios.put(url, body)
        callback(true, 'Atualizado com sucesso');

    } catch (e) {
        callback(false, 'Algo deu errado');
    }
}

export const deleteData = async (route, id,  callback) => {
    try {
        var url = `${REACT_APP_BASE_URL}/${route}/${id}?`;
        await axios.delete(url)
        callback(true, 'Apagado com sucesso');

    } catch (e) {
        callback(false, 'Algo deu errado ao apagar');
    }
}