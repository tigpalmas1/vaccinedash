import axios from 'axios';

import { noNews} from "../../common/constants/index";
import {checkResponseError} from "../../common/functions/index";
import {
    CLEAR_WHAREHOUSE_LIST,
    FETCH_WHAREHOUSE,
    FETCH_WHAREHOUSE_LOADING,
    SET_ACTIVE_WHAREHOUSE,
    SET_EDIT_WHAREHOUSE,
    WHAREHOUSE_UPDATE_ACTION
} from "./types";

const {REACT_APP_BASE_URL} = process.env

export const wharehouseUpdateAction = ({prop, value}) => async (dispatch) => {

    dispatch({
        type: WHAREHOUSE_UPDATE_ACTION,
        payload: {prop, value}
    })
};



export const setActiveItemWharehouse = (wharehouse) => async (dispatch) => {
    console.log(wharehouse)

    dispatch({
        type: SET_ACTIVE_WHAREHOUSE,
        payload: wharehouse
    })
};


export const setEditWharehouseAction = (newWharehouse) => async (dispatch) => {

    dispatch({
        type: SET_EDIT_WHAREHOUSE,
        payload: newWharehouse
    })
};


export const saveWarehouseItemAction = (warehouseItem, saveCallback, activeEstablishment) => async (dispatch) => {
        console.log("saveee")
        console.log(warehouseItem)

    try {
        const url = `${REACT_APP_BASE_URL}/lab-warehouse?`;

        const {data} = await axios.post(url, warehouseItem)
        console.log(data);

        dispatch({
            type: SET_EDIT_WHAREHOUSE,
            payload: null
        })

        saveCallback('Item salvo com sucesso')
        await reloadWhareHouseItens(activeEstablishment, dispatch, saveCallback)


    } catch (e) {
        console.log(e.response)
        saveCallback(checkResponseError(e))
    }

};

export const deleteWarehouseItemAction = (_id, saveCallback, activeEstablishment) => async (dispatch) => {


    try {
        const  url = `${REACT_APP_BASE_URL}/lab-warehouse/${_id}`;

       await axios.delete(url)



        saveCallback('Item Apagado com sucesso')
        await reloadWhareHouseItens(activeEstablishment, dispatch, saveCallback)




    } catch (e) {
        console.log(e.response)
        saveCallback(checkResponseError(e))
    }

};

export const editWarehouseItemAction = (item, saveCallback, activeEstablishment) => async (dispatch) => {
    console.log("Ediiite")
    console.log(item)


    try {
        const url = `${REACT_APP_BASE_URL}/lab-warehouse/${item._id}`;

         await axios.put(url, item)


        await reloadWhareHouseItens(activeEstablishment, dispatch, saveCallback)
        saveCallback('Item Editado com sucesso')


    } catch (e) {
        console.log(e.response)
        saveCallback(checkResponseError(e))
    }

};


export const fetchWarehouseItensAction = (activeEstablishment, activePage) => async (dispatch) => {
    const {_id} = activeEstablishment || {}


    dispatch({
        type: FETCH_WHAREHOUSE_LOADING,
        payload: true,
    })

    try {
        const url = `${REACT_APP_BASE_URL}/lab-warehouse?`;

        let config = {
            params: {
                skip: (activePage-1) *10,
                limit: 10,
                laboratoryId: _id

            },
        }


        const {data} = await axios.get(url, config)



        dispatch({
            type: FETCH_WHAREHOUSE,
            payload: {
                news: data,
                endList: data.length === 0,
                message: data.length > 0 ? '' : 'Sem ítems no estoque no momento',
            }
        })
    } catch (e) {
        console.log(checkResponseError(e));
        dispatch({
            type: FETCH_WHAREHOUSE_LOADING,
            payload: false,
        })
    }
}


const reloadWhareHouseItens = async (activeEstablishment, dispatch,) => {
    console.log(activeEstablishment);


    dispatch({
        type: FETCH_WHAREHOUSE_LOADING,
        payload: true,
    })

    try {
        var url = `${REACT_APP_BASE_URL}/lab-warehouse?`;

        let config = {
            params: {
                skip: 0,
                limit: 50,
                laboratoryId: activeEstablishment._id

            },
        }


        const {data} = await axios.get(url, config)



        dispatch({
            type: FETCH_WHAREHOUSE,
            payload: {
                news: data,
                endList: data.length === 0,
                message: data.length > 0 ? '' : noNews,
            }
        })
    } catch (e) {
        console.log(checkResponseError(e));
        dispatch({
            type: FETCH_WHAREHOUSE_LOADING,
            payload: false,
        })
    }

}


export const clearWharehouseList = () => async (dispatch) => {


    dispatch({
        type: CLEAR_WHAREHOUSE_LIST,

    })

};