import {
    AUTH_UPDATE,  LOGIN_LOADING, LOGIN_ERROR, LOGIN_SUCCESS,
    LOGOUT,

} from "./types";
import axios from 'axios';
import {checkResponseError, getStorage, setStorage} from "../../common/functions/index";
const {REACT_APP_BASE_URL} = process.env

export const authUpdate = ({prop, value},) => async (dispatch) => {
    dispatch({
        type: AUTH_UPDATE,
        payload: {prop, value}
    })
};

export const checkAuthentication = () => async (dispatch) => {



    try {

        const token = await  getStorage('token');
        const data = await  getStorage('data');



       if(token && data ){

           axios.defaults.headers.common['AccessToken'] = token;
           dispatch({
               type: LOGIN_SUCCESS,
               payload: data,
           })


       }else{


           dispatch({
               type: LOGIN_ERROR,
               payload: ''
           })
       }



    } catch (e) {
        console.log(e.message)

    }
};



export const loginServerAction = (auth) => async (dispatch) => {

    dispatch({type: LOGIN_LOADING})
    try {

        console.log(REACT_APP_BASE_URL)
        const {data, headers} = await axios.post(`${REACT_APP_BASE_URL}/login`, auth);

        const{ token, permission} = data ||{}
        const{ isAdmin, establishmentId, isTeacher , isPrincipal} = permission ||{}

        console.log(data)



        if(isAdmin || establishmentId|| isTeacher ||isPrincipal){
            axios.defaults.headers.common['AccessToken'] = token;
            await  setStorage('data', data);
            await setStorage('token', token);


            dispatch({
                type: LOGIN_SUCCESS,
                payload: data,
            })
        }else{
            dispatch({
                type: LOGIN_ERROR,
                payload: "Você não tem permissão de acesso a esse sistema, solicite permissão para um Administrador"
            })
        }





    } catch (e) {
        console.log(e.message)
        dispatch({
            type: LOGIN_ERROR,
            payload: checkResponseError(e)
        })
    }
};


export const logoutAction = () => async (dispatch) => {


    try {
        await setStorage("data", null);
        await setStorage("token", null);

        dispatch({
            type: LOGOUT,
        })

    } catch (e) {
        console.log(e.message);
    }
};







