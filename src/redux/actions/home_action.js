
import {deleteData, getData, postData, putData} from "./heper";
import {
    CLEAR_SCHEDULE_FORM,
    FETCH_COVID,
    FILTER_COVID_UPDATE,
    SCHEDULE_UPDATE
} from "./types";



export const filterUpdate = ({prop, value}) => async (dispatch) => {
    dispatch({
        type: FILTER_COVID_UPDATE,
        payload: {prop, value}
    })
};


export const scheduleUpdate = ({prop, value}) => async (dispatch) => {
    dispatch({
        type: SCHEDULE_UPDATE,
        payload: {prop, value}
    })
};


export const fetchCovidVaccination = (value, activePage) => async (dispatch) => {
    const config = {
        params: {
            city: value,
            limit: 10,
            skip: (activePage -1 ) * 10,
        }
    }
    getData(dispatch, 'covid',config, FETCH_COVID);
}

export const postScheduleCovid = (body, callback) => async () => {
    postData( 'covid',body, callback);
}

export const udpateScheduleCovid = (id, body, callback) => async () => {
    putData( 'covid',id,body, callback);
}

export const deleteScheduleAction = (id,  callback) => async () => {
    deleteData( 'covid',id, callback);
}

export const clearScheduleForm = () => async (dispatch) => {
    dispatch({
        type: CLEAR_SCHEDULE_FORM
    })
}