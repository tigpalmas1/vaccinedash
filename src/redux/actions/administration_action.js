import axios from 'axios';


import {
    FETCH_FOUND_ITENS, FETCH_VERSION,
    LOADING_SEARCH_ITENS,
} from "./types";
import {checkResponseError, } from "../../common/functions/index";
const {REACT_APP_BASE_URL} = process.env


export const createTagAction = (item, callback) => async () => {

    try {
        await axios.post(`${REACT_APP_BASE_URL}/tag`, item);
        reloadTags(callback)


    } catch (e) {
        console.log(e.message);
    }
}

export const deleteTagAction = (item, callback) => async () => {

    console.log(item)
    try {
        await axios.delete(`${REACT_APP_BASE_URL}/tag/${item._id}`);
        reloadTags(callback)


    } catch (e) {
        console.log(e.message);
    }
}


const reloadTags = async (callback) => {


    try {

        let config = {
            params: {
                skip: 0,
                limit: 50,
            },
        }

        const {data} = await axios.get(`${REACT_APP_BASE_URL}/tag`, config);
        callback(data)


    } catch (e) {
        console.log(e.message);
    }
}


export const fetch_version = () => async (dispatch) => {

    try {
        const {data} = await axios.get(`${REACT_APP_BASE_URL}/version`);

        dispatch({
            type: FETCH_VERSION,
            payload: data
        });


    } catch (e) {
        console.log(e.message);
    }
}

export const sendPushAction = (body, callback ) => async () => {

    try {
        const {data} = await axios.post(`${REACT_APP_BASE_URL}/push/users`, body);

       const{resMessage} = data||{}
        callback(resMessage)


    } catch (e) {
        console.log(e.message);
        console.log(e.response);
        callback(checkResponseError(e))

    }
}

export const searchAction = (text, establishment) => async (dispatch) => {
    dispatch({type: LOADING_SEARCH_ITENS, payload: true})


    let url = `${REACT_APP_BASE_URL}/browse?`;
    let config = '';

    try {

        config = {
            params: {
                search: text,
                establishmentId: establishment? establishment._id : null
            },
        };


        const {data} = await axios.get(url, config);
        const {length} = data

        console.log(data)


        dispatch({
            type: FETCH_FOUND_ITENS,
            payload: {
                foundItens: data,
                term: text,
                message: `${length}  ${length > 1 ? 'itens encontrados' : ' item encontrado'} para ${text}...`
            }
        })

    } catch (e) {
        dispatch({type: LOADING_SEARCH_ITENS, payload: false})
        console.log(e.response);

    }
}