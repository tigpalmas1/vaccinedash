import {

    FETCH_PROMOTED_VACCINE,  PROMOTED_VACCINE_UPDATE,


} from "./types";
import axios from 'axios'
import {checkResponseError} from "../../common/functions/index";
const {REACT_APP_BASE_URL} = process.env

export const promotedVaccineUpdate = ({prop, value}) => async (dispatch) => {

    dispatch({
        type: PROMOTED_VACCINE_UPDATE,
        payload: {prop, value}
    })
};

export const fetchPromotedVaccinesAction = () => async (dispatch) => {


    try {
        let url = `${REACT_APP_BASE_URL}/promote`;
        let config = {
            params: {
                skip: 0,
                limit: 10
            },

        };

        const {data} = await axios.get(url, config);

        dispatch({
            type: FETCH_PROMOTED_VACCINE,
            payload: {
                promotedVaccines: data,
            }
        });

    } catch (e) {
        console.log(checkResponseError(e));

    }


};


export const savePromotedVaccineAction = (promotedVaccine,  saveCallback) => async (dispatch) => {
    console.log(promotedVaccine)


    try {
        var url = `${REACT_APP_BASE_URL}/promote?`;

        const form = new FormData();
        form.append('file', promotedVaccine.localImage);
        form.append('object', JSON.stringify(promotedVaccine));

        const {data} = await axios.post(url, form)

        console.log(data);



    } catch (e) {
        console.log(e.message)
        saveCallback(checkResponseError(e))
    }
};


