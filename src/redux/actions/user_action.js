import {

 CLEAR_USERS,
    FETCH_USERS,  FETCH_USERS_LOADING,
     STATE_FILTER_USER_CHANGED,

} from "./types";
import axios from 'axios'

import {checkResponseError} from "../../common/functions/index";
const {REACT_APP_BASE_URL} = process.env



export const setUserAction = (item) => async (dispatch) => {
    dispatch({type: 'SET_USER', payload: item})

};

export const userUpdateAction = ({prop, value}) => async (dispatch) => {
    dispatch({
        type: 'USER_UPDATE',
        payload: {prop, value}
    })
};


export const fetchUserAction = ( activePage) => async (dispatch) => {





    try {


        dispatch({type: FETCH_USERS_LOADING, payload: {loading: true, message: 'Buscando Usuários'}});
        const url = `${REACT_APP_BASE_URL}/user?`;


        let config = {
            params: {
                skip: (activePage - 1) * 10,
                limit:10,

            },
        };




        const {data} = await axios.get(url, config)

        dispatch({
            type: FETCH_USERS,
            payload: {
                users: data,
                message: data.length > 0 ? '' : 'Nem um usuário encontrado',
                endList: data.length < 10

            }
        });

        dispatch({
            type: STATE_FILTER_USER_CHANGED,
            payload: false
        })

    } catch (e) {
        console.log(e);
        dispatch({type: FETCH_USERS_LOADING, payload: {loading: false, message: checkResponseError(e)}})

    }
};


export const changeUserStatusAction = (userId, status, callback) => async () => {

    try {
        const url = `${REACT_APP_BASE_URL}/user?`;
        await axios.post(url, {userId: userId, status: status})

        callback('Status do usuário alterado com sucesso')

    } catch (e) {
        console.log(e);
        callback('Algo deu errado' + e.message)

    }
};


export const setPermissionAction = (permissionId, permission, callback) => async () => {



    try {
        const url = `${REACT_APP_BASE_URL}/permission/${permissionId}?`;
         await axios.put(url, permission)

        callback('Perrmissões atualizadas com sucesso')

    } catch (e) {
        console.log(e.response);
        callback('Algo deu errado' )

    }
};

export const clearUserList = () => async (dispatch) => {

    dispatch({type: CLEAR_USERS})


};

