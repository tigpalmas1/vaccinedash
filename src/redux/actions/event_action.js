import {

    CLEAR_KITS_LIST,
     EVENT_UPDATE, FETCH_KITS, FETCH_KITS_LOADING,
     SET_ACTIVE_ITEM,
     SET_EDIT_EVENT,


} from "./types";
import axios from 'axios'
import {checkResponseError} from "../../common/functions/index";
const {REACT_APP_BASE_URL} = process.env


export const doseUpdateAction = ({prop, value}, index) => async (dispatch) => {
    dispatch({
        type: 'DOSE_UPDATE',
        payload: {
            prop,
            value,
            index

        }
    })
};

export const addDoseAction = () => async (dispatch) => {

    dispatch({
        type: 'DOSE_ADD',
        payload: {
           type: ''
        }
    })
};


export const removeDoseAction = (index,) => async (dispatch) => {


    dispatch({
        type: 'DOSE_REMOVE',
        payload: index
    })
};

export const setEditEventAction = (event) => async (dispatch) => {
    dispatch({
        type: SET_EDIT_EVENT,
        payload: event
    })
};


export const eventUpdateAction = ({prop, value},) => async (dispatch) => {

    dispatch({
        type: EVENT_UPDATE,
        payload: {prop, value}
    })
};


export const setActiveItemAction = (event) => async (dispatch) => {

    dispatch({
        type: SET_ACTIVE_ITEM,
        payload:event
    })
};


export const saveVaccineAction = (vaccine, saveCallback) => async () => {





    try {
        var url = `${REACT_APP_BASE_URL}/vaccine?`;

        await axios.post(url, vaccine)

        saveCallback('Vacina Salva com sucesso')


    } catch (e) {
        console.log(e.response)
        saveCallback(checkResponseError(e))
    }
};


export const editEventAction = ( event, saveCallback) => async () => {
    const{localImage, _id} = event



    try {
        var url = `${REACT_APP_BASE_URL}/event/${_id}`;

        const form = new FormData();

        if(localImage){
            form.append('file', localImage);
        }
        form.append('object', JSON.stringify(event));

        const {data} = await axios.put(url, form)
        const {isEnabled} = data ||{}

        saveCallback('Evento Editado com sucesso', isEnabled)


    } catch (e) {
        console.log(e.response)
        saveCallback(checkResponseError(e))
    }
};




export const clearKItsList = () => async (dispatch) => {
    console.log('chamando aqui')
    dispatch({type: CLEAR_KITS_LIST})
};

export const fetchVaccineAction = () => async (dispatch) => {



    dispatch({type: FETCH_KITS_LOADING, payload: {loading: true, message: 'Buscando Vacinas'}})
    try {
        var url = `${REACT_APP_BASE_URL}/vaccine?`;



        let config = {
            params: {
                skip: 0,
                limit: 50,
            },
        }
        const {data} = await axios.get(url, config)



        dispatch({
            type: FETCH_KITS,
            payload: {
                kits: data,
                message: data.length > 0 ? '' : 'Sem Vacinas cadastradas',
                endList: data.length < 5,
            }
        });

    } catch (e) {
        console.log( e.message)
        console.log( e.response)
        //  dispatch({type: FETCH_KITS_LOADING, payload: {loading: false, message: 'ops, algo deu erraodo ' + checkResponseError(e.response)}})

    }
};



export const editVaccineAction = ( vaccine, saveCallback) => async () => {



    try {
        var url = `${REACT_APP_BASE_URL}/vaccine/${vaccine._id}`;

        await axios.put(url, vaccine)

        saveCallback('Vacina Editada Salva com sucesso')


    } catch (e) {
        console.log(e.response)
        saveCallback(checkResponseError(e))
    }
};


export const deleteVaccineAction = ( vaccine, saveCallback) => async () => {



    try {
        var url = `${REACT_APP_BASE_URL}/vaccine/${vaccine._id}`;

        await axios.delete(url)

        saveCallback('Vacina Apagada com sucesso')


    } catch (e) {
        console.log(e.response)
        saveCallback(checkResponseError(e))
    }
};






