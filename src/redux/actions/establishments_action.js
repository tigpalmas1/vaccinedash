import axios from 'axios';
import {
    CLEAR_ESTABLISHMENT_LIST, ESTABLISHMENT_UPDATE,
    FETCHE_ESTABLISHMENTS, FETCHE_ESTABLISHMENTS_LOADING, ADDRESS_UPDATE,
    SET_ACTIVE_ESTABLISHMENT, SET_EDIT_ESTABLISHMENT, PHONE_UPDATE, PHONE_ADD, PHONE_REMOVE
} from "./types";
import {checkResponseError} from "../../common/functions"

const {REACT_APP_BASE_URL} = process.env



export const setActiveEstablishmentAction = (establishment) => async (dispatch) => {

    axios.defaults.headers.common['laboratoryId'] = establishment ? establishment._id : undefined;
    dispatch({
        type: SET_ACTIVE_ESTABLISHMENT,
        payload: establishment
    })
};

export const setEditEstablishmentAction = (establishment) => async (dispatch) => {
    dispatch({
        type: SET_EDIT_ESTABLISHMENT,
        payload: establishment
    })
};

export const establishmentUpdateAction = ({prop, value},) => async (dispatch) => {
    dispatch({
        type: ESTABLISHMENT_UPDATE,
        payload: {prop, value}
    })
};

export const establishmentUpdateImage = (imageUrl) => async (dispatch) => {
    dispatch({
        type: 'UPDATE_IMAGE',
        payload: imageUrl
    })
};




export const addressUpdateAction = ({prop, value}) => async (dispatch) => {
    dispatch({
        type: ADDRESS_UPDATE,
        payload: {
            prop,
            value,
            index:0
        }
    })
};

export const fetch_establishments = (skip, limit) => async (dispatch) => {
    dispatch({
        type: FETCHE_ESTABLISHMENTS_LOADING,
        payload: {
            loading: true,
            message: "Buscando Estabelecimentos...",
        },

    })

    var url = `${REACT_APP_BASE_URL}/laboratory?`;
    let config = '';
    try {
        config = {
            params: {
                skip: skip,
                limit: 50,
            },
        }
        const {data} = await axios.get(url, config)



        dispatch({
            type: FETCHE_ESTABLISHMENTS,
            payload: {
                loading: false,
                establishments: data,
                message: data.length > 0 ? '' : 'Sem laboratórios',
                endList: data.length < 15 ? true : false,
            }
        })
    } catch (e) {

        dispatch({
            type: FETCHE_ESTABLISHMENTS_LOADING,
            payload: {
                loading: false,
                message: e.message,

            },

        })
    }
}




export const saveEstablishmentAction = (establishment, activeEstablishment, saveCallback) => async (dispatch) => {

        console.log(establishment);

    try {


        var url = `${REACT_APP_BASE_URL}/laboratory?`;
        var result = await axios.post(url, establishment)


        const {data} = result || {}
        console.log(data);


        saveCallback(true, 'Unidade Criada com  sucesso')


    } catch (e) {
        console.log(e.message)
        saveCallback(false, checkResponseError(e))
    }
};


export const editEstablishmentAction = (establishment, saveCallback) => async (dispatch) => {
    const {localBanner, _id} = establishment || {}

    try {

        var url = `${REACT_APP_BASE_URL}/laboratory/${_id}`;
        const {data} = await axios.put(url, establishment)
        saveCallback(true, 'Estabelecimento Atualizado sucesso')


    } catch (e) {
        console.log(e.message)
        saveCallback(false, checkResponseError(e))
    }
};


export const clearEstablishmentList = () => async (dispatch) => {


    dispatch({
        type: CLEAR_ESTABLISHMENT_LIST,

    })

};



export const phoneUpdateAction = ({prop, value}, index) => async (dispatch) => {
    dispatch({
        type: PHONE_UPDATE,
        payload: {
            prop,
            value,
            index

        }
    })
};

export const addPhoneAction = (value,) => async (dispatch) => {
    console.log(value)
    dispatch({
        type: PHONE_ADD,
        payload: {
            value,
        }
    })
};


export const removePhoneAction = (index,) => async (dispatch) => {


    dispatch({
        type: PHONE_REMOVE,
        payload: index
    })
};


export const searchCepAction = (cep, callback) => async (dispatch) => {


    const url = `https://viacep.com.br/ws/${cep}/json`
    try {
        fetch(url)
            .then(response => response.json())
            .then(data => {
                console.log(data);
               callback(data)
            });
       // callback(data, erro)


    } catch (e) {
        console.log('erro')
        console.log(e.message)

        checkResponseError(e)
    }
}

