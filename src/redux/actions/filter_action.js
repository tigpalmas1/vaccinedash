import axios from 'axios';


import {
    ADD_STUDENT_ACTION, CLEAR_EVENTS_LIST, FAVORIT_UPDATE, FETCH_FILTER, SET_ACTIVE_STUDENT,
    STATE_CHANGED
} from "./types";
import {setStorage} from "../../common/functions/index";
const {REACT_APP_BASE_URL} = process.env






export const filterUpdateAction = ({prop, value}) => async (dispatch) => {

    dispatch({
        type: FAVORIT_UPDATE,
        payload: {prop, value}
    })
};


export const stateChangedAction = (status) => async (dispatch) => {
    dispatch({
        type: STATE_CHANGED,
        payload: {
            stateChanged: status
        }
    })
};


export const setFilterAction = (filter) => async (dispatch) => {




    try {
        await  setStorage("filter", filter)
        dispatch({
            type: FETCH_FILTER,
            payload: {
                filter: filter,
            }
        })


    } catch (e) {
        console.log(e)
    }
};







export const fetch_cities_action = (fetch_cities) => async (dispatch) => {


    /*try {
        const {data} = await axios.get(`${REACT_APP_BASE_URL}/city`);
        dispatch({
            type: FAVORIT_UPDATE,
            payload: {prop: "cities", value: data}
        })

    } catch (e) {
        console.log(e.message);
    }*/
}


export const fetch_tags = (fetch_tags) => async (dispatch) => {


   /* try {

        let config = {
            params: {
                skip: 0,
                limit: 50,
            },
        }

        const {data} = await axios.get(`${REACT_APP_BASE_URL}/tag`, config);

        dispatch({
            type: FAVORIT_UPDATE,
            payload: {prop: "tags", value: data}
        })


    } catch (e) {
        console.log(e.message);
    }*/
}


