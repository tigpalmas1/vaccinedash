import {
   DAYS_ADD, DAYS_REMOVE,

     DAYS_UPDATE, FETCH_NURSE_LOADING, FETCH_NURSE,
    SET_NURSE,

} from "./types";
import axios from 'axios'


import {checkResponseError} from "../../common/functions/index";
const {REACT_APP_BASE_URL} = process.env


export const setNurseAction = (item) => async (dispatch) => {
    dispatch({type: SET_NURSE, payload: item})

};

export const nurseUpdateAction = ({prop, value}) => async (dispatch) => {


    dispatch({
        type: 'NURSE_UPDATE',
        payload: {prop, value}
    })
};


export const daysRemoveAction = (index,) => async (dispatch) => {


    dispatch({
        type: DAYS_REMOVE,
        payload: index
    })
};


export const daysUpdateAction = ({prop, value}, index) => async (dispatch) => {
    dispatch({
        type: DAYS_UPDATE,
        payload: {
            prop,
            value,
            index

        }
    })
};

export const daysAddAction = (value,) => async (dispatch) => {
    console.log(value)
    dispatch({
        type: DAYS_ADD,
        payload: {
            value,
        }
    })
};


export const saveNurseAction = (nurse, saveCallback) => async (dispatch) => {


    try {
        var url = `${REACT_APP_BASE_URL}/nurse?`;

        const {data} = await axios.post(url, nurse)

        saveCallback('Aplicador Salvo com sucesso')


    } catch (e) {
        console.log(e.response)
        saveCallback(checkResponseError(e))
    }
};

export const updateNurseAction = (nurse, saveCallback) => async (dispatch) => {
    const {_id} = nurse ||{}

    try {
        var url = `${REACT_APP_BASE_URL}/nurse/${_id}?`;

        const {data} = await axios.put(url, nurse)

        saveCallback('Aplicador Atualizado com sucesso')


    } catch (e) {
        console.log(e.response)
        saveCallback(checkResponseError(e))
    }
};

export const deleteNurseAction = (_id, saveCallback) => async (dispatch) => {

    try {
        var url = `${REACT_APP_BASE_URL}/nurse/${_id}?`;

        const {data} = await axios.delete(url)

        saveCallback('Aplicador Excluído com sucesso')


    } catch (e) {
        console.log(e.response)
        saveCallback(checkResponseError(e))
    }
};

export const fetchNurseAction = () => async (dispatch) => {
    dispatch({
        type: FETCH_NURSE_LOADING,
        payload: {
            loading: true,
            message: "Buscando Aplicadores...",
        },

    })


    try {
        var url = `${REACT_APP_BASE_URL}/nurse?`;


        let config = {
            params: {
                skip: 0,
                limit: 100,
            },
        }
        const {data} = await axios.get(url, config)

        dispatch({
            type: FETCH_NURSE,
            payload: {
                loading: false,
                nurses: data,
                message: data.length > 0 ? '' : 'Ainda não foi cadastrado',
            }
        })




    } catch (e) {
        dispatch({
            type: FETCH_NURSE_LOADING,
            payload: {
                loading: false,
                message: checkResponseError(e),
            },

        })
    }
};

