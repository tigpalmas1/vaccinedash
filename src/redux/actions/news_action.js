import axios from 'axios';
import {
    CLEAR_NEWS_ESTABLISHMENT_LIST,
    CLEAR_NEWS_LIST, FETCH_EXPLORER_NEWS, FETCH_NEWS, FETCH_NEWS_ESTABLISHMENT,
    LOADING_ESTABLISHMENT_NEWS, NEWS_UPDATE_ACTION, RELOAD_NEWS, SET_ACTIVE_NEWS
} from "./types";
import { noNews} from "../../common/constants/index";
import {checkResponseError} from "../../common/functions/index";
import {getData} from "./heper";
const {REACT_APP_BASE_URL} = process.env


export const setActiveItemNews = (news) => async (dispatch) => {

    dispatch({
        type: SET_ACTIVE_NEWS,
        payload: news
    })
};


export const newsUpdateAction = ({prop, value}) => async (dispatch) => {

    dispatch({
        type: NEWS_UPDATE_ACTION,
        payload: {prop, value}
    })
};


export const saveNewsAction = (news, saveCallback) => async (dispatch) => {
        console.log(news);


    try {
        var url = `${REACT_APP_BASE_URL}/news?`;


         await axios.post(url, news)

        saveCallback()

    } catch (e) {
        console.log(e)
        saveCallback(checkResponseError(e))
    }
};

export const updateNewsAction = (_id, news, saveCallback) => async (dispatch) => {
    const {localImage} = news ||{}


    try {
        var url = `${REACT_APP_BASE_URL}/news/${_id}`;

        const form = new FormData();

        if(localImage){
            form.append('file',localImage);
        }

        form.append('object', JSON.stringify(news));

        await axios.put(url, form)

        saveCallback()

    } catch (e) {
        console.log(e)
        saveCallback(checkResponseError(e))
    }
};

export const clearListAction = (_id, news, saveCallback) => async (dispatch) => {

}


export const deleteNewsAction = (_id,  calllback) => async (dispatch) => {
        console.log(_id);


    try {
        var url = `${REACT_APP_BASE_URL}/news/${_id}?`;



         await axios.delete(url)

       calllback()


    } catch (e) {
        console.log(e.message)
        calllback(checkResponseError(e))
    }
};


export const fetchNews = (skip, limit,) => async (dispatch) => {

        const config = {
            params: {
                skip: 0,
                limit: 50
            }
        }


        getData(dispatch, 'news', config, FETCH_NEWS,)
}


const reloadNews = async (filter, dispatch, callback) => {


    try {
        var url = `${REACT_APP_BASE_URL}/news?`;

        const {selectedCity} = filter || {};


        let config = {
            params: {
                skip: 0,
                limit: 50,
                cityId: selectedCity._id,
                registered: true

            },
        }


        const {data} = await axios.get(url, config)
        console.log(data)

        dispatch({
            type: RELOAD_NEWS,
            payload: {
                news: data,
                endList: data.length === 0 ? true : false,
                message: data.length > 0 ? '' : noNews,
            }
        })

        callback('Noticia Salva com sucesso')
    } catch (e) {
        console.log(e);
        callback(checkResponseError(e))

    }

}



export const fetchNewsEstablishment = (_id) => async (dispatch) => {
    dispatch({type: LOADING_ESTABLISHMENT_NEWS})


    try {
        const {data} = await axios.get(`${REACT_APP_BASE_URL}/news?establishmentId=${_id}`);

        dispatch({
            type: FETCH_NEWS_ESTABLISHMENT,
            payload: {
                newsEstablishment: data,
                establihsmentNewsMessage: data.length > 0 ? '' : "Sem Notícias Cadastradas no Momento",
            }
        })
    } catch (e) {
        console.log(e);
    }
}

export const clearNewsEstablishemntList = () => async (dispatch) => {


    dispatch({
        type: CLEAR_NEWS_ESTABLISHMENT_LIST,

    })

};

export const clearNewsList = () => async (dispatch) => {


    dispatch({
        type: CLEAR_NEWS_LIST,

    })

};