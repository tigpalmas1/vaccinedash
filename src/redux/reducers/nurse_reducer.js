import {
   LOGOUT,
    DAYS_UPDATE,
    DAYS_REMOVE, DAYS_ADD,
     FETCH_NURSE, FETCH_NURSE_LOADING, SET_NURSE
} from "../actions/types";
import _ from 'lodash';


const INITIAL_STATE = {
    nurses: [],
    messagse: '',
    loading: false,
    newNurse: {
        workHours: [],
    }


}

export default function (state = INITIAL_STATE, {type, payload}) {
    switch (type) {
        case 'NURSE_UPDATE':
            return {
                ...state,
                newNurse: {
                    ...state.newNurse,
                    [payload.prop]: payload.value
                },
            }


        case DAYS_ADD:
            return {
                ...state,
                newNurse: {
                    ...state.newNurse,

                    workHours: [...state.newNurse.workHours, payload.value]
                },
            }

        case DAYS_REMOVE:
            return {
                ...state,
                newNurse: {
                    ...state.newNurse,

                    workHours: [
                        ...state.newNurse.workHours.slice(0, payload),
                        ...state.newNurse.workHours.slice(payload + 1)
                    ]
                }
            }

        case DAYS_UPDATE:
            const daysArray = state.newNurse.workHours
            daysArray[payload.index] = {
                ...state.newNurse.workHours[payload.index],
                [payload.prop]: payload.value
            }
            return {
                ...state,
                newNurse: {
                    ...state.newNurse,
                    workHours: daysArray
                },
            }

        case FETCH_NURSE:
            return {
                ...state,

                nurses: _.uniqBy(payload.nurses, "_id"),
                loading: false,
                message: payload.message,

            };

        case FETCH_NURSE_LOADING:

            return {
                ...state,
                loading: payload.loading,
                message: payload.message
            }
        case SET_NURSE:

            return {
                ...state,
                newNurse: payload ? payload : {
                    workHours: [],
                },

            }


        case LOGOUT:
            return INITIAL_STATE

        default:
            return state;
    }
}

