import {

    FETCH_PROMOTED_VACCINE,
  PROMOTED_VACCINE_UPDATE,

} from "../actions/types";
import _ from 'lodash'

const INITIAL_STATE = {
    promotedVaccines:[],
    newPromotedVaccine: {}

}



export default function (state = INITIAL_STATE, {type, payload}) {
    switch (type) {

        case PROMOTED_VACCINE_UPDATE:

            return {
                ...state,
                newPromotedVaccine: {
                    ...state.newPromotedVaccine,
                    [payload.prop]: payload.value
                },
            }
        case FETCH_PROMOTED_VACCINE:
            return {
                ...state,
                promotedVaccines: _.uniqBy(payload.promotedVaccines, "_id"),

            };

        default:
            return state;
    }
}