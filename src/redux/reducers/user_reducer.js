import {
     FETCH_USERS, FETCH_USERS_LOADING, LOGIN_SUCCESS, LOGOUT,
   SIGNUP_SUCCESS,
     CLEAR_USERS
} from "../actions/types";
import _ from 'lodash';


const INITIAL_STATE = {
    data: {
        user:{},
        permission:{}
    },
    users: [],
    messagse: '',
    loading: false,
    endList: false,
    item:{
       user:{name: 'Tiago'},
        permission:{}
    },


}

export default function (state = INITIAL_STATE, {type,payload}) {
    switch (type) {
        case 'SET_USER':
            return {
                ...state,
                item: payload
            }

        case 'USER_UPDATE':
            return {
                ...state,
                item: {
                    ...state.item,
                    permission: {
                        ...state.item.permission,
                        [payload.prop]: payload.value
                    }
                },
            }
        case SIGNUP_SUCCESS:
            return {
                ...state,
                data: payload
            }
        case LOGIN_SUCCESS:
            return {
                ...state,
                data: payload,
            }


        case FETCH_USERS_LOADING:
            return {
                ...state,
                message: payload.message,
                loading: payload.loading,
            }

        case FETCH_USERS:
            return {
                ...state,
                users: _.uniqBy(state.users.concat(payload.users), "user._id"),
                loading: false,
                message: payload.message,
                endList: payload.endList,


            }

        case CLEAR_USERS:
            return {
                ...state,
                users: [],


            }

        case LOGOUT:
            return INITIAL_STATE

        default:
            return state;
    }
}

