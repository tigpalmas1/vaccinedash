import {
     CLEAR_SCHEDULE_FORM,
        FETCH_COVID,
  FILTER_COVID_UPDATE,
 SCHEDULE_UPDATE,

} from "../actions/types";
import _ from 'lodash'

const INITIAL_STATE = {
    data: [],
    loading: false,
    message: '',
    schedule: {
        city:  'curitiba',
    },
    filter: {
        city:  'curitiba',
        cities: [
            {value: 'curitiba', label:'Curitiba-PR'},
            {value: 'londrina', label:'Londrina-PR'},
            {value: 'ponta-grossa', label:'Ponta Grossa-PR'},
            {value: 'maringa', label:'Maringá-PR'},
            {value: 'porto-alegre', label:'Porto Alegre-RS'},
            {value: 'florianopolis', label:'Florianópolis-SC'},
            {value: 'blumenau', label:'Blumenau-SC'},
            {value: 'joinville', label:'Joinville-SC'},
            {value: 'paranavai', label:'Paranavaí-PR'},
            {value: 'cascavel', label:'Cascavel-PR'},
        ]
    }

}



export default function (state = INITIAL_STATE, {payload, type}) {
    switch (type) {

        case SCHEDULE_UPDATE:
            return {
                ...state,
                schedule: {
                    ...state.schedule,
                    [payload.prop]: payload.value
                },
            }
        case FILTER_COVID_UPDATE:
            return {
                ...state,
                filter: {
                    ...state.filter,
                    [payload.prop]: payload.value
                },
            }

        case FETCH_COVID:

            return {
                ...state,
                data: _.uniqBy(payload.data, "_id"),
                loading: false,
                message: payload.message,
                endList: payload.endList,
            };

        case CLEAR_SCHEDULE_FORM:
            console.log("CLEAR_SCHEDULE_FORM");
            console.log(payload);
            return {
                ...state,
                schedule: {
                    city:  'curitiba',
                },
            };

        default:
            return state;
    }
}