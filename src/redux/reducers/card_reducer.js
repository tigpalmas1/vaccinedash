import {
    APPLY_CARD_UPDATE, CLEAR_DATA_STAMP,
    FETCH_CARDS,
    FETCH_CARDS_LOADING, FETCH_USER_TABLE, SCHEDULE_UPDATE, SET_ACTIVE_CARD, SET_ACTIVE_STAMP, SET_CARD_STATUS,
} from "../actions/types";
import _ from 'lodash'

const INITIAL_STATE = {
    cards: [],
    activeCard: {},
    userTable: {},
    dataStamp: {},
    activeStamp:null
}



export default function (state = INITIAL_STATE, {payload,type}) {
    switch (type) {
        case SET_ACTIVE_CARD:

            return {
                ...state,
                activeCard: payload,

            };

        case SET_CARD_STATUS:

            return {
                ...state,
                activeCard: {
                    ...state.activeCard,
                    status: payload
                },

            };

        case SET_ACTIVE_STAMP:

            return {
                ...state,
                activeStamp: payload,

            };

        case APPLY_CARD_UPDATE:
            return {
                ...state,
                dataStamp: {
                    ...state.dataStamp,
                    [payload.prop]: payload.value
                },
            }
        case CLEAR_DATA_STAMP:
            return {
                ...state,
                dataStamp: {

                },
            }
        case FETCH_CARDS:

            return {
                ...state,
                cards: _.uniqBy(payload.cards, "_id"),
                loading: false,
                message: payload.message,
                endList: payload.endList,
            };

        case FETCH_USER_TABLE:
            return {
                ...state,
                userTable: payload.userTable,
            };

        default:
            return state;
    }
}