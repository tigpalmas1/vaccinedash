import {

     FETCH_ESTABLISHMENT_INFO,  FETCH_FOUND_ITENS,

     FETCH_VERSION,

} from "../actions/types";

const INITIAL_STATE = {
   data:{
       androidVersion: '',
       iosVersion: '',
       dashVersion: '',
   },
   info:{

   },
    foundItens:[],
    messageFound: '',
    foundLoading: false

}



export default function (state = INITIAL_STATE, {type, payload}) {
    switch (type) {
        case FETCH_VERSION:{
            return {
                ...state,
                data: payload,
            } ;
        }

        case FETCH_ESTABLISHMENT_INFO:{
            return {
                ...state,
                info: payload,
            } ;
        }

        case FETCH_FOUND_ITENS:{
            return {
                ...state,
                foundItens: payload.foundItens,
                messageFound: payload.message,
            } ;
        }


        default:
            return state;
    }
}