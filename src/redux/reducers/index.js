import {combineReducers} from 'redux';


import auth from './auth_reducer'
import establishment from './establishments_reducer'
import administration from './administration_reducer'
import wharehouse from './wharehouse_reducer'

import event from './event_reducer'
import user from './user_reducer'
import nurses from './nurse_reducer'
import filter from './filter_reducer'
import promotedVaccine from './promoted_vaccine_reducer'
import card from './card_reducer'
import news from './news_reducer'
import covid from './covid_reducer'



export default combineReducers({
    auth,
    establishment,
    wharehouse,
    administration,
    covid,
    user,
    nurses,
    event,
    filter,
    promotedVaccine,
    card,
    news
});