import {

    FETCH_WHAREHOUSE,
    FETCH_WHAREHOUSE_LOADING,
    SET_ACTIVE_WHAREHOUSE,
    SET_EDIT_WHAREHOUSE,
    WHAREHOUSE_UPDATE_ACTION,
} from "../actions/types";
import _ from 'lodash'

const INITIAL_STATE = {
    activeWharehouse:{},
    news: [],
    explorerNews: [],
    newsEstablishment: [],
    newsLoading: false,
    loading:false,
    endList: false,
    message: '',
    establihsmentNewsMessage: '',
    newWharehouse:{}

}



export default function (state = INITIAL_STATE, action) {
    switch (action.type) {

        case WHAREHOUSE_UPDATE_ACTION:

            return {
                ...state,
                newWharehouse: {
                    ...state.newWharehouse,
                    [action.payload.prop]: action.payload.value
                },
            }
        case SET_ACTIVE_WHAREHOUSE: {
            return {...state,
                newWharehouse: action.payload,
            } ;
        }

        case SET_EDIT_WHAREHOUSE: {
            return {...state,
                newWharehouse: action.payload,
            } ;
        }

        case FETCH_WHAREHOUSE_LOADING:
            return {...state, loading: action.payload,  message: 'Buscando Estoque'};
        case FETCH_WHAREHOUSE:
            return {
                ...state,
                news: _.uniqBy(action.payload.news, "_id"),
                loading: false,
                message: action.payload.message,
                endList: action.payload.endList,
            };




        default:
            return state;
    }
}