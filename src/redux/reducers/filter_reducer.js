import {

} from "../actions/types";
import {FAVORIT_UPDATE} from "../actions/types";
import {STATE_CHANGED} from "../actions/types";
import {FETCHE_TAGS} from "../actions/types";
import {FETCH_CITIES} from "../actions/types";
import {NO_FILTER} from "../actions/types";
import {FETCH_FILTER} from "../actions/types";
import {ADD_STUDENT_ACTION} from "../actions/types";
import {SET_ACTIVE_STUDENT} from "../actions/types";




const INITIAL_STATE = {
    preference: {
        selectedStudent: null

    },
    stateChangend: false,
    setedFilter: false,



};

export default function (state = INITIAL_STATE, {type, payload }) {
    switch (type) {
        case SET_ACTIVE_STUDENT: {
            return {
                ...state,
                preference:{
                    ...state.preference,
                    selectedStudent: payload
                } ,
            } ;
        }


        case ADD_STUDENT_ACTION:
            return {
                ...state,
                filter:{
                    ...state.filter,
                    selectedStudent:payload
                },


            }

        case FAVORIT_UPDATE:

            return {...state,
                preference: {
                    ...state.preference,
                    [payload.prop]: payload.value
                }

            }
        case STATE_CHANGED:

            return {...state,
                stateChangend: payload.stateChanged
            }
        case FETCHE_TAGS:

            return {...state,
                preference: {
                    ...state.preference,
                    tags: payload.tags
                }
            };
        case FETCH_CITIES:

            return {...state,
                preference: {
                    ...state.preference,
                    cities: payload.cities
                }
            };
        case FETCH_FILTER:
            return {
                ...state,
                preference: payload.filter,
                setedFilter: true,

            }
        case NO_FILTER:
            return {
                ...state,
                setedFilter: payload.filter,

            }
        default:
            return state;
    }
}

