import {
    AUTH_UPDATE, LOGIN_LOADING, LOGIN_ERROR,
    LOGOUT, LOGIN_SUCCESS, SIGNUP_LOADING, SIGNUP_SUCCESS, SIGNUP_ERROR, AUTH_LOADING,

} from "../actions/types";



const INITIAL_STATE = {
    auth: {
        name: '',
        lastName: '',
        email: '',
        password: '',
        confirmPassword: '',
        acceptTerms: '',
        type: '',
    },
    errorMessage: '',
    errorSignup: '',
    loadingCheckUser: false,
    loading: false,
    loadingSignup: false,
    logged: false,
    checked: false

}

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        case AUTH_LOADING:
            return {
                ...state,
                loadingCheckUser: true,

            }

        case AUTH_UPDATE:
            return {
                ...state,
                auth: {
                    ...state.auth,
                    [action.payload.prop]: action.payload.value
                },


            }
        case LOGIN_LOADING:
            return {
                ...state,
               loading: true,
            }
        case LOGIN_SUCCESS:
            return {
                ...state,
                logged: true,
                loading: false,
                loadingCheckUser: false,
                checked: true,
            }

        case LOGIN_ERROR:
            return {
                ...state,
                logged: false,
                loading: false,
                errorMessage:action.payload,
                loadingCheckUser: false,
                checked: true,
            }

        case SIGNUP_LOADING:
            return {
                ...state,
                loadingSignup: true,
            }
        case SIGNUP_SUCCESS:
            return {
                ...state,
                logged: true,
                loadingSignup: false,
            }

        case SIGNUP_ERROR:
            return {
                ...state,
                logged: false,
                loadingSignup: false,
                errorSignup:action.payload
            }



        case LOGOUT:
            return INITIAL_STATE

        default:
            return state;
    }
}

