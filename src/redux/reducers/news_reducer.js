import {
    AUTH_UPDATE,
    FETCH_NEWS, NEWS_UPDATE_ACTION, SET_ACTIVE_NEWS,
} from "../actions/types";
import _ from 'lodash'

const INITIAL_STATE = {
    news:[],


}



export default function (state = INITIAL_STATE, {payload, type}) {
    switch (type) {
        case SET_ACTIVE_NEWS:
            return {
                ...state,
                newNews:payload
            }

        case FETCH_NEWS:
            return {
                ...state,
                news: _.uniqBy(payload.data, "_id"),
                loading: false,
                message: payload.message,
                endList: payload.endList,
            };
        case NEWS_UPDATE_ACTION:
            return {
                ...state,
                newNews: {
                    ...state.newNews,
                    [payload.prop]: payload.value
                },
            }


        default:
            return state;
    }
}