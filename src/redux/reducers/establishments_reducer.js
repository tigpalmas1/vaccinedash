import {
    CLEAR_ESTABLISHMENT_LIST, CLEAR_EXPLORER, CLEAR_EXPLORER_ESTABLISHMENTS,
    ESTABLISHMENT_UPDATE,
    FETCHE_ESTABLISHMENTS, FETCHE_ESTABLISHMENTS_LOADING, FETCHE_EXPLORER_ESTABLISHMENTS,
    ADDRESS_UPDATE,
    MEDIA_ADD, MEDIA_REMOVE, MEDIA_UPDATE, PERSONAL_DATA_UPDATE, PHONE_ADD, PHONE_REMOVE, PHONE_UPDATE,
    SET_ACTIVE_ESTABLISHMENT, SET_EDIT_ESTABLISHMENT
} from "../actions/types";
import _ from 'lodash'

const INITIAL_STATE = {
    activeEstablishment: null,

    establishments: [],
    explorer_establishments: [],
    loading: false,
    endList: false,
    message: '',
    message_events_explorer: '',
    newEstablishment: {
        address: [],

        phone: [],


    }

}


export default function (state = INITIAL_STATE, {payload, type}) {
    switch (type) {

        case SET_ACTIVE_ESTABLISHMENT: {
            console.log(payload)
            return {
                ...state,
                activeEstablishment: payload,
                newEstablishment: {address: []}
            };
        }

        case SET_EDIT_ESTABLISHMENT: {

            return {
                ...state,
                newEstablishment: payload,
            };
        }

        case 'UPDATE_IMAGE': {

            return {
                ...state,
                newEstablishment: {
                    ...state.newEstablishment,
                    imageId: {smallUrl: payload}
                },
            };
        }


        case 'CLEAR_ESTABLISHMENT': {

            return {
                ...state,
                newEstablishment: {address: []},
            };
        }

        case ESTABLISHMENT_UPDATE:
            return {
                ...state,
                newEstablishment: {
                    ...state.newEstablishment,
                    [payload.prop]: payload.value
                },
            }

        case PERSONAL_DATA_UPDATE:
            return {
                ...state,
                newEstablishment: {
                    ...state.newEstablishment,
                    personalDataId: {
                        ...state.newEstablishment.personalDataId,
                        [payload.prop]: payload.value
                    }

                },
            }

        case ADDRESS_UPDATE:
            console.log(payload.index)

            const addressArray = state.newEstablishment.address;
            addressArray[payload.index] = {
                ...state.newEstablishment.address[payload.index],
                [payload.prop]: payload.value
            }

            console.log(addressArray)

            return {
                ...state,
                newEstablishment: {
                    ...state.newEstablishment,
                    address: addressArray


                },
            }


        case PHONE_ADD:
            return {
                ...state,
                newEstablishment: {

                    ...state.newEstablishment,
                    phone: [...state.newEstablishment.phone, payload.value]

                },
            }

        case PHONE_REMOVE:
            return {
                ...state,
                newEstablishment: {

                    ...state.newEstablishment,
                    phone: [
                        ...state.newEstablishment.phone.slice(0, payload),
                        ...state.newEstablishment.phone.slice(payload + 1)
                    ]

                },
            }

        case PHONE_UPDATE:
            const phoneArray = state.newEstablishment.phone
            phoneArray[payload.index] = {
                ...state.newEstablishment.phone[payload.index],
                [payload.prop]: payload.value
            }

            return {
                ...state,
                newEstablishment: {
                    ...state.newEstablishment,

                    phone: phoneArray


                },
            }


        case MEDIA_ADD:
            return {
                ...state,
                newEstablishment: {
                    ...state.newEstablishment,
                    personalDataId: {
                        ...state.newEstablishment.personalDataId,
                        socialNetwork: [...state.newEstablishment.personalDataId.socialNetwork, payload.value]
                    }

                },
            }

        case MEDIA_REMOVE:
            return {
                ...state,
                newEstablishment: {
                    ...state.newEstablishment,
                    personalDataId: {
                        ...state.newEstablishment.personalDataId,
                        socialNetwork: [
                            ...state.newEstablishment.personalDataId.socialNetwork.slice(0, payload),
                            ...state.newEstablishment.personalDataId.socialNetwork.slice(payload + 1)
                        ]
                    }
                },
            }

        case MEDIA_UPDATE:
            const socialArray = state.newEstablishment.personalDataId.socialNetwork
            socialArray[payload.index] = {
                ...state.newEstablishment.personalDataId.socialNetwork[payload.index],
                [payload.prop]: payload.value
            }

            return {
                ...state,
                newEstablishment: {
                    ...state.newEstablishment,
                    personalDataId: {
                        ...state.newEstablishment.personalDataId,
                        socialNetwork: socialArray
                    }

                },
            }


        case FETCHE_ESTABLISHMENTS:
            return {
                ...state,

                establishments: _.uniqBy(state.establishments.concat(payload.establishments), "_id"),
                loading: false,
                message: payload.message,
                endList: payload.endList,
            };

        case FETCHE_EXPLORER_ESTABLISHMENTS:
            return {
                ...state,
                explorer_establishments: payload.explorer_establishments,
                message_events_explorer: payload.message_events_explorer,
                loading: false
            };

        case CLEAR_EXPLORER_ESTABLISHMENTS:
            return {...state, explorer_establishments: [], loading: false};

        case FETCHE_ESTABLISHMENTS_LOADING:

            return {
                ...state,
                loading: payload.loading,
                message: payload.message
            }

        case CLEAR_EXPLORER: {
            return {
                ...state,
                explorer_establishments: [],
                message_events_explorer: ''
            }
        }

        case CLEAR_ESTABLISHMENT_LIST:
            return {
                ...state,
                endList: false,
                loading: false,
                message: '',
                establishments: []
            }
        default:
            return state;
    }
}