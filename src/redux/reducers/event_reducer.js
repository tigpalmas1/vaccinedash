import {
    CLEAR_KITS_LIST,
    EVENT_UPDATE,
    FETCH_KITS,
    FETCH_KITS_LOADING,
    LOGOUT,
    SET_ACTIVE_ESTABLISHMENT,
    SET_ACTIVE_ITEM,
    SET_EDIT_EVENT,
} from "../actions/types";
import _ from 'lodash';


const INITIAL_STATE = {
    loading: false,
    kits: [],
    noDescriptionEvents: [],
    activeEvent: null,
    message: '',
    endList: false,
    kit: {
        itemId: []
    },
    newEvent: {
        isStandard: false,
        name: '',
        dose: [],

    }

}

export default function (state = INITIAL_STATE, {payload, type}) {
    switch (type) {
        case SET_ACTIVE_ITEM: {
            return {
                ...state,
                activeEvent: payload? payload :{isStandard: false,  name: '', dose: []},
            };
        }

        case SET_EDIT_EVENT: {

            return {
                ...state,
                newEvent: payload,
            };
        }

        case EVENT_UPDATE:

            return {
                ...state,
                newEvent: {
                    ...state.newEvent,
                    [payload.prop]: payload.value
                },
            }

        case FETCH_KITS_LOADING:
            return {
                ...state,
                message: payload.message,
                loading: payload.loading
            }

        case FETCH_KITS:
            return {
                ...state,
                kits: _.uniqBy(payload.kits, "_id"),
                loading: false,
                message: payload.message,
                endList: payload.endList,
            }

        case SET_ACTIVE_ESTABLISHMENT: {
            return {
                ...state,

                kits: [],
                loading: false,
                message: ''
            }
        }

        case CLEAR_KITS_LIST: {
            return {
                ...state,

                kits: [],
                loading: false,
                message: ''
            }
        }

        case 'DOSE_ADD':
            const {newEvent} = state ||{}
            const {dose} = newEvent ||[]

            return {
                ...state,
                newEvent: {
                    ...state.newEvent,
                    dose: dose? [...state.newEvent.dose, payload.value]: [ payload.value]
                },
            }

        case 'DOSE_REMOVE':
            return {
                ...state,
                newEvent: {
                    ...state.newEvent,

                    dose: [
                        ...state.newEvent.dose.slice(0, payload),
                        ...state.newEvent.dose.slice(payload + 1)
                    ]
                }

            }

        case 'DOSE_UPDATE':
            const tempDose = state.newEvent.dose
            tempDose[payload.index] = {
                ...state.newEvent.dose[payload.index],
                [payload.prop]: payload.value
            }

            return {
                ...state,
                newEvent: {
                    ...state.newEvent,
                    dose: tempDose
                    }


            }

        case LOGOUT:
            return INITIAL_STATE

        default:
            return state;
    }
}

