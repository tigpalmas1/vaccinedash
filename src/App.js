import React from 'react';
import './App.css';
import MainContainer from "./components/navigation/stacks/MainContainer";
import {Provider} from 'react-redux';
import store from './redux/store'




function App() {
    return (

            <Provider store={store}>
                <MainContainer/>
            </Provider>

    );
}

export default App;
